#comdeck agcm_nudging
# sk: AGCM nudging and DCPP runmodes (2020-12-11)

if [[ "$runmode" == NEMO-AGCM-dcpp-assim*-spin* ]] ; then
  # DCPP assimilation runmode
  # we repeatedly use the 1958-1967 decade for spinup
  if [ "${reanal_use_era5}" = "on" ] ; then
    target_ss_pfx=td_era5_t63l37_gcm18_t63
  else
    target_ss_pfx=td_era40_debias_ngaus3_gcm18_t63
  fi
  yearss=$year
  addyrs=0
  while [ $yearss -gt 1967 ] ; do
    yearss=`expr $yearss - 10`
    addyrs=`expr $addyrs + 10`
  done
  addyrs=`echo $addyrs | awk '{printf "%05d",$1}'`
  access SPCFILE ${target_ss_pfx}_${yearss}_m01_months12_ss
  # relabel the year in the target file
  echo "C*TSTEP                  $addyrs     YYYYMMDDHH     YYYYMMDDHH" | ccc tstep SPCFILE SPCFILE.1 ; mv SPCFILE.1 SPCFILE
elif [ -n "${target_ss_pfx}" -a ! -s SPCFILE ] ; then
  # nudging target is specified - use it
  access SPCFILE ${target_ss_pfx}_${year}_ss
else
  # use reanalyses as the nudging target
  .  reanal_sp2ss.cdk
fi

if [ -n "${target_soil_tg1}" -a ! -s TG1FILE ] ; then
  # Soil temperature nudging file
  access TG1FILE ${target_soil_tg1} # top soil layer temperature
  echo "C*TSTEP                  $addyrs       YYYYMMDD       YYYYMMDD" | ccc tstep TG1FILE TG1FILE.1 ; mv TG1FILE.1 TG1FILE
fi
if [ -n "${target_soil_wg1}" -a ! -s WG1FILE ] ; then
  # Soil moisture nudging file
  access WG1FILE ${target_soil_wg1} # top soil layer volumetric moisture
  echo "C*TSTEP                  $addyrs       YYYYMMDD       YYYYMMDD" | ccc tstep WG1FILE WG1FILE.1 ; mv WG1FILE.1 WG1FILE
fi

# access daily SST
if [ -n "${target_ocn_sst}" -a ! -s SSTFILE ] ; then
  access SSTFILE  ${target_ocn_sst}
fi

# access daily SICN
if [ -n "${target_ocn_sicn}" -a ! -s SICNFILE ] ; then
  access SICNFILE ${target_ocn_sicn}
fi

# access daily SIC
if [ -n "${target_ocn_sic}" -a ! -s SICFILE ] ; then
  access SICFILE ${target_ocn_sic}
fi

# spectral climatological bias-correcting tendencies
if [ -n "$tspcclim" -a "$tspcclim" != " " -a ! -s "TSPCCLIM" ] ; then
  access TSPCCLIM $tspcclim
fi

# gridded climatological bias-correcting tendencies
if [ -n "$tgrdclim" -a "$tgrdclim" != " " -a ! -s "TGRDCLIM" ] ; then
  access TGRDCLIM $tgrdclim
fi
