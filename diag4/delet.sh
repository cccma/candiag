#!/bin/sh
#                   delet               ec - Jun 28/11 - fm
#   ----------------------------------- Delete files. Each file name must
#                                       be specifed by parmsub parameters 
#                                       file1, file2, etc. and the "join"
#                                       condef parameter must specify the
#                                       number of files to delete.

    # build up a file with names of files to delete
    DELFILE=delete_files.$$
    for i in `seq $join`; do
      nam=file$i ;
      echo ${!nam} >> $DELFILE
    done

    # call fdb function
    fdb mdelete -f $DELFILE || true # don't die if failed (e.g. no files to delete)

    rm -f $DELFILE
