#!/bin/bash
set -x
# gcm_parallel_setup
# 
#   - Setup environment
#   - create agcm/coupler script 
#   - create prunsc script (which launches the model)

# define default thread counts for each component
nproc_a=${nproc_a:=1}
nproc_o=${nproc_o:=1}
nnode_a=${nnode_a:=0}
nnode_o=${nnode_o:=0}
nnode_c=${nnode_c:=0}
nproc_c=${nproc_c:=0}

# Determine highest thread count
if (( nproc_a >= nproc_o )) && (( nproc_a >= nproc_c )); then
    nthreads_max=$nproc_a
elif (( nproc_o >= nproc_a )) && (( nproc_o >= nproc_c )); then
    nthreads_max=$nproc_o
elif (( nproc_c >= nproc_a )) && (( nproc_c >= nproc_o )); then
    nthreads_max=$nproc_c
fi

# Determine total number of mpi tasks
# THIS IS MPI TASKS, NOT NODES
#   - rename these variables
ntask_total=$(( nnode_a + nnode_c + nnode_o ))

# Setup environment
cp ${WRK_DIR}/config/runtime_environment . 
source runtime_environment
export OMP_NUM_THREADS=$nthreads_max 
export nemo_exec agcm_exec coupler_exec
export nnode_a nnode_c nnode_o
export nproc_a nproc_c nproc_o
export RANK_OUTPUT_PRFX RANK_OUTPUT_SFX

# create run command script
if [[ -z "$cmd_runp1" ]] ; then
    echo "Parallel Setup: The user must define a run command via 'cmd_runp1' and potentially 'cmd_runp2/3'!"
    echo "Bailing...."
    exit 1
else
    cmd_run="$cmd_runp1"
    [[ -n "$cmd_runp2" ]] && cmd_run="$cmd_run $cmd_runp2"
    [[ -n "$cmd_runp3" ]] && cmd_run="$cmd_run $cmd_runp3"
    echo "$cmd_run" > prunsc
    chmod u+x prunsc
fi
