#!/bin/sh

### this script processes 6-hourly and 1-hourly 2D variables (ryj001 in Jan. 2022)
### 6-hourly and 3-hourly 3D variables were processed in daily_cordex.sh

if [ -z "$year_offset" ] ; then
  echo "ERROR in daily_cmip6.dk: undefined variable year_offset"
  exit 1
fi
if [ -z "$delt" ] ; then
  echo "ERROR in daily_cmip6.dk: undefined variable delt"
  exit 1
fi

yearoff=`echo "$year_offset"  | $AWK '{printf "%4d", $0+1}'`
deltmin=`echo "$delt"         | $AWK '{printf "%5d", $0/60.}'`
echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL     YYYYMMDDHH" > ic.tstep_model_6h
echo "C*BINSML      6         1         1" > ic.bins6h

. ggfiles.cdk

########################################################################################################
### list of 6-hourly accumulative variables to be selected from gs file
varlist1="rofo rof smlt ufs vfs fn zn "

rm -f ic.xsave
for var in $varlist1; do
  binsml npakgg/${var^^} $var.6h input=ic.bins6h

  ### relable time step
  tstep $var.6h $var input=ic.tstep_model_6h

  ### accumulate xsave input card
  echo -e "C*XSAVE       ${var^^}\n" >> ic.xsave
done

### list of 6-hourly instantaneous variables to be selected from gs file
varlist2="tg wgf wgl sno sicn snw"
for var in $varlist2; do
  if [[ $var == snw ]]; then
    ### compute snow amount over land (on tile 1 only)
    tsplit npakgg/SNOT tmp1 tmp2 tmp3 tmp4 tmp5 snot.6h
    echo "C*SELLEV      1    1" | ccc sellev snot.6h snw.6h
  else
    tsplit npakgg/${var^^} tmp1 tmp2 tmp3 tmp4 tmp5 $var.6h
  fi

  ### relable time step
  tstep $var.6h $var input=ic.tstep_model_6h

  ### accumulate xsave input card
  echo -e "C*XSAVE       ${var^^}\n" >> ic.xsave
done

rm -f *.6h tmp*

access old6h ${flabel}6h
xsave old6h $varlist1 $varlist2 new6h input=ic.xsave

delete old6h
save new6h ${flabel}6h

########################################################################################################
### list of 1-hourly variables to be output
varlist="ps pmsl st sq su sv srh pcp pcpc pcpn qfs swa cldt gt fss fdl fsg flg fso fsr olr hfl hfs pblh pwat clwt cict wgf wgl rofo rof ufs vfs tn"

rm -f ic.xsave
for var in $varlist; do
  if [[ $var == ps ]]; then
    tstep npakgg/LNSP lnsp input=ic.tstep_model_6h
    expone lnsp tmps
    echo "XLIN            1.00             PS" | ccc xlin tmps ps
  elif [[ $var == pmsl ]]; then
    ### pnsl is the pmsl output directly from GEM
    tstep npakgg/PNSL pnsl input=ic.tstep_model_6h
    echo "XLIN            0.01           PMSL" | ccc xlin pnsl pmsl
  else
    tstep npakgg/${var^^} $var input=ic.tstep_model_6h
  fi

  ###accumulate xsave input card
  echo -e "C*XSAVE       ${var^^}\n" >> ic.xsave
done

xsave old1h $varlist new1h input=ic.xsave

save new1h ${flabel}1h

