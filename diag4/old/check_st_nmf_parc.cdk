#comdeck check_st_nmf_parc
#
# Add "parc_check" switch to allow by-passing the "PARC" check.
#                 check_st_nmf_parc    FM - Apr 09/08 - FM. ---check_st_nmf_parc

#  ----------------------------------- From local "ST_nmf" binary file extract
#                                      (as applicable) PARM and PARC named binary
#                                      records, (if present) convert "PARC" into 
#                                      character file and verify its equivalent 
#                                      setting to the user's specified critical 
#                                      section, if no discrepency is detected, 
#                                      generate "new_ST_nmf" replacement file 
#                                      to "ST_nmf" insuring PARM and (if 
#                                      applicable) PARC named binary records 
#                                      appending to the bottom of it.

echo "" ; echo " check_st_nmf_parc: Checking and processing PARM/PARC records:" ; echo ""

(\rm -f new_ST_nmf X_ST XPARM XPARC rcvrd.critical user.critical || : )

# Possibly, setup to use "float1" executables...

if [ "$float1" = 'on' ] ; then 
 FLTEXTNSN='_float1' 
else 
 FLTEXTNSN=' ' 
fi

if [ -s "ST_nmf" ] ; then

 # Extract "PARM" and "PARC" named binary records from "ST_nmf".

 separmc${FLTEXTNSN} ST_nmf X_ST XPARM XPARC

 if [ -n "$parc_check" -a "$parc_check" = "off" ] ; then
  :
 else
  if [ -s "XPARC" ] ; then
 
   # if successful in recovering "XPARC" critical record, 
   # convert it to character format.
 
   bin2txt XPARC rcvrd.critical
 
   # check, if PARC binary file from the "critical section" 
   # in the submission job, is present and not empty.
 
   if [ -s "PARC" ] ; then
 
    # if test is passed, convert "PARC" named binary record of the 
    # user's defined critical section into "user.critical" character 
    # format file.
   
    bin2txt PARC user.critical
 
    # perform a one-to-one equivalency test between parameters setting 
    # in the "rcvrd.critical" vs. those in "user.critical" file.
 
    chkcrtcl rcvrd.critical user.critical &&
    critical_check_ok1='yes' || 
    critical_check_ok1='no'
    chkcrtcl user.critical rcvrd.critical && 
    critical_check_ok2='yes' || 
    critical_check_ok2='no'
 
    # abort if any discrepency is detected.
 
    if [ -s "haltit" -o "$critical_check_ok1" != 'yes' -o "$critical_check_ok2" != 'yes' ] ; then
     abort='yes'
     exit 1
    else
     unset critical_check_ok1 critical_check_ok2
    fi
 
 
   else
  
    # Force an abort if XPARC is present but PARC is absent.
  
    echo "" ; echo " check_st_nmf_parc: Abort due to the presence of PARC record in 'ST_nmf' restart file"
    echo "                    and the absence of corresponding valid 'critical section' in the "
    echo "                    submission job" ; echo ""
    touch haltit
    (echo "" ; echo " check_st_nmf_parc: Abort due to the presence of PARC record in 'ST_nmf' restart file") >> haltit
    echo "                    and the absence of corresponding valid 'critical section' in the "             >> haltit
    (echo "                    submission job" ; echo "")                                                    >> haltit
    abort='yes'
    exit 2
 
   fi
 
  else
 
   if [ -s "PARC" ] ; then
 
    # Force an abort if XPARC is absent but PARC is present.
  
    echo "" ; echo " check_st_nmf_parc: Abort due to the absence of PARC record in 'ST_nmf' restart file "
    echo "                    and the presence of a valid 'critical section' in the submission job" ; echo ""
    touch haltit
    (echo "" ; echo " check_st_nmf_parc: Abort due to the absence of PARC record in 'ST_nmf' restart file ")    >> haltit
    (echo "                    and the presence of a valid 'critical section' in the submission job" ; echo "") >> haltit
    abort='yes'
    exit 3
 
   fi
  fi
 fi

 # Generate in "new_ST_nmf" a replacement to "ST_nmf" file 
 # insuring PARM and (if applicable) PARC named binary records
 # are appended to the bottom of it.

 if [ -s PARM -a -s PARC ] ; then 

   joinup${FLTEXTNSN} new_ST_nmf X_ST PARC PARM

 elif [ -s PARM ] ; then

   joinup${FLTEXTNSN} new_ST_nmf X_ST PARM

 else

   mv X_ST new_ST_nmf
 
 fi

else

 # Force an abort if "ST_nmf" local file is not present 
 # or empty ...

 ls -ld ST_nmf 
 touch haltit
 echo "" ; echo " check_st_nmf_parc: ST_nmf local file should not be empty!" ; echo "" 
 (echo "" ; echo " check_st_nmf_parc: ST_nmf local file should not be empty!" ; echo "") >> haltit
 abort='yes'
 exit 4

fi
#  ............................................................check_st_nmf_parc
