#deck daily_cmip5
jobname=daily_cmip5; time=$gptime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<'end_of_script'

#  Phase out disabling the trap in optional processing of 
#  daily cm variables via "cmfiles.cdk" call.
#  
#
#                  daily_cmip5          sk,dy - Nov 21/13 - fm
#  -----------------------------------  extract (sub) daily data from 
#                                       the selected fields in gs and 
#                                       cm files
#  
#  Daily data types:
#
#  tier 1 - (sub)daily 2D variables extracted for all runs/periods
#
#    input variables:
#
#      suffix=gs
#      sq stmn stmx st pcp pcpn swa rof qfs fmi flg fsg hfl hfs 
#      srh srhn srhx fn cldt pcpc su sv swx fdl fss fslo fla  
#      gc wgf wgl gt sno sicn sic
#      pmsl
#
#      suffix=cm
#      sst sst2 hblm uice vice
#
#    output variables:
#      suffix=dd  
#      sq stmn stmx st pcp pcpn swa rof qfs fmi flg fsg hfl hfs
#      srh srhn srhx fn cldt pcpc su sv swx fdl fss fslo fla
#      gc (at 00Z) wgfl (=wgf+wgl at 1st layer) gt sno sicn sic
#      pmsl
#      sst sst2 hblm uice vice
#
#  tier 2 - (sub)daily 3D variables on pressure levels
#           extracted for some runs/periods
#
#    input variables available after gpint*.dk.
#
#    output variables:
#      suffix=dp
#      omeg phi rh shum temp u v (levels = 1000  850  700  500  250  100
#                                          50   10)
#      pmsl_6h temp_6h u_6h v_6h (levels = 850  500  250)   
#
#  if gssave=on, (sub)daily 3D variables on model levels are selected.
#
#    output variables:  
#      suffix=ds
#      ps shum temp u v
#
#  ----------------------------------- end of the head comments
#
#
#     if [ $daily_cmip5_tiers -eq 0 ] ; then
#       exit
#     fi

#   ---------------------------------- check some variables

    if [ -z "$year_offset" ] ; then
      echo "ERROR in daily_cmip5.dk: undefined variable year_offset"
      exit 1
    fi
    if [ -z "$delt" ] ; then
      echo "ERROR in daily_cmip5.dk: undefined variable delt"
      exit 1
    fi
    yearoff=`echo "$year_offset"  | $AWK '{printf "%4d", $0+1}'`
    deltmin=`echo "$delt"         | $AWK '{printf "%5d", $0/60.}'`

    if [ -z "$daily_cmip5_tiers" ] ; then

#  ----------------------------------- set default daily tiers, 
#                                      if not defined

      daily_cmip5_tiers="1" # 2-D vars
    fi

#  ----------------------------------- prepare input cards 

    echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL       YYYYMMDD" > ic.tstep_model
    echo "C*TSTEP   $deltmin     ${yearoff}010100       ACCMODEL       YYYYMMDD" > ic.tstep_accmodel
    echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL     YYYYMMDDHH" > ic.tstep_model_6h
    echo "C*BINSML      4         1" > ic.binsml

# ------------------------------------ access gs file

    if [ "$daily_cmip5_tiers" != "0" ] ; then

.     ggfiles.cdk

    fi

# ------------------------------------ there are several daily data types:
#                                      tier 1 is for (sub)daily 2D variables 
#                                             extracted for all runs/periods
#                                      tier 2 is for (sub)daily 3D variables 
#                                             on pressure levels extracted 
#                                             for some runs/periods
#                                      Also, if gssave=on, (sub)daily 3D 
#                                      variables on model levels are selected.
    for tier in $daily_cmip5_tiers ; do

      if [ $tier -eq 1 ] ; then

# ------------------------------------ TIER 1: 2D variables

        vars_all=""

# ------------------------------------ accumulated gs variables 

        vars="sq stmn stmx st pcp pcpn swa rof qfs fmi flg fsg hfl hfs srh srhn srhx fn cldt pcpc su sv swx fdl fss fslo fla"
        VARS=`fmtselname $vars`
        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS" | ccc select npakgg $vars || true
#                                      relabel time step
        for v in $vars ; do
          if [ -s $v ] ; then
            V=`echo $v | tr 'a-z' 'A-Z'`
            tstep $v gg$v input=ic.tstep_accmodel
#                                      accumulate variable list
            vars_all="$vars_all gg$v"
#                                      accumulate xsave input card
            echo "C*XSAVE       $V
               " >> ic.xsave
          fi
        done

# ------------------------------------ sampled gs variables 

        vars="gc wgf wgl gt sno sicn sic"
        VARS=`fmtselname ${vars}`
        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS" | ccc select npakgg $vars

# ------------------------------------ access pmsl
        release pmsl
        access  pmsl ${flabel}_pmsl

# ------------------------------------ process some variables
#                                      select 00Z from gc
        rsplit gc gc1 gc2 gc3 gc4
        mv gc1 gc
        rm gc2 gc3 gc4
        vars="gc"
#                                      compute first layer daily wgf+wgl
        echo "C*SELLEV      1    1" > ic.sellev1
        sellev wgf wgf1 input=ic.sellev1
        sellev wgl wgl1 input=ic.sellev1
        add wgf1 wgl1 wgfl
        echo "C*NEWNAM   WGFL" | ccc newnam wgfl wgfl.1 ; mv wgfl.1 wgfl
        release wgf wgl wgf1 wgl1
#                                      bin daily
        for v in pmsl wgfl gt sno sicn sic ; do
          binsml $v $v.d input=ic.binsml
          mv $v.d $v
          vars="$vars $v"
        done
#                                      relabel time step
        for v in $vars ; do
          if [ -s $v ] ; then
            V=`echo $v | tr 'a-z' 'A-Z'`
            tstep $v gg$v input=ic.tstep_model
#                                      accumulate variable list
            vars_all="$vars_all gg$v"
#                                      accumulate xsave input card
            echo "C*XSAVE       $V
               " >> ic.xsave
          fi
        done

# ------------------------------------ daily cm variables

        access CmCheck ${model1}cm nocp na
        if [ -s "./CmCheck" ] ; then
         release CmCheck
.        cmfiles.cdk  
        else
         release CmCheck
        fi

        if [ -s npakcm ] ; then
        vars="sst sst2 hblm uice vice"
        VARS=`fmtselname $vars`
        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$VARS" | ccc select npakcm $vars || true

        for v in $vars ; do
          if [ -s $v ] ; then
            V=`echo $v | tr 'a-z' 'A-Z'`
            mv $v cm$v
#                                      accumulate variable list
            vars_all="$vars_all cm$v"
#                                      accumulate xsave input card
            echo "C*XSAVE       $V
               " >> ic.xsave
          fi
        done
        fi

# ------------------------------------ xsave

        vars0=`echo "$vars_all" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
        rm old
        while [ -n "$vars0" ] ; do
          vars1=`echo "$vars0" | cut -f1-80 -d' '`
          head -160 ic.xsave > ic.xsave1
          echo vars=$vars1
          cat ic.xsave1
          xsave old $vars1 new input=ic.xsave1
          mv new old
          vars0=`echo "$vars0" | cut -f81- -d' '`
          tail -n +161 ic.xsave > ic.xsave1 ; mv ic.xsave1 ic.xsave
        done

#   ---------------------------------- save results.

        save old ${flabel}dd
      fi # tier=1

#
# ------------------------------------ TIER 2: 3D variables on pressure levels
#                                      (available after gpint*.dk)
      if [ $tier -eq 2 ] ; then
        vars_all=""

# ------------------------------------ 3D pressure level daily

        vars="gpt gprh gpq gpu gpv gpw gpz"
        echo "SELLEV        8 1000  850  700  500  250  100   50   10" > ic.sellev

#                                      access all variables
        for v in $vars ; do
          if [ "$v" = "gpt" ] ; then
            V="TEMP"
          elif [ "$v" = "gprh" ] ; then
            V="RH"
          elif [ "$v" = "gpq" ] ; then
            V="SHUM"
          elif [ "$v" = "gpu" ] ; then
            V="U"
          elif [ "$v" = "gpv" ] ; then
            V="V"
          elif [ "$v" = "gpw" ] ; then
            V="OMEG"
          elif [ "$v" = "gpz" ] ; then
            V="PHI"
          else
            V=`echo "$v" | sed -e's/^gp//' | tr 'a-z' 'A-Z'`
          fi
#                                      access
          release $v
          access  $v ${flabel}_$v
#                                      select levels
          sellev $v $v.l input=ic.sellev
          mv $v.l $v
#                                      bin daily
          binsml $v $v.d input=ic.binsml
#                                      relabel time step
          tstep $v.d $v.t input=ic.tstep_model
#                                      accumulate xsave input card
          echo "C*XSAVE       $V
               " >> ic.xsave
          vars_all="$vars_all $v.t"
        done

# ------------------------------------ 3D pressure level 6-h variables

        vars="pmsl gpu gpv gpt"
        echo "SELLEV        3 850  500  250" > ic.sellev.06h
        
        for v in $vars ; do
          if [ "$v" = "gpt" ] ; then
            V="TEMP"
          elif [ "$v" = "gprh" ] ; then
            V="RH"
          elif [ "$v" = "gpq" ] ; then
            V="SHUM"
          elif [ "$v" = "gpu" ] ; then
            V="U"
          elif [ "$v" = "gpv" ] ; then
            V="V"
          elif [ "$v" = "gpz" ] ; then
            V="PHI"
          elif [ "$v" = "gpw" ] ; then
            V="OMEG"
          elif [ "$v" = "pmsl" ] ; then
            V="PMSL"
            release pmsl
          else
            V=`echo "$v" | sed -e's/^gp//' | tr 'a-z' 'A-Z'`
          fi
#                                      access, if does not exist
          if [ ! -s $v ] ; then
            access $v ${flabel}_$v
          fi
#                                      select level subset
          if [ "$v" != "pmsl" ] ; then
            sellev $v $v.l input=ic.sellev.06h
            mv $v.l $v
          fi
#                                      relabel time step
          tstep $v $v.t.6h input=ic.tstep_model_6h
#                                      accumulate xsave input card
          echo "C*XSAVE       ${V}_6H
               " >> ic.xsave
             vars_all="$vars_all $v.t.6h"
        done # vars

# ------------------------------------ xsave

        vars0=`echo "$vars_all" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
        rm old
        while [ -n "$vars0" ] ; do
          vars1=`echo "$vars0" | cut -f1-80 -d' '`
          head -160 ic.xsave > ic.xsave1
          echo vars=$vars1
          cat ic.xsave1
          xsave old $vars1 new input=ic.xsave1
          mv new old
          vars0=`echo "$vars0" | cut -f81- -d' '`
          tail -n +161 ic.xsave > ic.xsave1 ; mv ic.xsave1 ic.xsave
        done

#   ---------------------------------- save results.

        save old ${flabel}dp
      fi # tier=2
    done # daily_cmip5_tiers

# ------------------------------------ (sub)daily on model levels

    if [ "$gssave" = "on" ] ; then
#                                      sampled (sub)daily data on model levels
#                                      requires gssave=on
      # 3D sigma levels
      gsvars="ps gsq gst gsu gsv"
      rm ic.xsave.gs
      for v in $gsvars ; do
        if [ "$v" = "gsq" ] ; then
          V="SHUM"
        elif [ "$v" = "gst" ] ; then
          V="TEMP"
        elif [ "$v" = "gsu" ] ; then
          V="U"
        elif [ "$v" = "gsv" ] ; then
          V="V"
        elif [ "$v" = "gsz" ] ; then
          V="PHI"
        elif [ "$v" = "ps" -o "$v" = "gsps" ] ; then
          V="PS"
        fi
        release $v
        access  $v ${flabel}_$v
#                                      relable time step
        tstep $v $v.1 input=ic.tstep_model_6h ; mv $v.1 $v
#                                      accumulate xsave input card
        echo "C*XSAVE       $V
               " >> ic.xsave.gs
      done

# ------------------------------------ xsave

      vars0=`echo "$gsvars" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
      rm old
      while [ -n "$vars0" ] ; do
        vars1=`echo "$vars0" | cut -f1-80 -d' '`
        head -160 ic.xsave.gs > ic.xsave.gs1
        xsave old $vars1 new input=ic.xsave.gs1
        mv new old
        vars0=`echo "$vars0" | cut -f81- -d' '`
        tail -n +161 ic.xsave.gs > ic.xsave.gs1 ; mv ic.xsave.gs1 ic.xsave.gs
      done

#   ---------------------------------- save results.

      save   old ${flabel}ds

    fi # gssave=on

end_of_script

cat > Input_Cards <<end_of_data
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days
         RUN       = $run
         FLABEL    = $flabel
         MODEL1    = $model1
0 DAILY_CMPI5 ------------------------------------------------------ DAILY_CMIP5
end_of_data

. endjcl.cdk
