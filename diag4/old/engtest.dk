#deck engtest
jobname=engt ; time=$stime ; memory=$memory2
. comjcl.cdk

cat > Execute_Script <<end_of_script

#                   engtest             ml nes. oct 26/88 - fm.
#   ----------------------------------- computes velocity potential and
#                                       streamfunction at 200 and 850 mbs  also
#                                       vertically integrated total energy flux
#                                       streamfunction and velocity potential 
#                                       for each of the control and experiment
#                                       runs in an experiment  and plots out
#                                       the result.
#                                       model1 and letter x refer to pooled
#                                       control run  while model2 and letter y
#                                       refer to the experimental run.
#
    access gp ${model1}gp
#   ----------------------------------- vertically integrated total energy flux
#                                       streamfunction and velocity potential.
#   ----------------------------------- first the control run.
.   engjcl.cdk
    cp gpsitn psinx 
    rm gpsitn
    cp gpsits psisx 
    rm gpsits
    cp gtrupn runx 
    rm gtrupn
    cp gtrvpn rvnx 
    rm gtrvpn
    cp gtrups rusx 
    rm gtrups
    cp gtrvps rvsx 
    rm gtrvps
    cp gchit chix 
    rm gchit
    cp gtdu dux 
    rm gtdu
    cp gtdv dvx 
    rm gtdv
    cp gp filex 
    rm gp
#   ----------------------------------- now the experimental run.
    access gp ${model2}gp
.   engjcl.cdk
    cp gpsitn psiny 
    rm gpsitn
    cp gpsits psisy 
    rm gpsits
    cp gtrupn runy 
    rm gtrupn
    cp gtrvpn rvny 
    rm gtrvpn
    cp gtrups rusy 
    rm gtrups
    cp gtrvps rvsy 
    rm gtrvps
    cp gchit chiy 
    rm gchit
    cp gtdu duy 
    rm gtdu
    cp gtdv dvy 
    rm gtdv
    cp gp filey 
    rm gp
#   ----------------------------------- velocity potential and streamfunction.
    xfind filex ux 
    xfind filex vx 
    xfind filey uy 
    xfind filey vy 
    rm filex filey
    gwtqd ux vx spvortx spdivx 
    gwtqd uy vy spvorty spdivy 
    rm ux uy vx vy
    splinv spdivx mspchix 
    splinv spdivy mspchiy 
    splinv spvortx msppsix 
    splinv spvorty msppsiy 
    xlin mspchix spchix 
    xlin mspchiy spchiy 
    xlin msppsix sppsix 
    xlin msppsiy sppsiy 
    cofagg spchix chixl 
    cofagg spchiy chiyl 
    cofagg sppsix psixl 
    cofagg sppsiy psiyl 
    rm spvortx spvorty spdivx spdivy mspchix mspchiy spchix spchiy
    rm msppsix msppsiy sppsix sppsiy
    sub chiyl chixl chidifl 
    sub psiyl psixl psidifl 
    newnam chixl kix 
    newnam chiyl kiy 
    newnam chidifl kidif 
    newnam psixl psix 
    newnam psiyl psiy 
    newnam psidifl psidif 
    rm chixl chiyl chidifl psixl psiyl psidifl
#   ----------------------------------- now calculate the difference between
#                                       the two runs.
    sub psiny psinx psindif 
    sub psisy psisx psisdif 
    sub runy runx rundif 
    sub rvny rvnx rvndif 
    sub rusy rusx rusdif 
    sub rvsy rvsx rvsdif 
    sub chiy chix chidif 
    sub duy dux dudif 
    sub dvy dvx dvdif 
#   ----------------------------------- plot out resulting grids.
    libncar plunit=$plunit 
.   plotid.cdk
    if [ "$subarea" = on ] ; then
#     ----------------------------------- run subarea first on the files to
#                                         be plotted.
#                                         first on the velocity potential and
#                                         streamfuncion at 200 and 850 mbs.
      subarea kix kixs 
      subarea kiy kiys 
      subarea kidif kidifs 
      subarea psix psixs 
      subarea psiy psiys 
      subarea psidif psidifs 
      rm kix kiy kidif psix psiy psidif
#     ----------------------------------- now the vertically integrated total
#                                         energy flux streamfunction and velocity
#                                         potential.
      subarea psiny psinys 
      subarea runy runys 
      subarea rvny rvnys 
      subarea psisy psisys 
      subarea rusy rusys 
      subarea rvsy rvsys 
      rm psiny psisy runy rusy rvny rvsy
      subarea chiy chiys 
      subarea duy duys 
      subarea dvy dvys 
      rm chiy duy dvy
      subarea psinx psinxs 
      subarea runx runxs 
      subarea rvnx rvnxs 
      subarea psisx psisxs 
      subarea rusx rusxs 
      subarea rvsx rvsxs 
      rm psinx psisx runx rusx rvnx rvsx
      subarea chix chixs 
      subarea dux duxs 
      subarea dvx dvxs 
      rm chix dux dvx
      subarea psindif psindfs 
      subarea rundif rundifs 
      subarea rvndif rvndifs 
      subarea psisdif psisdfs 
      subarea rusdif rusdifs 
      subarea rvsdif rvsdifs 
      rm psindif psisdif rundif rusdif rvndif rvsdif
      subarea chidif chidifs 
      subarea dudif dudifs 
      subarea dvdif dvdifs 
      rm chidif dudif dvdif
#     ----------------------------------- plot first the velocity potential and
#                                         streamfuncion at 200 and 850 mbs.
      ggplot kixs 
      ggplot kiys 
      ggplot kidifs 
      ggplot psixs 
      ggplot psiys 
      ggplot psidifs 
      rm kixs kiys kidifs psixs psiys psidifs
#     ----------------------------------- now the vertically integrated total
#                                         energy flux streamfunction and velocity
#                                         potential.
      ggplot psinys runys rvnys 
      ggplot psisys rusys rvsys 
      rm psinys psisys runys rusys rvnys rvsys
      ggplot chiys duys dvys 
      rm chiys duys dvys
      ggplot psinxs runxs rvnxs 
      ggplot psisxs rusxs rvsxs 
      rm psinxs psisxs runxs rusxs rvnxs rvsxs
      ggplot chixs duxs dvxs 
      rm chixs duxs dvxs
      ggplot psindfs rundifs rvndifs 
      ggplot psisdfs rusdifs rvsdifs 
      rm psindfs psisdfs rundifs rusdifs rvndifs rvsdifs
      ggplot chidifs dudifs dvdifs 
      rm chidifs dudifs dvdifs
    fi
    if [ "$subarea" != on ] ; then
#     ----------------------------------- plot first the velocity potential and
#                                         streamfuncion at 200 and 850 mbs.
      ggplot kix 
      ggplot kiy 
      ggplot kidif 
      ggplot psix 
      ggplot psiy 
      ggplot psidif 
      rm kix kiy kidif psix psiy psidif
#     ----------------------------------- now the vertically integrated total
#                                         energy flux streamfunction and velocity
#                                         potential.
      ggplot psiny runy rvny 
      ggplot psisy rusy rvsy 
      rm psiny psisy runy rusy rvny rvsy
      ggplot chiy duy dvy 
      rm chiy duy dvy
      ggplot psinx runx rvnx 
      ggplot psisx rusx rvsx 
      rm psinx psisx runx rusx rvnx rvsx
      ggplot chix dux dvx 
      rm chix dux dvx
      ggplot psindif rundif rvndif 
      ggplot psisdif rusdif rvsdif 
      rm psindif psisdif rundif rusdif rvndif rvsdif
      ggplot chidif dudif dvdif 
      rm chidif dudif dvdif
    fi
.   plotid.cdk
.   plot.cdk

end_of_script

cat > Input_Cards <<end_of_data

+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days 
         RUN       = $run
         MODEL1    = $model1 
         MODEL2    = $model2 
0 ENGTEST ----------------------------------------------------- ENGTEST 
. engdat.cdk
. engdat.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        U 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        V 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        U 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        V 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GWTQD.    $lrt$lmt$typ    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GWTQD.    $lrt$lmt$typ    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.            -1.        0.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.            -1.        0.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.            -1.        0.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.            -1.        0.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
COFAGG.   $lon$lat    0 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
COFAGG.   $lon$lat    0 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
COFAGG.   $lon$lat    0 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
COFAGG.   $lon$lat    0 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    CHI1 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    CHI2 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    CHID 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    PSI1 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    PSI2 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    PSID 
. headfram.cdk
if [ "$subarea" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SUBAREA.       $ltb     $lnl     $ltt     $lnr
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 CHI1  200$map     1.E-6     -100.      100.        1.0${b}8
CONTROL $model1.       VELOCITY POTENTIAL AT  200 MBS. UNITS 1.E6*M2/SEC.
GGPLOT.           -1 CHI1  850$map     1.E-6     -100.      100.        1.0${b}8
CONTROL $model1.       VELOCITY POTENTIAL AT  850 MBS. UNITS 1.E6*M2/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 CHI2  200$map     1.E-6     -100.      100.        1.0${b}8
EXPERIMENT $model2.    VELOCITY POTENTIAL AT  200 MBS. UNITS 1.E6*M2/SEC.
GGPLOT.           -1 CHI2  850$map     1.E-6     -100.      100.        1.0${b}8
EXPERIMENT $model2.    VELOCITY POTENTIAL AT  850 MBS. UNITS 1.E6*M2/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 CHID  200$map     1.E-6     -100.      100.        2.0${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) VEL. POT. AT 200 MBS. UNITS 1.E6*M2/SEC.
GGPLOT.           -1 CHID  850$map     1.E-6     -100.      100.        2.0${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) VEL. POT. AT 850 MBS. UNITS 1.E6*M2/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 PSI1  200$map     1.E-6     -360.      360.       30.0${b}8
CONTROL $model1.       STREAMFUNCTION AT  200 MBS. UNITS 1.E6*M2/SEC.
GGPLOT.           -1 PSI1  850$map     1.E-6     -100.      100.        5.0${b}8
CONTROL $model1.       STREAMFUNCTION AT  850 MBS. UNITS 1.E6*M2/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 PSI2  200$map     1.E-6     -360.      360.       30.0${b}8
EXPERIMENT $model2.    STREAMFUNCTION AT  200 MBS. UNITS 1.E6*M2/SEC.
GGPLOT.           -1 PSI2  850$map     1.E-6     -100.      100.        5.0${b}8
EXPERIMENT $model2.    STREAMFUNCTION AT  850 MBS. UNITS 1.E6*M2/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 PSID  200$map     1.E-6     -100.      100.        5.0${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) STRMFUN. AT 200 MBS. UNITS 1.E6*M2/SEC. 
GGPLOT.           -1 PSID  850$map     1.E-6     -100.      100.        5.0${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) STRMFUN. AT 850 MBS. UNITS 1.E6*M2/SEC. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
   GGPLOT         -1   -1   -1$map   1.0E-16    -2.0E2     2.0E2     5.0E02${b}1
  EXPERIMENT $model2. VERT INT TOTAL ENERGY STRM FUNC (NHEM) J/S * 1.0E16 
   VECPLT     1.0E-9     0.0E0     1.0E2    2    2
         ROTATIONAL ENERGY FLUX  J/M/S * 1.0E9
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
   GGPLOT         -1   -1   -1$map   1.0E-16    -2.0E2     2.0E2     5.0E02${b}1
  EXPERIMENT $model2. VERT INT TOTAL ENERGY STRM FUNC (SHEM) J/S * 1.0E16 
   VECPLT     1.0E-9     0.0E0     1.0E2    2    2
         ROTATIONAL ENERGY FLUX  J/M/S * 1.0E9
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
   GGPLOT         -1   -1   -1$map   1.0E-15    -1.0E1     1.0E1     0.1E02${b}8
  EXPERIMENT $model2. VERT INT TOTAL ENERGY POT FUNC   J/S * 1.0E15
   VECPLT     1.0E-6     0.0E0     5.0E2$ncx$ncy
         DIVERGENT ENERGY FLUX  J/M/S * 1.0E6 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
   GGPLOT         -1   -1   -1$map   1.0E-16    -2.0E2     2.0E2     5.0E02${b}1
  CONTROL $model1. VERT INT TOTAL ENERGY STRM FUNC (NHEM) J/S * 1.0E16
   VECPLT     1.0E-9     0.0E0     1.0E2    2    2
         ROTATIONAL ENERGY FLUX  J/M/S * 1.0E9
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
   GGPLOT         -1   -1   -1$map   1.0E-16    -2.0E2     2.0E2     5.0E02${b}1
  CONTROL $model1. VERT INT TOTAL ENERGY STRM FUNC (SHEM) J/S * 1.0E16
   VECPLT     1.0E-9     0.0E0     1.0E2    2    2
         ROTATIONAL ENERGY FLUX  J/M/S * 1.0E9
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
   GGPLOT         -1   -1   -1$map   1.0E-15    -1.0E1     1.0E1     0.1E02${b}8
  CONTROL $model1. VERT INT TOTAL ENERGY POT FUNC   J/S * 1.0E15 
   VECPLT     1.0E-6     0.0E0     5.0E2$ncx$ncy
         DIVERGENT ENERGY FLUX  J/M/S * 1.0E6 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
   GGPLOT         -1   -1   -1$map   1.0E-16    -2.0E1     2.0E1     1.0E02${b}1
  RUN $run DAYS $days. (EXP-CONT) VERT INT ENERGY STRM FUNC (NHEM) J/S*1.0E16
   VECPLT     1.0E-9     0.0E0     2.0E1    2    2
         ROTATIONAL ENERGY FLUX  J/M/S * 1.0E9
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
   GGPLOT         -1   -1   -1$map   1.0E-16    -2.0E1     2.0E1     1.0E02${b}1
  RUN $run DAYS $days. (EXP-CONT) VERT INT ENERGY STRM FUNC (SHEM) J/S*1.0E16
   VECPLT     1.0E-9     0.0E0     2.0E1    2    2
         ROTATIONAL ENERGY FLUX  J/M/S * 1.0E9
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
   GGPLOT         -1   -1   -1$map   1.0E-14    -2.0E1     2.0E1     1.0E02${b}8
  RUN $run DAYS $days. (EXP-CONT) VERT INT ENERGY POT FUNC   J/S*1.0E14 
   VECPLT     1.0E-5     0.0E0     2.0E3$ncx$ncy
         DIVERGENT ENERGY FLUX  J/M/S * 1.0E5 
. tailfram.cdk

end_of_data

. endjcl.cdk


