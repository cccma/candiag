#deck flxstat3
jobname=flxstat3 ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<'end_of_script'
#
#   save convective precipitation and precipitable water,  
#   PBL height, and top of convection. 
#   remove use of parameters tssave, ggsave, zxsave, isgg, and isbeg;
#      remove use of zxlook;
#      remove use of sfcstat.cdk
#   replace =df newmod option with =df gcm2+.
#                  flxstat3            gjb., ec. oct 3/96 - dl.
#                                      rh feb. 5/98 
#   ---------------------------------- sfc fluxes. data stored as summed
#                                      fluxes over isgg time steps for non-
#                                      windstress fields and over isbeg time
#                                      steps for windstress fields.

.     ggfiles.cdk

#   ---------------------------------- mean pcp rate, surface moisture flux,
#                                      and surface heat flux.
ccc   select npakgg pcp qfs hfs
ccc   select npakgg pwat pcpc qevp
ccc   select npakgg pblh tcv  dr

      for x in pcp qfs hfs pwat pcpc qevp pblh tcv dr 
      do
          ln -s $x x
          timavg  x  tx
          zonavg  tx ztx

          xsaveup new_gp tx
          xsaveup new_xp ztx
          rm x $x tx ztx
      done

#   ---------------------------------- stresses.
ccc   select npakgg ufs vfs
#                                      average of stress amplitude.
      ke      ufs vfs a
      xlin    a   b
      sqroot  b   amp
      timavg  amp tamp
      xsaveup new_gp  tamp
      rm a b amp tamp
#                                      #sfc stresses - taux, tauxy.
      for x in ufs vfs
      do
          ln -s $x x
          timavg  x  tx
          zonavg  tx ztx

          xsaveup new_gp tx
          xsaveup new_xp ztx
          rm x $x tx ztx
      done

      if [ "$gcm2plus" = on ] ; then
#   ---------------------------------- "ocean" stresses.
ccc      select npakgg oufs ovfs
         rm npakgg
#                                      average of stress amplitude.
         ke      oufs ovfs a
         xlin    a    b
         sqroot  b    amp
         timavg  amp  tamp
         xsaveup new_gp    tamp
         rm a b amp tamp

         for x in oufs ovfs
         do
            ln -s $x x
            timavg  x  tx
            zonavg  tx ztx

            xsaveup new_gp tx
            xsaveup new_xp ztx
            rm x $x tx ztx
         done
      fi

#   ---------------------------------- save results.
      access oldgp ${flabel}gp
      xjoin oldgp new_gp newgp
      save newgp ${flabel}gp
      delete oldgp

      access oldxp ${flabel}xp
      xjoin oldxp new_xp newxp
      save newxp ${flabel}xp
      delete oldxp

end_of_script

cat > Input_Cards <<end_of_data
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days
         RUN       = $run
         FLABEL    = $flabel
         MODEL1    = $model1
         T1        = $t1
         T2        = $t2
         T3        =      $t3
         G1        = $g1
         G2        = $g2
         G3        =      $g3
0 FLXSTAT2 ------------------------------------------------------------ FLXSTAT2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME  PCP  QFS  HFS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME PWAT PCPC QEVP
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME PBLH  TCV   DR
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         PCP
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (PCP)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         QFS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (QFS)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         HFS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (HFS)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         PWAT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (PWAT)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         PCPC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (PCPC)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         QEVP
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (QEVP)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         PBLH
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (PBLH)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         TCV
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (TCV)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         DR 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (DR)  
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $g1 $g2 $g3 LEVS    1    1 NAME  UFS  VFS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN              2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         TAUA
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         UFS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (UFS)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         VFS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (VFS)
if [ "$gcm2plus" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT NPAKGG   $g1 $g2 $g3 LEVS    1    1 NAME OUFS OVFS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN              2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         OTAUA
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         OUFS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (OUFS)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         OVFS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         (OVFS)
fi
end_of_data

. endjcl.cdk
