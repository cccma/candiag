#deck plotmix
jobname=plotmix ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<end_of_script


#   Replace -273. by -273.16
#                  plotmix             ml,ec,dl - Nov 21/00.
#   ---------------------------------- standard plots.
#
#                                      observed pmsl  pcp  and olr are used as
#                                      comparisons for the model data.

      libncar plunit=$plunit
.     plotid.cdk

      access gpfile  ${flabel}gp
      access xpfile  ${flabel}xp
      access obsfile $obsfile

#   ---------------------------------- surface plots.
#
#                                      pmsl.
#
#                                      model data.
      xfind gpfile tpmsl
      ggplot tpmsl
      zonavg tpmsl ztpmsl
      rm tpmsl
#                                      observed data.
      xfind obsfile tpmsl
      zonavg tpmsl ztpmslo

      joinup plts ztpmsl ztpmslo
      xmplot plts
      rm tpmsl ztpmslo ztpmsl plts

#                                      pcp  convert to mm/day .
#
#                                      model data.
      xfind gpfile tpcp1
      xlin tpcp1 tpcp
      ggplot tpcp
      globavg tpcp output=dummy
      zonavg tpcp ztpcp
      rm tpcp1 tpcp
#                                      observed data.
      xfind obsfile tpcp1
      xlin tpcp1 tpcpo
      zonavg tpcpo ztpcpo

      joinup plts ztpcp ztpcpo
      xmplot plts
      rm tpcp1 tpcpo ztpcpo ztpcp plts

#                                      ground temperature.
      xfind gpfile tgt
      ggplot tgt
      rm tgt

#                                      screen temperature.
      xfind gpfile tst
      ggplot tst
      globavg tst output=dummy
      rm tst

#   ---------------------------------- zonal cross-section plots.
#
#                                      relative humidity.
      xfind xpfile rhr
      zxplot rhr
      rm rhr

#                                      cloudiness.
      xfind xpfile cldr
      zxplot cldr
      rm cldr

#                                      total overlapped cloudiness.
      xfind gpfile ttcld
      zonavg ttcld zttcld
      ggplot ttcld
      xplot zttcld
      globavg ttcld output=dummy
      rm ttcld zttcld
#                                      output global averages.
      txtplot input=dummy
      rm dummy
#                                      local planetary albedo.
#
#                                      model data.
      xfind gpfile tlpa
      zonavg tlpa ztlpa
#                                      observed data.
      xfind obsfile tolpa
      fmask tolpa mask
      mlt tolpa mask mtolpa
      rzonavg mtolpa mask ztolpa

      joinup plts ztlpa ztolpa
      xmplot plts
      rm tlpa ztlpa tolpa ztolpa plts mask mtolpa

#   ---------------------------------- outgoing longwave radiation.
#
#                                      model data.
      xfind gpfile tflg
      xfind gpfile tfla
      add tfla tflg tflag
      zonavg tflag ztflag
      rm tfla tflg tflag
#                                      observed data.
      xfind obsfile olr
      xlin olr tolr
      fmask tolr mask
      mlt tolr mask mtolr
      rzonavg mtolr mask ztolr

      joinup plts ztflag ztolr
      xmplot plts
      rm olr tolr ztolr ztflag plts mask mtolr

#                                      various means.
      xfind xpfile zdel
      xfind xpfile ur
      xfind xpfile tr
      xfind xpfile vr
      xlin tr tc

      mlt vr zdel dv
      zxpsi dv psi

      zxplot ur
      zxplot tc
      zxplot psi
      rm zdel ur vr tr tc dv psi

#                                      variances.
      xfind xpfile us2r
      xfind xpfile up2r
      sqroot us2r sdus
      sqroot up2r sdup
      zxplot sdus
      zxplot sdup
      rm sdus sdup

      xfind xpfile vs2r
      xfind xpfile vp2r
      sqroot vs2r sdvs
      sqroot vp2r sdvp
      zxplot sdvs
      zxplot sdvp
      rm sdvs sdvp

#                                      kinetic energy.
      add us2r up2r a
      add vs2r vp2r b
      add a b eke2
      xlin eke2 eke
      zxplot eke
      rm a b eke2 eke us2r up2r vs2r vp2r

#                                      uv transport.
      xfind xpfile usvsr
      xfind xpfile upvpr
      zxplot usvsr
      zxplot upvpr
      rm usvsr upvpr

#                                      tv transport.
      xfind xpfile tsvsr
      xfind xpfile tpvpr
      zxplot tsvsr
      zxplot tpvpr
      rm tsvsr tpvpr xpfile

#   ---------------------------------- surface energy fluxes.
#                                      sign - positive downward.

      xfind gpfile gc
      xfind gpfile hfx
      xfind gpfile qfx
      xfind gpfile balg

      xlin qfx lf
      xlin hfx hf

      zonavg hf zhf
      zonavg lf zlf
      zonavg balg zbalg

#                                      total fluxes.
      add balg hf aa
      add aa lf net
      zonavg net znet
      joinup zflux znet zbalg zlf zhf
      rm aa znet zhf zlf zbalg hfx
#                                      land and ocean separately
#                                      ocean =1  non-ocean =0.
      fmask gc mi
      fmask gc ml
      add ml mi mil
      xlin mil mo
      zonavg mo zmo
      zonavg mil zmil
      fpow zmil izmil
      globavg mo gmo
      rm mi ml gc mil zmil

#                                      ocean fluxes.
      mlt hf mo hfa
      mlt lf mo lfa
      mlt balg mo balga
      add balga hfa aa
      add aa lfa neta
      rm aa

      rzonavg hfa mo zhfo
      rzonavg lfa mo zlfo
      rzonavg balga mo zbalgo
      rzonavg neta mo zneto
      joinup zfluxo zneto zbalgo zlfo zhfo
      rm zneto zhfo zlfo zbalgo mo

#                                      land/ice fluxes.
      gmlt zfluxo zmo fluxa
      sub zflux fluxa fluxb
      gmlt fluxb izmil zfluxl
      rm fluxa fluxb izmil zmo

#                                      global averages total and ocean.
      div neta gmo neto
      div balga gmo balgo
      div lfa gmo lfo
      div hfa gmo hfo
      rm neta balga lfa hfa gmo

#                                      balance at top and bottom.

#                                      model data for balance at top.
      xfind gpfile balt
      zonavg balt zbalt
#                                      observed data for balance at top.
      xfind obsfile obalt
      fmask obalt mask
      mlt obalt mask mobalt
      rzonavg mobalt mask zobalt

      joinup plts zbalt zobalt
      xmplot plts
      rm obsfile obalt zbalt zobalt plts mask mobalt

      xfind gpfile beg
      xfind gpfile pcp

      sub pcp qfx pme
      xlin pme lpme
      sub balt beg nete
      rm gpfile qfx pcp pme

      globavg nete  output=dummy
      globavg beg   output=dummy
      globavg balt  output=dummy
      globavg lpme  output=dummy
      txtplot input=dummy
      rm nete beg balt lpme dummy

#                                      global surface fluxes.
      globavg net   output=dummy
      globavg balg  output=dummy
      globavg lf    output=dummy
      globavg hf    output=dummy

#                                      ocean surface fluxes.
      globavg neto   output=dummy
      globavg balgo  output=dummy
      globavg lfo    output=dummy
      globavg hfo    output=dummy
      txtplot input=dummy
      rm neto balgo lfo hfo dummy

#   ---------------------------------- plot out fluxes.

      xmplot zflux
      xmplot zfluxo
      xmplot zfluxl

      ggplot balg
      ggplot lf
      ggplot hf
      rm hf lf balg zflux zfluxo zfluxl
.     plotid.cdk
.     plot.cdk

end_of_script

cat > Input_Cards <<end_of_data
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days
         RUN       = $run
         FLABEL    = $flabel
         OBSFILE   = $obsfile
         OBSDAY    = $obsday
0 PLOTMIX -------------------------------------------------------------- PLOTMIX
. headfram.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        PMSL
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.            0 NEXT    1$map        1.      900.     1100.        5.0${b}8
    RUN $run.  DAYS $days.   MSL PRESSURE PMSL.  UNITS MB.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        PMSL*${obsday}*NMC (MB)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XMPLT PMSL         0 NEXT    1      950.     1050.    1    2
   RUN $run.  DAYS $days.  MODEL AND OBS MSL PRESSURE.  UNITS MB.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        PCP
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.         86400.        0.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT PCP         0 NEXT    1    0        1.        0.       50.        2.0${b}8
   RUN $run.  DAYS $days.  PRECIP RATE.  UNITS MM/DAY.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        PCP*${obsday}*JAEGER (KG/M2-S)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.         86400.        0.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XMPLOT PCP         0 NEXT    1        0.       10.    1    2
   RUN $run.  DAYS $days.  MODEL AND OBS PRECIP RATE.  UNITS MM/DAY.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         GT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.            0 NEXT    1    0        1.     -100.       50.        5.0${b}8
  RUN $run.  DAYS $days.   GROUND TEMPERATURE.  UNITS  DEG C.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         ST
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.            0 NEXT    1    0        1.     -100.       50.        5.0${b}8
  RUN $run.  DAYS $days.   SCREEN TEMPERATURE.  UNITS  DEG C.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (RHC)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0   -1     1.0E2        0.      100.     1.0E1    0$kin
  RUN $run.  DAYS $days.  RELATIVE HUMIDITY (RHC)R.  UNITS PERCENTAGE.
                 90.      -90.     $m01     1000.    0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (SCLD)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0   -1        1.        0.        1.     1.E-1    0$kin
  RUN $run.  DAYS $days.  CLOUDINESS (SCLD)R.
                 90.      -90.     $m01     1000.    0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        CLDT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT            -1 CLDT   -1    0        1.        0.        1.     2.E-10${b}8
    RUN $run.  DAYS $days.    TOTAL CLOUDINESS.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT CLDT         0 NEXT    1        0.        1.    1        0.
   RUN $run. DAYS $days.   TOTAL CLOUDINESS.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LPA
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LPA*${obsday}*ERBE (FRACTION)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
FMASK.               NEXT   LT        2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XMPLOT LPA         0 NEXT    1        0.        1.    1    2
   RUN $run. DAYS $days. (LOCAL) PLANETARY ALBEDO.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        FLG
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        FLA
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        OLR*${obsday}*ERBE (W/M2)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.            -1.        0.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
FMASK.               NEXT   GT     -500.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XMPLOT OLR         0 NEXT    1     -500.        0.    1    2
   RUN $run. DAYS $days. MODEL AND OBS NET TERRESTRIAL FLUX (OLR). UNITS W/M-2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        ($d)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (U)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (T)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (V)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.             1.   -273.16
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.       U    0    0$lxp        1.     -150.      150.        5.$kax$kin
  RUN $run.  DAYS $days.  ZONAL VELOCITY (U)R.  UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp        1.     -140.      100.       10.$kax$kin
  RUN $run.  DAYS $days.  TEMPERATURE (T)R.  UNITS DEG C.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.     PSI    0    0$lxp    1.E-10    -2.0E1     3.0E1     1.0E0    0$kin
  RUN $run.  DAYS $days.  MASS STREAM FUNCTION PSI.  UNITS 1.E10*KG/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (U*U*)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (U"U")R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    U*U*    0    0$lxp        1.        0.      500.        2.$kax$kin
  RUN $run.  DAYS $days.  STANDING EDDY SD  ((U*U*)R.)**0.5)  UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    U"U"    0    0$lxp        1.        0.      500.        2.$kax$kin
  RUN $run.  DAYS $days.  TRANSIENT EDDY SD  ((U+U+)R.)**0.5)  UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (V*V*)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (V"V")R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    V*V*    0    0$lxp        1.        0.      500.        2.$kax$kin
  RUN $run.  DAYS $days.  STANDING EDDY SD  ((V*V*)R.)**0.5)  UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    V"V"    0    0$lxp        1.        0.      500.        2.$kax$kin
  RUN $run.  DAYS $days.  TRANSIENT EDDY SD  ((V+V+)R.)**0.5)  UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.          0.5E0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0$lxp    1.0E-1        5.      500.       5.0$kax$kin
  RUN $run.  DAYS $days.   EDDY KINETIC ENERGY (KE)R.  UNITS 10*(M/SEC)2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (U*V*)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (U"V")R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    U*V*    0    0$lxp        1.     -300.      300.       5.0$kax$kin
  RUN $run.  DAYS $days.  STANDING EDDY TRANSPORT (U*V*)R.  UNITS M2/SEC2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    U"V"    0    0$lxp        1.     -300.      300.        5.$kax$kin
  RUN $run.  DAYS $days.  TRANSIENT EDDY TRANSPORT (U+V+)R.  UNITS M2/SEC2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (T*V*)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (T"V")R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    T*V*    0    0$lxp        1.     -300.      300.       5.0$kax$kin
  RUN $run.  DAYS $days.  STANDING EDDY TRANSPORT (T*V*)R.  UNITS DEG*M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    T"V"    0    0$lxp        1.     -300.      300.        5.$kax$kin
  RUN $run.  DAYS $days.  TRANSIENT EDDY TRANSPORT (T+V+)R.  UNITS DEG*M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        GC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        HFS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        QFS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        BALG
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.       -2.501E6
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           -1.0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
FMASK.               NEXT   GT       0.5
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
FMASK.               NEXT   LT      -0.5
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.            -1.        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
FPOW.            -1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        BALT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        BALT*${obsday}*ERBE (W/M2)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
FMASK.               NEXT   LT      200.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XMPLOTBALT         0 NEXT    1     -200.      200.    1    2
   RUN $run. DAYS $days. MODEL AND OBS BALANCE AT THE TOP (BALT). UNITS W/M-2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        BEG
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        PCP
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.        2.501E6
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XMPLOT.            0 NEXT    1     -250.      250.         4
   RUN $run.  DAYS $days.  NET,RAD,LATENT,SENSBLE HEAT FLUX.  UNITS W/M2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XMPLOT.            0 NEXT    1     -250.      250.         4
   RUN $run.  DAYS $days.  NET,RAD,LAT,SENS HEAT FLUX (OCEANS).   W/M2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XMPLOT.            0 NEXT    1     -250.      250.         4
   RUN $run.  DAYS $days.  NET,RAD,LAT,SENS HEAT FLUX (LAND/ICE).   W/M2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.            0 NEXT    1    0        1.     -500.      500.       25.0${b}8
   RUN $run. DAYS $days.   SURFACE RADIATIVE FLUX. UNITS W/M2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.            0 NEXT    1    0        1.     -500.      500.       25.0${b}8
   RUN $run.  DAYS $days.  SURFACE LATENT HEAT FLUX.   UNITS W/M2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.            0 NEXT    1    0        1.     -500.      500.       25.0${b}8
   RUN $run.  DAYS $days.  SURFACE SENSIBLE HEAT FLUX.   UNITS W/M2.
. tailfram.cdk
end_of_data

. endjcl.cdk
