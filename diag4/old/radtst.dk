#deck radtst
jobname=radt ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<end_of_script

#                   radtst              m lazare. october 25/85.
#   ----------------------------------- computes the standard t-test for
#                                       significance of the deviation of a
#                                       statistic from its pooled control
#                                       state  and plots out the result.
#                                       the percent area of significance on the
#                                       t-test plots are also calculated.
#                                       this test is done for radiation
#                                       statistics.
#                                       model1 and letter x refer to pooled
#                                       control run  while model2 and letter y
#                                       refer to the experimental run.
#
    access filex ${model1}gp
    access filey ${model2}gp
    libncar plunit=$plunit 
.   plotid.cdk
#   ----------------------------------- make initial null standard deviation file
    xfind filex alb 
    xlin alb null 
    rm alb xlin
#   ----------------------------------- find appropriate critical value of t.
    critt tc 
    xlin null tcrit input=tc 
    rm xlin
    xlin null nx 
    xlin null ny 
    xlin nx nx1 
    xlin ny ny1 
    add nx ny b1 
    mlt nx ny b2 
    div b1 b2 b3 
    sqroot b3 fact 
    add nx1 ny1 dof 
    rm tc nx ny b1 b2 b3
#   ----------------------------------- radiation balance at the ground  balg .
.   ttstgp.cdk
    ggplot num 
    ggplot tratio 
    rm x y num tratio
#   ----------------------------------- radiation balance at the top of the
#                                       atmosphere  balt .
.   ttstgp.cdk
    ggplot num 
    ggplot tratio 
    rm x y num tratio
#   ----------------------------------- solar flux absorbed at surface  fsg .
.   ttstgp.cdk
    ggplot num 
    ggplot tratio 
    rm x y num tratio
#   ----------------------------------- solar flux absorbed by atm  fsa .
.   ttstgp.cdk
    ggplot num 
    ggplot tratio 
    rm x y num tratio
#   ----------------------------------- ground albedo  salb .
.   ttstgp.cdk
    ggplot num 
    ggplot tratio 
    rm x y num tratio
#   ----------------------------------- net terrestial flux at surface  flg .
.   ttstgp.cdk
    ggplot num 
    ggplot tratio 
    rm x y num tratio
#   ----------------------------------- terrestial flux absorbed by atmosphere
#                                        fla .
.   ttstgp.cdk
    ggplot num 
    ggplot tratio 
    rm nx1 ny1 dof fact tcrit filex filey
    rm x y num tratio
    if [ "$splot" = on ] ; then
      ggplot stotal 
      rm stotal
    fi
.   plotid.cdk
.   plot.cdk

end_of_script

cat > Input_Cards <<end_of_data

+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days 
         RUN       = $run
         MODEL1    = $model1 
         MODEL2    = $model2 
0 RADTST  ----------------------------------------------------- RADTST
. headfram.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        SALB
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           0.E0      0.E0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
CRITT.       $alpha     $xyr     $yyr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           0.E0     $xyr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           0.E0     $yyr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           1.E0     -1.E0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           1.E0     -1.E0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        BALG
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(BALG) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        BALG
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(BALG) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.   SBALG 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E0    -80.E0     80.E0      5.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR BALG. UNITS W/M2. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT    1$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR BALG.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        BALT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(BALT) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        BALT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(BALT) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.   SBALT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT   -1$map      1.E0    -80.E0     80.E0      5.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR BALT. UNITS W/M2. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT   -1$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR BALT.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        FSG 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(FSG)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        FSG 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(FSG)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    SFSG 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT   -1$map      1.E0    -80.E0     80.E0      5.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR FSG. UNITS W/M2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT   -1$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR FSG. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        FSA 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(FSA)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        FSA 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(FSA)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    SFSA 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E0    -80.E0     80.E0      5.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR FSA. UNITS W/M2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT    1$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR FSA. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        SALB
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(SALB) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        SALB
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(SALB) 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.   SSALB 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map        1.        0.        1.       0.20${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR SALB. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT    1$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR SALB.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        FLG 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(FLG)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        FLG 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(FLG)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    SFLG 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E0    -80.E0     80.E0      5.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR FLG. UNITS W/M2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT    1$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR FLG. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        FLA 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(FLA)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        FLA 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(FLA)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    SFLA 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E0    -80.E0     80.E0      5.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR FLA. UNITS W/M2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT    1$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR FLA. 
if [ "$splot" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1SBALG    1$map      1.E0      0.E0     20.E0      1.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF BALG. UNITS W/M2.
GGPLOT.           -1SBALT   -1$map      1.E0      0.E0     20.E0      1.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF BALT. UNITS W/M2.
GGPLOT.           -1 SFSG   -1$map      1.E0      0.E0     20.E0      1.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF FSG. UNITS W/M2. 
GGPLOT.           -1 SFSA    1$map      1.E0      0.E0     20.E0      1.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF FSA. UNITS W/M2. 
GGPLOT.           -1SSALB    1$map      1.E0      0.E0     20.E0      1.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF SALB.
GGPLOT.           -1 SFLG    1$map      1.E0      0.E0     20.E0      1.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF FLG. UNITS W/M2. 
GGPLOT.           -1 SFLA    1$map      1.E0      0.E0     20.E0      1.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF FLA. UNITS W/M2. 
fi
. tailfram.cdk

end_of_data

. endjcl.cdk


