#deck sfctst1
jobname=sfct1 ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<end_of_script

#   join gb file to gp file.
#                   sfctst1             m lazare. jun 22/90 - ec.
#   ----------------------------------- computes the difference between an
#                                       experimental and a control run  each
#                                       comprised of one independant
#                                       simulation  and plots out the result.
#                                       this is done for statistics at
#                                       the surface.
#                                       model1 and letter x refer to control
#                                       run  while model2 and letter y refer
#                                       to the experimental run.
#
    access gpfile ${model1}gp
    access gbfile ${model1}gb na
    joinup filex gpfile gbfile 
    rm gpfile gbfile
# 
    access gpfile ${model2}gp
    access gbfile ${model2}gb na
    joinup filey gpfile gbfile 
    rm gpfile gbfile
# 
    libncar plunit=$plunit 
.   plotid.cdk
#   ----------------------------------- mean sea level pressure pmsl .
.   test1gp.cdk
    ggplot num 
    rm x y num
#   ----------------------------------- precipitation rate pcp .
.   test1gp.cdk
    ggplot num 
    rm x y num
#   ----------------------------------- surface moisture flux qfs .
.   test1gp.cdk
    ggplot num 
    rm x y num
#   ----------------------------------- surface heat flux hfs .
.   test1gp.cdk
    ggplot num 
    rm x y num
    if [ "$nobeg" != on ] ; then
#     ----------------------------------- surface energy balance beg .
.     test1gp.cdk
      ggplot num 
      rm x y num
    fi
#   ----------------------------------- zonal surface wind stress ufs .
.   test1gp.cdk
    newnam num dufs 
    rm x y num
#   ----------------------------------- meridional surface wind stress vfs .
.   test1gp.cdk
    newnam num dvfs 
    rm x y num
#   ----------------------------------- surface stress amplitude taua .
#                                       plot out the vector surface wind stress
#                                       difference as well.
.   test1gp.cdk
    ggplot num dufs dvfs 
    rm x y num dufs dvfs
#   ----------------------------------- lowest model level zonal wind lu .
.   test1gp.cdk
    newnam num du 
    square du du2 
    rm x y num
#   ----------------------------------- lowest model level meridional wind lv .
#                                       plot the vector wind difference.
.   test1gp.cdk
    newnam num dv 
    square dv dv2 
    add du2 dv2 dav2 
    sqroot dav2 dav 
    newnam dav amp 
    rm dav2 du2 dv2 dav
    ggplot amp du dv 
    rm amp du dv
    rm x y num
#   ----------------------------------- lowest model level temperature lt .
.   test1gp.cdk
    ggplot num 
    rm x y num
#   ----------------------------------- lowest model level specific humidity lq .
.   test1gp.cdk
    ggplot num 
    rm x y num
.   plotid.cdk
.   plot.cdk

end_of_script

cat > Input_Cards <<end_of_data

+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days 
         RUN       = $run
         MODEL1    = $model1 
         MODEL2    = $model2 
0 SFCTST1 -------------------------------------------------------------- SFCTST1
. headfram.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        PMSL
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        PMSL
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E0    -40.E0     40.E0      2.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR PMSL. UNITS MBS.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        PCP 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        PCP 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E5    -20.E0     20.E0    25.E-10${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR PCP. UNITS 1.E-5*KG/(M2*SEC). 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        QFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        QFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E6    -40.E0     40.E0     10.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR QFS. UNITS 1.E-6*KG/(M2*SEC). 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        HFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        HFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E0   -100.E0    100.E0     25.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR HFS. UNITS W/M2.
if [ "$nobeg" != on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        BEG 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        BEG 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E0   -100.E0    100.E0     25.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR BEG. UNITS W/M2.
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        UFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        UFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    DUFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        VFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        VFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    DVFS 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        TAUA
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        TAUA
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT    1$map      1.E2      -80.       80.        4.2${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR SFC WIND STRESS. UNITS 1.E-2*N/M2.
                1.E2        0.       10.$ncx$ncy
                        VECPLOT OF DIFFERENCE IN (TAUX,TAUY). 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LU
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LU
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     DLU 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LV
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LV
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     DLV 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     AMP 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1  AMP   -1$map        1.        0.       30.        2.2${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR LOWEST MODEL LEVEL WIND. UNITS M/SEC
                  1.        0.        5.$ncx$ncy
                        VECPLOT OF (U,V). 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT   -1$map      1.E0    -40.E0     40.E0      2.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR LOWEST MODEL LEVEL T. UNITS DEGK. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LQ
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        LQ
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 NEXT   -1$map      1.E3    -40.E0     40.E0      2.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR LOWEST MODEL LEVEL Q. UNITS G/KG. 
. tailfram.cdk

end_of_data

. endjcl.cdk


