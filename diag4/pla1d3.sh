#!/bin/sh
#
#
#
#                  pla1d               KS - Dec 05/13 -------------pla1d
#   ---------------------------------- Compute diagnostic results for
#                                      PLA calculations.
#
# -------------------------------------------------------------------------
#
#  Output variables:
#
#  1) General
#
#  ALTI : Altitude
#
#  2) CDNC-related
#
#  ZCDN : Cloud droplet number concentration
#  CCN  : Cloud droplet number concentration (in-cloud)
#  CC02 : CCN concentration at 0.2% supersaturation
#  CC04 : CCN concentration at 0.4% supersaturation
#  CC08 : CCN concentration at 0.8% supersaturation
#  CC16 : CCN concentration at 1.6% supersaturation
#  WPAR : Parcel vertical velocity in calculation of CDNC
#  RCRI : Critical radius in calculation of CDNC
#  SUPS : Maximum supersaturation in calculation of CDNC
#  CCNE : Cloud droplet number concentration diagnosed based on aerosol 
#         mass concentrations (semi-empirical parameterization)
#  VCNE : Vertical integral of CCNE
#  VCCN : Vertical integral of CCN
#
#  3) Aerosol microphysics-related
#
#  VOAE : Emission flux of particulate organic matter
#  VBCE : Emission flux of black carbon
#  VASE : Emission flux of ammonium sulphate
#  VMDE : Emission flux of mineral dust
#  VSSE : Emission flux of sea salt
#  VOAG : Gas-to-particle conversion flux of particulate organic matter
#  VBCG : Gas-to-particle conversion flux of black carbon
#  VASG : Gas-to-particle conversion flux of ammonium sulphate
#  VMDG : Gas-to-particle conversion flux of mineral dust
#  VSSG : Gas-to-particle conversion flux of sea salt
#  VOAW : Stratiform cloud deposition flux of particulate organic matter
#  VBCW : Stratiform cloud deposition flux of black carbon
#  VASW : Stratiform cloud deposition flux of ammonium sulphate
#  VMDW : Stratiform cloud deposition flux of mineral dust
#  VSSW : Stratiform cloud deposition flux of sea salt
#  VOAC : Cumulus cloud deposition flux of particulate organic matter
#  VBCC : Cumulus cloud deposition flux of black carbon
#  VASC : Cumulus cloud deposition flux of ammonium sulphate
#  VMDC : Cumulus cloud deposition flux of mineral dust
#  VSSC : Cumulus cloud deposition flux of sea salt
#  VOAD : Dry deposition and graviational settling flux of particulate organic matter
#  VBCD : Dry deposition and graviational settling flux of black carbon
#  VASD : Dry deposition and graviational settling flux of ammonium sulphate
#  VMDD : Dry deposition and graviational settling flux of mineral dust
#  VSSD : Dry deposition and graviational settling flux of sea salt
#  HENR : Henry's law constant for sulphur dioxide in cloud liquid water
#  O3FR : Fraction of ozone oxidation in total in-cloud oxidation of sulphur
#  HPFR : Fraction of hydrogen peroxide oxidation in total in-cloud oxidation of sulphur
#  VASI : Ammonium sulphate in-cloud oxidation flux 
#  VAS1 : Stratiform cloud in-cloud scavenging flux of ammonium sulphate
#  VAS2 : Stratiform cloud below-cloud scavenging flux of ammonium sulphate
#  VAS3 : Stratiform cloud below-cloud re-evaporation flux of ammonium sulphate
#  SCND : Gas-to-particle condensation rate of ammonium sulphate 
#  VSCD : Vertical integral of SCND
#  SNCN : Gas-to-particle nucleation rate of aerosol number
#  VNCN : Vertical integral of SNCN
#  SSUN : Gas-to-particle nucleation rate of ammonium sulphate
#  VASN : Vertical integral of SSUN
#  SGSP : Gas-phase production rate of sulphuric acid
#  VGSP : Vertical integral of SGSP 
#
.   ggfiles.cdk
#
# ---------------------------------------- select input fields.
#
   varl3d='ALTI SNCN SSUN SCND SGSP CC02 CC04 CC08 CC16 WPAR N100 N200 W100 W200 ACAS ACOA ACBC ACSS ACMD'
   varl3dx='N00 N20 N50 W00 W20 W50'
   varl3dc='ZCDN CCNE RCRI SUPS HENR O3FR HPFR'
   varl3dcx='CCN'
   varl2d='VNCN VASN VSCD VGSP VOAE VBCE VASE VMDE VSSE VOAW VBCW VASW VMDW VSSW 
           VOAD VBCD VASD VMDD VSSD VOAG VBCG VASG VMDG VSSG VOAC VBCC VASC VMDC
           VSSC VASI VAS1 VAS2 VAS3 VCNE VCCN'
#
   for VL in $varl3d ; do
     echo "SELECT          $r1 $r2 $r3     -9001 1000      $VL" | ccc select npakgg $VL
   done
   for VL in $varl3dx ; do
     echo "SELECT          $r1 $r2 $r3     -9001 1000       $VL" | ccc select npakgg $VL
   done
   for VL in $varl3dc ; do
     echo "SELECT          $r1 $r2 $r3     -9001 1000      $VL" | ccc select npakgg $VL
   done
   for VL in $varl3dcx ; do
     echo "SELECT          $r1 $r2 $r3     -9001 1000       $VL" | ccc select npakgg $VL
   done
   for VL in $varl2d ; do
     echo "SELECT          $r1 $r2 $r3         1    1      $VL" | ccc select npakgg $VL
   done
#
# ---------------------------------------- repack fields.
#
   for VL in $varl3d ; do
     echo "REPACK.       2" | ccc repack $VL ${VL}RPCK
   done
   for VL in $varl3dx ; do
     echo "REPACK.       2" | ccc repack $VL ${VL}RPCK
   done
   for VL in $varl3dc ; do
     echo "REPACK.       2" | ccc repack $VL ${VL}RPCK
   done
   for VL in $varl3dcx ; do
     echo "REPACK.       2" | ccc repack $VL ${VL}RPCK
   done
   for VL in $varl2d ; do
     echo "REPACK.       2" | ccc repack $VL ${VL}RPCK
   done
#
# ---------------------------------------- time average results.
#
   for VL in $varl3d ; do
     timavg ${VL}RPCK ${VL}T
   done
   for VL in $varl3dx ; do
     timavg ${VL}RPCK ${VL}T
   done
   for VL in $varl2d ; do
     timavg ${VL}RPCK ${VL}T
   done
#
# ---------------------------------------- create masks for proper averaging
#                                          of in-cloud results.
#
   for VL in $varl3dc ; do
     echo "  FMASK.             NEXT   GT        0." | ccc fmask ${VL}RPCK ${VL}MSK
   done
   for VL in $varl3dcx ; do
     echo "  FMASK.             NEXT   GT        0." | ccc fmask ${VL}RPCK ${VL}MSK
   done
#
# ---------------------------------------- time-average in-cloud results.
#
   i2=1
   for VL in $varl3dc ; do
     timavg ${VL}MSK ${VL}MA
     timavg ${VL}RPCK  ${VL}TA
     div ${VL}TA ${VL}MA ${VL}T
     i2=`echo $i2 | awk '{printf "%02i", $0}'` # 2-digit number
     cp ${VL}MA MK${i2}
     i2=`expr $i2 + 1`
   done
   for VL in $varl3dcx ; do
     timavg ${VL}MSK ${VL}MA
     timavg ${VL}RPCK  ${VL}TA
     div ${VL}TA ${VL}MA ${VL}T
     i2=`echo $i2 | awk '{printf "%02i", $0}'` # 2-digit number
     cp ${VL}MA MK${i2}
     i2=`expr $i2 + 1`
   done
#   
# ---------------------------------------- "xsave" results
#
   for VL in $varl3d ; do
     echo "XSAVE.        ${VL}
NEWNAM.    ${VL}" | ccc xsaveup new_gp ${VL}T
   done
   for VL in $varl3dx ; do
     echo "XSAVE.        ${VL}
NEWNAM.     ${VL}" | ccc xsaveup new_gp ${VL}T
   done
   for VL in $varl3dc ; do
     echo "XSAVE.        ${VL}
NEWNAM.    ${VL}" | ccc xsaveup new_gp ${VL}T
   done
   for VL in $varl3dcx ; do
     echo "XSAVE.        ${VL}
NEWNAM.     ${VL}" | ccc xsaveup new_gp ${VL}T
   done
   for VL in $varl2d ; do
     echo "XSAVE.        ${VL}
NEWNAM.    ${VL}" | ccc xsaveup new_gp ${VL}T
   done
   i2=1
   for VL in $varl3dc ; do
     i2=`echo $i2 | awk '{printf "%02i", $0}'` # 2-digit number
     echo "XSAVE.        MK${i2}
NEWNAM.    MK${i2}" | ccc xsaveup new_gp MK${i2}
     i2=`expr $i2 + 1`
   done
   for VL in $varl3dcx ; do
     i2=`echo $i2 | awk '{printf "%02i", $0}'` # 2-digit number
     echo "XSAVE.        MK${i2}
NEWNAM.    MK${i2}" | ccc xsaveup new_gp MK${i2}
     i2=`expr $i2 + 1`
   done
#
#  ----------------------------------- save results.
    access oldgp ${flabel}gp
    xjoin oldgp new_gp newgp
    save newgp ${flabel}gp
    delete oldgp
#    cp new_gp newgp
#    save newgp ${flabel}xt