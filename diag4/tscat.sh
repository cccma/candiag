#!/bin/sh
#
#              tscat                   L Solheim - Mar, 2008
#   ---------------------------------- Concatenate 'ts' files
#
#  Concatenate a sequence of 'ts' files that were created by tseries.dk
#  and, optionally,
#    - plot each time series found in the resulting file
#    - convert each time series to necdf
#

#   ----------------------------------
#   start wall clock
    echo "\nSTART tscat:  "`date`"\n"

# ----------------------------------
# a simple error exit routine
bail(){
    echo "tscat: "$1
    echo "tscat: "$1 >> haltit
    exit 1
}

# ----------------------------------
# Extract variable definitions from a PARM record
pardef(){
  # usage: pardef PARM_txt var1 [var2 ...]

  # Extract particular variable definitions from the PARM record
  # or from the current environment

  # The first arg is the name of a text file containing the parmsub section.
  # All subsequent args are variable names (there must be at least one).
  [ -z "$1" ] && bail "pardef: Missing PARM_txt file name."
  [ -z "$2" ] && bail "pardef: Missing variable name."
  loc_PARM_txt=$1

  [ ! -s $loc_PARM_txt ] && bail "pardef: $loc_PARM_txt is missing or empty"

  rm -f parmsub1 parmsub.x

  # Remove any CPP_I definition that may appear in the parmsub section
  addr1='/^ *## *[Cc][Pp][Pp]_[Ii]_[Ss][Tt][Aa][Rr][Tt]/'
  addr2='/^ *## *[Cc][Pp][Pp]_[Ii]_[Ee][Nn][Dd]/'
  eval sed \'${addr1},${addr2}d\' $loc_PARM_txt > parmsub.x

  # Remove comments and blank lines
  # Remove any lines that source an external script via ". extscript"
  # Remove " +PARM" line inserted by bin2txt, if any
  sed 's/#.*$//; /^ *[+]/d; /^ *\. .*/d; /^ *$/d' parmsub.x > parmsub1

  # Prepend a shebang line
  echo '#!/bin/sh' | cat - parmsub1 > parmsub.x

  # Ensure the resulting script ends with a blank line
  echo 'echo " "' | cat parmsub.x - > parmsub1

  # Make sure parmsub1 and parmsub.x are equivalent and executable
  cat parmsub1 > parmsub.x
  chmod u+x parmsub.x parmsub1

  [ ! -s parmsub.x ] && bail "pardef: Unable to create parmsub.x"

  echo " "
  loc_iarg=1
  while [ $loc_iarg -lt 100 ]; do
    loc_iarg=`expr $loc_iarg + 1`
    eval loc_var=\$$loc_iarg
    [ -z "$loc_var" ] && break

    # Determine parameter values from the parmsub record
    echo 'echo "$'${loc_var}'"' | cat parmsub.x - > parmsub1
    loc_val=`/bin/sh ./parmsub1|tail -1`

    # Create a variable of the same name with "_parm" appended
    # This can be used by the invoking script
    eval ${loc_var}_parm=\'$loc_val\'
    [ -n "$loc_val" ] &&\
      echo "Found in $loc_PARM_txt :: $loc_var = $loc_val"
  done
  rm -f parmsub1 parmsub.x
}

#   ----------------------------------
#   Determine a list of time series files to concatenate.
#   Input file names are found in shell variables with names of the form
#   tscatNN or tscatNNN where NN and NNN are 2 and 3 digit integers.
#   The following loop will look for variables named tscat01, tscat02, ...
#   unitl it finds the first undefined value, at which point it will
#   stop the loop processing. Any tscatNN variables with NN greater than
#   the number associated with the first null value found will be ignored,
#   even if they are defined.
    rm -f TSCAT_FILES_IN
    touch TSCAT_FILES_IN
    nn=0
    while [ $nn -lt 999 ]; do
      if [ $nn -lt 99 ]; then
        nn=`echo $nn|awk '{printf "%2.2d",$1+1}' -`
      else
        nn=`echo $nn|awk '{printf "%3.3d",$1+1}' -`
      fi
      eval tsfile=\$tscat$nn
      [ -z "$tsfile" ] && break
      access TS$nn $tsfile
      echo "TS$nn" >> TSCAT_FILES_IN
      echo "TS$nn == $tsfile"
    done
    [ ! -s TSCAT_FILES_IN ] && bail "Unable to find any ts file names"
    nfiles=`echo $nn|awk '{printf "%d",$1-1}' -`

#   ----------------------------------
#   TS_netcdf flags the creation of a set of netcdf files
#   from data in the concatenated output file
    TS_netcdf=${TS_netcdf:=0}
    XXX=`echo $TS_netcdf|sed 's/ //g'`
    eval TS_netcdf\=$XXX
    [ "$TS_netcdf" = 'on'  ] && eval TS_netcdf\=1
    [ "$TS_netcdf" = 'off' ] && eval TS_netcdf\=0
    [ "$TS_netcdf" = 'yes' ] && eval TS_netcdf\=1
    [ "$TS_netcdf" = 'no'  ] && eval TS_netcdf\=0

#   ----------------------------------
#   TS_nc_add_var flags inclusion of extra variables (e.g. VINF,PARM)
#   in the output netcdf files, if any
    TS_nc_add_var=${TS_nc_add_var:=0}
    XXX=`echo $TS_nc_add_var|sed 's/ //g'`
    eval TS_nc_add_var\=$XXX
    [ "$TS_nc_add_var" = 'on'  ] && eval TS_nc_add_var\=1
    [ "$TS_nc_add_var" = 'off' ] && eval TS_nc_add_var\=0
    [ "$TS_nc_add_var" = 'yes' ] && eval TS_nc_add_var\=1
    [ "$TS_nc_add_var" = 'no'  ] && eval TS_nc_add_var\=0

#   ----------------------------------
#   TS_nc_suffix is a string to add to the end of output netcdf file names
    TS_nc_suffix=${TS_nc_suffix:=''}

#   ----------------------------------
#   TS_file_split flags writing multiple output files with one variable
#   per output file. Each individual file will be created if it does not
#   exist or appended to if it exists
    TS_file_split=${TS_file_split:=0}
    [ "$TS_file_split" = 'on' ]  && eval TS_file_split\=1
    [ "$TS_file_split" = 'off' ] && eval TS_file_split\=0
    [ "$TS_file_split" = 'yes' ] && eval TS_file_split\=1
    [ "$TS_file_split" = 'no' ]  && eval TS_file_split\=0

#   ----------------------------------
#   TS_file_append flags appending files created when TS_file_split=on
#   to any existing file of the same name. If TS_file_append=off then multiple
#   editions of the same file will be created. These multiple editions must
#   then be concatentated to form a single time series later on.
    TS_file_append=${TS_file_append:=1}
    [ "$TS_file_append" = 'on' ]  && eval TS_file_append\=1
    [ "$TS_file_append" = 'off' ] && eval TS_file_append\=0
    [ "$TS_file_append" = 'yes' ] && eval TS_file_append\=1
    [ "$TS_file_append" = 'no' ]  && eval TS_file_append\=0

#   ----------------------------------
#   tscat_split flags writing multiple output files with one variable
#   per output file. Each individual file will be created if it does not
#   exist or appended to if it exists
    tscat_split=${tscat_split:=$TS_file_split}
    [ "$tscat_split" = 'on' ]  && eval tscat_split\=1
    [ "$tscat_split" = 'off' ] && eval tscat_split\=0
    [ "$tscat_split" = 'yes' ] && eval tscat_split\=1
    [ "$tscat_split" = 'no' ]  && eval tscat_split\=0

#   ----------------------------------
#   Always split into individual files when netcdf output is requested
    [ $TS_netcdf -eq 1 ] && tscat_split=1

#   ----------------------------------
#   TS_file_clobber flags overwrite of output file if it exists
    TS_file_clobber=${TS_file_clobber:=0}
    [ "$TS_file_clobber" = 'on' ]  && eval TS_file_clobber\=1
    [ "$TS_file_clobber" = 'off' ] && eval TS_file_clobber\=0
    [ "$TS_file_clobber" = 'yes' ] && eval TS_file_clobber\=1
    [ "$TS_file_clobber" = 'no' ]  && eval TS_file_clobber\=0

#   ----------------------------------
#   TS_plot flags the creation of a set of time series plots
#   from data in the concatenated output file
    TS_plot=${TS_plot:=0}
    XXX=`echo $TS_plot|sed 's/ //g'`
    eval TS_plot\=$XXX
    [ "$TS_plot" = 'on'  ] && eval TS_plot\=1
    [ "$TS_plot" = 'off' ] && eval TS_plot\=0
    [ "$TS_plot" = 'yes' ] && eval TS_plot\=1
    [ "$TS_plot" = 'no'  ] && eval TS_plot\=0

#   ----------------------------------
#   TS_yoff is a year offset that will be added to time axis values
#   on plots created when TS_plot has been set to 'on' (or 'yes' or 1).
    TS_yoff=${TS_yoff:=0}

#   ----------------------------------
#   TS_del_after_cat flags the deletion of all input tscatNN files after
#   they have been concatenated into a single file. This will never happpen
#   unless the output file is saved (ie: tscat_save is set non null).
#   The default is to leave all input files on disk.
    TS_del_after_cat=${TS_del_after_cat:=0}
    XXX=`echo $TS_del_after_cat|sed 's/ //g'`
    eval TS_del_after_cat\=$XXX
    [ "$TS_del_after_cat" = 'on'  ] && eval TS_del_after_cat\=1
    [ "$TS_del_after_cat" = 'off' ] && eval TS_del_after_cat\=0
    [ "$TS_del_after_cat" = 'yes' ] && eval TS_del_after_cat\=1
    [ "$TS_del_after_cat" = 'no'  ] && eval TS_del_after_cat\=0

#   ----------------------------------
#   TS_force_tscat=on tells this script to run tscat even when there is
#   only 1 input file supplied
    TS_force_tscat=${TS_force_tscat:=0}
    XXX=`echo $TS_force_tscat|sed 's/ //g'`
    eval TS_force_tscat\=$XXX
    [ "$TS_force_tscat" = 'on'  ] && eval TS_force_tscat\=1
    [ "$TS_force_tscat" = 'off' ] && eval TS_force_tscat\=0
    [ "$TS_force_tscat" = 'yes' ] && eval TS_force_tscat\=1
    [ "$TS_force_tscat" = 'no'  ] && eval TS_force_tscat\=0

#   ----------------------------------
#   tscat_run=on tells this script to run tscat, or not
    tscat_run=${tscat_run:=1}
    XXX=`echo $tscat_run|sed 's/ //g'`
    eval tscat_run\=$XXX
    [ "$tscat_run" = 'on'  ] && eval tscat_run\=1
    [ "$tscat_run" = 'off' ] && eval tscat_run\=0
    [ "$tscat_run" = 'yes' ] && eval tscat_run\=1
    [ "$tscat_run" = 'no'  ] && eval tscat_run\=0

    [ $nfiles -le 1 ]         && tscat_run=0
    [ $nfiles -gt 1 ]         && tscat_run=1
    [ $TS_force_tscat -eq 1 ] && tscat_run=1
    [ $tscat_split -eq 1 ]    && tscat_run=1

#   ----------------------------------
#   Set command line arguments passed to tsinfo below
    tsinfo_opt="runid=$runid"

#   ----------------------------------
#   Set CCMVal specific tsinfo options
    tscat_ccmval_fid=${tscat_ccmval_fid:=''}
    if [ -n "$tscat_ccmval_fid" ]; then
      tsinfo_opt="$tsinfo_opt --CCMVal fid=$tscat_ccmval_fid"
    fi

#   ----------------------------------
#   Set command line options passed to tscat
    tscat_opts=''
    if [ $TS_plot -eq 1 ]; then
      tscat_opts="$tscat_opts TS_plot=on"
    fi
    if [ $TS_yoff -ne 0 ]; then
      tscat_opts="$tscat_opts TS_yoff=$TS_yoff"
    fi

#    if [ $nfiles -gt 1 -o $TS_force_tscat -eq 1 ]; then
#    if [ ! \( $nfiles -le 1 -a $TS_netcdf -eq 1 \) ]; then
    if [ $tscat_run -eq 1 ]; then

#     ----------------------------------
#     Concatenate all specified 'ts' files into a single file
#     Create certain crvplot input files when TS_plot = 1
#     tscat will create the file NEW_TSFILE
      tscat $tscat_opts flist=TSCAT_FILES_IN out=NEW_TSFILE

      [ ! -s NEW_TSFILE ] && bail "Unable to create output tsfile"

#     ----------------------------------
#     Save the output file
#     Delete all input files (under control of TS_del_after_cat)
      tscat_save=${tscat_save:=''}
      if [ -n "$tscat_save" ]; then

        # Save the 'ts' file output from tscat as $tscat_save
        access oldTSFILE $tscat_save na
        save NEW_TSFILE $tscat_save
        delete oldTSFILE na

        if [ $TS_del_after_cat -eq 1 ]; then
          # Delete all input files named in the tscatNN shell variables
          nn=0
          while [ $nn -lt 999 ]; do
            if [ $nn -lt 99 ]; then
              nn=`echo $nn|awk '{printf "%2.2d",$1+1}' -`
            else
              nn=`echo $nn|awk '{printf "%3.3d",$1+1}' -`
            fi
            eval tsfile=\$tscat$nn
            [ -z "$tsfile" ] && break

            # Make sure the output file does not get deleted when an input
            # file happens to have the same name as the output file
            [ x$tsfile = x$tscat_save ] && continue

            # Delete the input file. Ignore any errors in delete.
            delete TS$nn na
          done

        fi
      fi
    elif [ $nfiles -eq 1 ]; then
      mv TS01 NEW_TSFILE
    else
      bail "nfile=$nfiles is out of range when tscat_run=$tscat_run"
    fi

#   ----------------------------------
#   Split the input time series files into one file per variable found in
#   the input files(s)

    if [ $tscat_split -eq 1 ]; then

      # Clean up all settmp* and PARM*.txt files that were created from the
      # previous invocation of tscat to save on disk space
      rm -f settmp*
      rm -f PARM[0-9]*.txt

      # Invoke tscat on the file output from the previous invocation
      # (with splitpfx defined) to get a set of files, each containing an
      # individual superlabeled set.
      tscat splitpfx=cccset NEW_TSFILE

      # Set command line options for ccc2nc
      TS_ccc2nc_opts=${TS_ccc2nc_opts:=''}

      # cccset_FILES will be an ascii file that contains info about each
      # file create by the previous call to tscat
      nline=0
      while [ $nline -lt 9000 ]; do
        nline=`expr $nline + 1`
        # Read each line from the file cccset_FILES
        this_line=`awk -v nline="$nline" '
          {if (NR==nline) {printf "%s",$0; exit}}
          END {if (nline>NR) {printf "END_OF_FILE"}}' cccset_FILES`
        [ -z "$this_line" ] && continue
        [ "$this_line" = "END_OF_FILE" ] && break

        # Identify info available in cccset_FILES for the current variable
        # curr_nt is the number of time steps for this variable
        curr_nt=`echo $this_line|awk '{printf "%d",$1}' -`
        # curr_kmin is the minimum kount value
        curr_kmin=`echo $this_line|awk '{printf "%d",$2}' -`
        # curr_kmax is the maximum kount value
        curr_kmax=`echo $this_line|awk '{printf "%d",$3}' -`
        # curr_fname is the file name
        curr_fname=`echo $this_line|awk '{printf "%s",$4}' -`
        # curr_slab is the superlabel
        curr_slab=`echo $this_line|awk \
          '{sub(/^.*cccset..... /,"",$0); printf "%s",$0}' -`

        # Ignore HEADER sets
        [ $curr_nt -le 0 ] && continue

        # Determine runid from the current PARM record
        runid=${TS_nc_runid:=''}
        parmrec=`echo $curr_fname|sed 's/^cccset\(.*\)/PARM\1.txt/'`
        if [ -s "$parmrec" ]; then
          pardef $parmrec runid delt plid coord
        fi
        runid="${TS_nc_runid:=$runid_parm}"

        echo "curr_nt=$curr_nt  curr_kmin=$curr_kmin  curr_kmax=$curr_kmax"
        echo "curr_fname=$curr_fname"
        echo "curr_slab=$curr_slab"

        if [ $TS_netcdf -eq 1 ]; then

          # Netcdf output is requested

          # Determine the variable name (ibuf3 value) found in this file.
          # It is assumed that there is only 1 unique ibuf3 value for all GRID records
          vname=`ggstat $curr_fname|awk '{if($2=="GRID"){printf "%s",$4;exit}}' -`
          [ -z "$vname" ] && bail "No GRID records found in $curr_fname"

          # Define input and output file names
          iname=$curr_fname
          lc_vname=`echo $vname|tr '[A-Z]' '[a-z]'`
          if [ -n "$TS_nc_suffix" ]; then
            oname=${lc_vname}_${TS_nc_suffix}.nc
          elif [ -n "$tscat_save" ]; then
            oname=${lc_vname}_${tscat_save}.nc
            iname=$tscat_save
          elif [ -n "$tscat01" ]; then
            oname=${lc_vname}_${tscat01}.nc
            iname=$tscat01
          elif [ -n "$runid" ]; then
            oname=${lc_vname}_${runid}.nc
          else
            oname=${lc_vname}_$$.nc
          fi

          if [ $TS_nc_add_var -eq 1 ]; then
            vlist="$vname VINF PARM"
          else
            vlist="$vname"
          fi

          # Rename the current input file so that the correct file name will
          # appear in the global meta data of the output netcdf file
          if [ "$iname" != "$curr_fname" ]; then
            rm -f $iname
            cp $curr_fname $iname
          fi

          ggstat $iname

          # Convert to netcdf
          ccc2nc $TS_ccc2nc_opts delt="$delt_parm" plid="$plid_parm" \
            coord="$coord_parm" -v "$vlist" -o "$oname" $iname

          # Save this netcdf file to DATAPATH/RUNPATH
          if [ -s "$oname" ]; then
            save $oname $oname
            echo "Created netcdf file $oname"
          fi

          echo " "
          ncdump -c $oname
          echo " "

        else

          # CCCma format output is requested

          # Determine the name of the file containing the VINF record
          # for the current variable
          vinfrec=`echo $curr_fname|sed 's/^cccset\(.*\)/VINF\1.txt/'`

          # Determine an output file name using information found in the VINF record
          outname=`tsinfo --info=OutName $tsinfo_opt $vinfrec` ||\
            bail "Error executing tsinfo --info=OutName $tsinfo_opt $vinfrec"

          echo "outname=$outname"
          ls -l $curr_fname

          if [ $TS_file_clobber -eq 1 ]; then
            # If the user has requested a new file then delete the old file
            access oldTSFILE $outname na
            delete oldTSFILE na
            save $curr_fname $outname
          elif [ $TS_file_append -eq 0 ]; then
            # Save files without appending
            # This will create several editions of the same file.
            # These editions should then be concatenated into a single time series
            # after this job has completed.
            save $curr_fname $outname
          else
            # Append to any existing file
            access oldTSFILE $outname na
            if [ -s oldTSFILE ]; then
              # Concatenate the current file with the previous file
              rm -f NEWTS
              tscat oldTSFILE $curr_fname out=NEWTS
              mv NEWTS $curr_fname
              rm -f settmp*
            fi
            save $curr_fname $outname
            delete oldTSFILE na
          fi
          release $curr_fname

        fi

        # Remove the current single variable file
        rm -f $curr_fname

      done

    fi

#   ----------------------------------
#   plot time series (under control of TS_plot)

    if [ $TS_plot -eq 1 ]; then

      plunit=${plunit:='VIC'}

      libncar plunit=$plunit

      # tsplot will have created a file name PARM.txt containing the PARM
      # record in ascii format if a PARM record was present in any of the
      # input 'ts' files.
      model=''
      runid=''
      iyear=''
      if [ -s PARM.txt ]; then
        pardef PARM.txt model runid iyear
      fi
      model="${model:=$model_parm}"
      runid="${runid:=$runid_parm}"
      runid=`echo $runid|tr '[a-z]' '[A-Z]'`
      iyear="${iyear:=$iyear_parm}"

      # Define a string to be made part of the plot title of every plot
      usr_ptitle="${TS_ptitle:=$tscat01}"
      # usr_ptitle=`echo "$usr_ptitle"|tr '[a-z]' '[A-Z]'`

      # ymin=ymax means autoscale the y axis
      ymin=0.0;   ymin=`echo $ymin|awk '{printf "%10.0E",$1}' -`
      ymax=0.0;   ymax=`echo $ymax|awk '{printf "%10.0E",$1}' -`

      # tscat will create 3 files for each time series
      #   *_vals   ...contains function values
      #   *_time   ...contains time axis values
      #   *_title  ...contains a string to use in the plot title
      for yfile in `ls -1 *_vals`; do
        xfile=`echo $yfile|sed 's/_vals$/_time/'`
        tfile=`echo $yfile|sed 's/_vals$/_title/'`
        echo $yfile $xfile $tfile
        ggstat $yfile
        ggstat $xfile
        var=`ggstat $yfile|awk '{if(NF==13){print $4;exit}}' -`
        npts=`ggstat $yfile|awk -v var=$var '{if($4==var){print $6;exit}}' -`
        npts=`echo $npts|awk '{printf "%5d",$1}' -`
        [ -z "$npts" ] && bail "Cannot determine number of time steps for $var"

        xunits=`ggstat $xfile|awk '{if(NF==13){print $4;exit}}' -`
        xunits=`echo $xunits|tr '[A-Z]' '[a-z]'`
        xunits=`echo $xunits|sed -e's/^ *//' -e's/ $//'`
        xlab="Time (${xunits})"
        xlab=`echo $xlab|awk '{printf "%20s",$0}' -`
        ylab=$var
        ylab=`echo $ylab|awk '{printf "%20s",$0}' -`
        ptitle=' '
        # if [ -s "$tfile" ]; then
        #   ptitle=`cat $tfile`
        #   ptitle="$ptitle  $usr_ptitle"
        # else
          ptitle="$usr_ptitle"
        # fi
        echo "xlab: $xlab"
        echo "ylab: $ylab"
        echo "ptitle: $ptitle"

        ccc crvplot $yfile $xfile <<EOF
   CRVPLOT        -1 NEXT   -1$ymin$ymax    0$npts    2  0 1 1  011000
$xlab$ylab$ptitle
EOF

      done

      . plot.cdk

    fi

#   end of plotting
#   ----------------------------------

#   ----------------------------------
#   stop wall clock
    echo "\nSTOP tscat:  "`date`"\n"
