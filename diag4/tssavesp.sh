#!/bin/sh
#                  tssavesp            D. Liu - Nov. 26, 1995
#   ---------------------------------- save time series of spectral fields:
#                                      t850, vort300, div300, z300, z500, z1000.

#   ---------------------------------- t850.
      access spt ${flabel}_spt
echo "SELECT    STEPS $t1 $t2 $s3 LEVS  850  850 NAME TEMP" | ccc   select spt t850
      savets t850 ${tlabel}t85$tendlbl
      rm spt t850

#   ---------------------------------- vr300 and div300.
      access spvr  ${flabel}_spvr
      access spdiv ${flabel}_spdiv
echo "SELECT    STEPS $t1 $t2 $s3 LEVS  300  300 NAME VORT" | ccc   select spvr  vr300
echo "SELECT    STEPS $t1 $t2 $s3 LEVS  300  300 NAME  DIV" | ccc   select spdiv div300
      savets vr300  ${tlabel}vr3$tendlbl
      savets div300 ${tlabel}dv3$tendlbl
      rm spvr spdiv vr300 div300

#   --------------------------------- z300, z500, and z1000.
      access spphi ${flabel}_spz
echo "SELECT    STEPS $t1 $t2 $t3 LEVS  300  300 VARS  PHI" | ccc   select spphi z300
echo "SELECT    STEPS $t1 $t2 $t3 LEVS  500  500 VARS  PHI" | ccc   select spphi z500
echo "SELECT    STEPS $t1 $t2 $t3 LEVS 1000 1000 VARS  PHI" | ccc   select spphi z1000
      rm spphi
      savets z300  ${tlabel}z300s$tendlbl
      savets z500  ${tlabel}z500s$tendlbl
      savets z1000 ${tlabel}z1000s$tendlbl
