#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Recipe specific to U2/ord_soumet ESM runs
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# On U2, since we are using maestro, and thus ord_soumet
#   we are unable to request different OMP THREADS/MPI PROCS
#   combinations on each node, and as such we need to hack
#   the PBS_NODEFILE such that we don't end up stacking 
#   some of the ocean MPI threads on the first node

# first determine how many MPI processes we want for the AGCM/Coupler
AGCM_COUPLER_MPI_PROCS=$((nnode_a + nnode_c))
NUM_MPI_PROCS=$AGCM_COUPLER_MPI_PROCS

# now remove entries from the PBS_NODEFILE (by producing a new one)
#   that correspond to the first node such that we only have NUM_MPI_PROCS
#   available on it
for node in $(sort -u $PBS_NODEFILE); do
    for i in $(seq $NUM_MPI_PROCS); do
        echo $node >> $PWD/pbs_nodefile.$PBS_JOBID 
    done

    # after the first node, we reset the number of mpi procs back to the
    #   allowed number
    if (( NUM_MPI_PROCS == AGCM_COUPLER_MPI_PROCS )); then
        NUM_MPI_PROCS=80
    fi
done

# make it so the updated node file is used
export PBS_NODEFILE=$PWD/pbs_nodefile.$PBS_JOBID
echo "Updated PBS_NODEFILE=$PBS_NODEFILE"
cat $PBS_NODEFILE
