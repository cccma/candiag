#!/bin/sh
#                  vstats              gjb,dl. jan 24/01 - sk.
#   ---------------------------------- calculate v statistics
#                                      and covariances with w.
#
#   sk: re-write for new timavg/xsave, phase out xtrans.
#
      access v ${flabel}_gpv

      if [ "$datatype" = "specsig" ]; then
	echo "DIFF          V $t1 $t2 $delt" | dif v dvdt
      else
	echo "DIFF          V $t1 $t2 $delt       1.0" | dif v dvdt
      fi
      timavg v tv vp tvp2

echo "XSAVE.        V
NEWNAM.
XSAVE.        DV/DT
NEWNAM.      V.
XSAVE.        V\"V\"
NEWNAM.    V\"V\"" > ic.xsave
      vars="tv dvdt tvp2"

      if [ "$wxstats" = on ]; then
	access w  ${flabel}_gpw
	timavg w  tw wp
	timcov vp wp tvpwp
echo "XSAVE.        V"W"
NEWNAM.    V\"W\"" >> ic.xsave
        vars="$vars tvpwp"
      fi

#   ---------------------------------- xsave
      xsave new_gp $vars new. input=ic.xsave
      mv new. new_gp

#   ---------------------------------- save results.
      release oldgp
      access  oldgp ${flabel}gp
      xjoin   oldgp new_gp newgp
      save    newgp ${flabel}gp
      delete  oldgp
