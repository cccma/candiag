#!/bin/sh
#   sk: re-write for new timavg/xsave.
#                  zonstat             gjb., dl, sk - jun 29/2001
#   ---------------------------------- module to produce zonally averaged statistics
#                                      from "gp" file.

      access gpfile ${flabel}gp
      echo "XFIND.        B" | ccc xfind  gpfile beta

#   ---------------------------------- representative zonal means [x]r, [dx/dt]r,
#                                      [x"x"]r, and [x*x*]r.
      vars="q t z u v"
      if [ "$wxstats" = on ] ; then
	vars="$vars w"
      fi

      for x in $vars ; do
	X=`echo $x | tr 'a-z' 'A-Z'`
	if [ "$x" = "z" ] ; then
	  X1="GZ"
	else
	  X1=$X
	fi
	
echo "XFIND.        Q
XFIND.        D${X1}/DT
XFIND.        ${X1}\"${X1}\"" > ic.xfind
  	xfind gpfile x dxdt xp2 input=ic.xfind
	rzonavg x    beta rzx xs rzxs2
	rzonavg dxdt beta rzdxdt
	rzonavg xp2  beta rzxp2

echo "XSAVE.        (${X1})R
NEWNAM.       ${X1}
XSAVE.        (D${X1}/DT)R
NEWNAM.    ${X}/DT
XSAVE.        (${X1}\"${X1}\")R
NEWNAM.
XSAVE.        (${X1}\*${X1}\*)R
NEWNAM.    ${X}\*${X}\*" > ic.xsave
         xsave new_xp rzx rzdxdt rzxp2 rzxs2 new. input=ic.xsave
         mv new. new_xp
         mv xs ${x}s
         rm x dxdt xp2 xs xs2 rzx rzdxdt rzxp2 rzxs2
      done

#   ---------------------------------- [x'v']r and [x*v*]r.
      vars="q t z u"
      for x in $vars ; do
	X=`echo $x | tr 'a-z' 'A-Z'`
	if [ "$x" = "z" ] ; then
	  X1="GZ"
	else
	  X1=$X
	fi
	
	echo "XFIND.        ${X1}\"V\"" | ccc xfind   gpfile   xpvp
	rzonavg xpvp beta rzxpvp

	mlt     ${x}s  vs   xsvs
	rzonavg xsvs beta rzxsvs

	echo "XSAVE.        (${X1}\"V\")R
NEWNAM.
XSAVE.        (${X1}\*V*\)R
NEWNAM.    ${X}\*V\*" > ic.xsave
         xsave new_xp rzxpvp rzxsvs new. input=ic.xsave
         mv new. new_xp
         rm xs
      done

      if [ "$wxstats" = on ] ; then
#   ---------------------------------- [x'w']r and [x*w*]r.
	vars="q t z u v"
	for x in $vars ; do
	  X=`echo $x | tr 'a-z' 'A-Z'`
	  if [ "$x" = "z" ] ; then
	    X1="GZ"
	  else
	    X1=$X
	  fi

	  echo "XFIND.        ${X1}\"W\"" | ccc xfind   gpfile   xpwp
	  rzonavg xpwp beta rzxpwp

	  mlt     ${x}s  ws   xsws
	  rzonavg xsws beta rzxsws

	  echo "XSAVE.        (${X1}\"W\")R
NEWNAM.
XSAVE.        (${X1}\*W\*)R
NEWNAM.    ${X}\*W\*" > ic.xsave 
          xsave new_xp rzxpwp rzxsws new. input=ic.xsave
	  mv new. new_xp
	  rm xs
	done
      fi
