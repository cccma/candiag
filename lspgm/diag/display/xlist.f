      PROGRAM XLIST 
C     PROGRAM XLIST (XFILE,       OUTPUT,                               )       A2
C    1         TAPE1=XFILE, TAPE6=OUTPUT) 
C     -----------------------------------                                       A2
C                                                                               A2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       A2
C     JAN 14/92 - E. CHAN  (ADD WARNING XIT IF PROBLEM WITH FBUFFIN)            
C     MAR 16/92 - E. CHAN  (MODIFY FOR CCRN DATA IN 2-RECORD FORMAT)            
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII AND            
C                           REPLACE BUFFERED I/O)                             
C     DEC 06/83 - B.DUGAS.                                                      
C     FEB 14/80 - J.D.HENDERSON 
C                                                                               A2
CXLIST   - LISTS SUPERLABELS IN A LABELED SET FILE                      1  0    A1
C                                                                               A3
CAUTHOR  - J.D.HENDERSON                                                        A3
C                                                                               A3
CPURPOSE - PRINTS ALL SUPERLABELS IN AN XSAVE-TYPE FILE (XFILE).                A3
C          NOTE - THESE LABELS ARE 80 CHARACTERS OF IDENTIFICATION              A3
C                 THAT PRECEDE EACH MULTI-LEVEL SET, 16 OF WHICH ARE            A3
C                 INTENTIONALLY LEFT BLANK.                                     A3
C                                                                               A3
CINPUT FILE...                                                                  A3
C                                                                               A3
C      XFILE = FILE CONTAINING SUPERLABELED SETS.                               A3
C-------------------------------------------------------------------------- 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/ICOM/IBUF(8),LABL(20)
      CHARACTER*80 LABEL
      EQUIVALENCE (LABL,LABEL)
      DATA MAXX/28/
C-------------------------------------------------------------------- 
      NFF=2 
      CALL JCLPNT(NFF,1,6)
      REWIND 1
      WRITE(6,6001) 
      N=0 
  110 CALL FBUFFIN(1,IBUF,-8,K,LEN)
      IF (K.EQ.0) THEN
        CALL                                       XIT('XLIST',0)
      ELSEIF (K.GT.0) THEN
        CALL                                       XIT('XLIST',-101)
      ENDIF
      N=N+1 
      IF(IBUF(1).EQ.NC4TO8("LABL")) THEN
        BACKSPACE 1
        BACKSPACE 1
        CALL FBUFFIN(1,IBUF,MAXX,K,LEN)
        WRITE(6,6010) N,LABEL
      ENDIF
      GO TO 110 
C-------------------------------------------------------------------- 
 6001 FORMAT('0')
 6010 FORMAT('0',I5,5X,A80)
      END
