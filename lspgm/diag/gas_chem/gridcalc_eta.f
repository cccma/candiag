      PROGRAM GRIDCALC_ETA
C     PROGRAM GRIDCALC_ETA (TEMP,  LNSP,  XOUT1,  XOUT2,  INPUT,  OUTPUT,)
C    1         TAPE1=TEMP,TAPE2=LNSP,TAPE11=XOUT1, TAPE12=XOUT2, TAPE5=INPUT,
C              TAPE6=OUTPUT)
C     ------------------------------------------------------------------
C
C     Aug  8/14 - D. Plummer - changed name from accmip_gridcalc
C     Aug 11/11 - D. Plummer (DERIVED FROM VSINTH_TROP)
C
C        - CALCULATE THE GEOMETRIC THICKNESS OF ETA LEVELS AND THE MASS
C          OF AIR IN EACH LEVEL
C
CINPUT FILE...                                                                  J3
C                                                                               J3
C      TEMP = INPUT SERIES OF ETA LEVEL TEMPERATURE FIELDS.                     J3
C      LNSP = INPUT SERIES OF LN(SURFACE PRESSURE) IN MB.                       J3
C                                                                               J3
COUTPUT FILE...                                                                 J3
C                                                                               J3
C      XOUT1 = GEOMETRIC THICKNESS OF EACH MODEL LAYER IN METRES                J3
C      XOUT2 = MASS OF AIR IN EACH MODEL LAYER IN KG/M^2
C
CINPUT PARAMETERS...
C                                                                               J5
C      LEVTYP = FULL FOR MOMENTUM VARIABLE, AND                                 J5
C               HALF FOR THERMODYNAMIC ONE.                                     J5
C      LAY    = DEFINES THE POSITION OF LAYER INTERFACES IN RELATION            J5
C               TO LAYER CENTRES (SEE BASCAL).                                  J5
C               (ZERO DEFAULTS TO THE FORMER STAGGERING CONVENTION).            J5
C      ICOORD = 4H ETA/4H SIG FOR ETA/SIGMA LEVELS OF INPUT FIELDS.             J5
C      SIGTOP = VALUE OF SIGMA AT TOP OF DOMAIN FOR VERTICAL INTEGRAL.          J5
C               IF .LT.0., THEN INTERNALLY DEFINED BASED ON LEVTYP              J5
C               FOR UPWARD COMPATIBILITY.                                       J5
C      PTOIT  = PRESSURE (PA) OF THE RIGID LID OF THE MODEL.                    J5
C                                                                               J5
C      NOTE - LAY AND LEVTYP DEFINE THE TYPE OF LEVELLING FOR THE VARIABLE.     J5
C                                                                               J5
CEXAMPLE OF INPUT CARD...                                                       J5
C                                                                               J5
C*ACCGRID. HALF        1.    0  SIG  -1.        0.                              J5
C------------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_LAT,
     &                       SIZES_MAXLEV

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
      LOGICAL OK
      INTEGER LEV(SIZES_MAXLEV)
C
      REAL ETA(SIZES_MAXLEV), ETAB(SIZES_MAXLEV)
      REAL ABM(SIZES_MAXLEV), BBM (SIZES_MAXLEV)
      REAL ABB(SIZES_MAXLEV), BBB (SIZES_MAXLEV)
C
      REAL SIGB(SIZES_LONP1xLAT,2), SIGM(SIZES_LONP1xLAT,2)
      REAL PSMBLN(SIZES_LONP1xLAT)
      REAL DELZ(SIZES_LONP1xLAT), DELM(SIZES_LONP1xLAT)
      REAL TEMP(SIZES_LONP1xLAT), PRESS(SIZES_LONP1xLAT)
C
      REAL DTDZ(SIZES_LONP1xLAT,SIZES_MAXLEV)
      REAL PRLEVS(SIZES_LONP1xLAT,SIZES_MAXLEV)
C
      COMMON /ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
      DATA MAXLEV /SIZES_MAXLEV/
      DATA MAXX /SIZES_LONP1xLATxNWORDIO/
C---------------------------------------------------------------------
      NFIL = 6
      CALL JCLPNT(NFIL,1,2,11,12,5,6)
      REWIND 1
      REWIND 2

C     * READ CONTROL DIRECTIVES.

      READ(5,5010,END=991) LEVTYP,LAY,ICOORD,SIGTOP,PTOIT
      IF(ICOORD.EQ.NC4TO8("    ")) ICOORD=NC4TO8(" SIG")
      IF(ICOORD.EQ.NC4TO8(" SIG")) THEN
        PTOIT=MAX(PTOIT,0.00)
      ELSE
        PTOIT=MAX(PTOIT, 0.100E-09)
      ENDIF
      WRITE(6,6005) LEVTYP,LAY,ICOORD,SIGTOP,PTOIT

C     * GET ETA VALUES OF LAYER CENTRES FROM XIN.

      CALL FILEV (LEV,ILEV,IBUF,1)
      IF(ILEV.LT.1 .OR. ILEV.GT.MAXLEV)  CALL   XIT('GRIDCALC',-1)
      WRITE(6,6007) ILEV,(LEV(L),L=1,ILEV)

      CALL LVDCODE(ETA,LEV,ILEV)
      DO 150 L=1,ILEV
         ETA(L)=ETA(L)*.001
  150 CONTINUE

C     * DETERMINE THE FIELD SIZE.

      NWDS=IBUF(5)*IBUF(6)

C     * EVALUATE LAYER INTERFACES FROM LEVTYP AND LAY.
      IF    (LEVTYP.EQ.NC4TO8("FULL")) THEN
         CALL BASCAL (ETAB,IBUF, ETA,ETA,ILEV,LAY)
      ELSEIF(LEVTYP.EQ.NC4TO8("HALF")) THEN
         CALL BASCAL (IBUF,ETAB, ETA,ETA,ILEV,LAY)
      ELSE
         CALL                                   XIT('GRIDCALC',-3)
      ENDIF

C     * EVALUATE THE PARAMETERS A AND B OF THE VERTICAL DISCRETIZATION
C     * PERFORMED FOR BOTH THE LAYER MIDPOINTS AND LOWER FACE

      CALL COORDAB (ABM,BBM, ILEV,ETA ,ICOORD,PTOIT)
      CALL COORDAB (ABB,BBB, ILEV,ETAB,ICOORD,PTOIT)

C---------------------------------------------------------------------

      NSETS=0
  200 CONTINUE

C     * READ NEXT LN(PS) NEEDED TO EVALUATE THE SIGMA LEVELS.

      CALL GETFLD2 (2,PSMBLN,-1,-1,NC4TO8("LNSP"),1,IBUF,MAXX,OK)
      IF(NSETS.EQ.0) WRITE(6,6025) IBUF
      IF(.NOT.OK) THEN
         IF(NSETS.GT.0) THEN
            WRITE(6,6025) IBUF
            WRITE(6,6020) NSETS
            CALL                                XIT('GRIDCALC',0)
         ELSE
            CALL                                XIT('GRIDCALC',-4)
         ENDIF
      ENDIF

      ITIM=IBUF(2)
      DO 300 I=1,NWDS
         PRESS(I)=100.*EXP(PSMBLN(I))
  300 CONTINUE

C     * CALCULATE THE GEOMETRIC THICKNESS AND MASS THICKNESS OF EACH LAYER

      IGH = 1
      LOW = 2

      IF(SIGTOP.LT.0.E0)THEN
         IF(LEVTYP.EQ.NC4TO8("FULL"))THEN
            DO 440 I=1,NWDS
               SIGB(I,IGH)=0.0E0
  440       CONTINUE
         ELSE
            DO 450 I=1,NWDS
               SIGB(I,IGH)=PTOIT/PRESS(I)
  450       CONTINUE
         ENDIF
      ELSE
         DO 460 I=1,NWDS
            SIGB(I,IGH)=SIGTOP
  460    CONTINUE
      ENDIF

C     * PROCESS ALL LEVELS AND CALCULATE THE GEOMETRIC AND PRESSURE
C     * THICKNESS

      CNST1 = -287.04/9.80616
      DO 800 L=1,ILEV

         CALL GETFLD2 (1,TEMP,-1,ITIM,NC4TO8("TEMP"),LEV(L),
     1                 IBUF,MAXX,OK)

         IF(.NOT.OK) CALL                         XIT('GRIDCALC',-5)

         CALL NIVCAL  (SIGB(1,LOW), ABB(L),BBB(L),PRESS,1,NWDS,NWDS)

         DO I=1,NWDS
            DELZ(I) = CNST1*TEMP(I)*LOG(SIGB(I,IGH)/SIGB(I,LOW))
         ENDDO

         DO I=1,NWDS
            DELM(I) = (SIGB(I,LOW) - SIGB(I,IGH))*PRESS(I)/9.80616
         ENDDO

         IBUF(3)=NC4TO8("DELZ")
         IBUF(4)=LEV(L)
         CALL PUTFLD2 (11,DELZ,IBUF,MAXX)
C
         IBUF(3)=NC4TO8("DELM")
         IBUF(4)=LEV(L)
         CALL PUTFLD2 (12,DELM,IBUF,MAXX)

         KEEP= LOW
         LOW = IGH
         IGH = KEEP

 800  CONTINUE

      NSETS=NSETS+1

      GO TO 200

C     * E.O.F. ON INPUT.

  991 CALL                                      XIT('GRIDCALC',-6)
C---------------------------------------------------------------------
 5010 FORMAT(11X,A4,I5,1X,A4,E5.0,E10.0,E10.0)
 6005 FORMAT('  LEVTYP = ',A4,', LAY = ',0P,I5, 
     1       ', COORD=',1X,A4,', SIGTOP =',F15.10,
     2       ', P.LID (PA)=',E10.3)
 6007 FORMAT(' ILEV,ETA =',I5,2X,20I5/(18X,20I5))
 6020 FORMAT('0',I6,' SETS WERE PROCESSED')
 6025 FORMAT(' ',A4,I10,2X,A4,5I10)
      END
