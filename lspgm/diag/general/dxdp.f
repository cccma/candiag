      PROGRAM DXDP
C     PROGRAM DXDP (XIN,       XOUT,       OUTPUT,                      )       C2
C    1        TAPE1=XIN, TAPE2=XOUT, TAPE6=OUTPUT)
C     --------------------------------------------                              C2
C                                                                               C2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       C2
C     JAN 12/93 - E. CHAN  (DECODE LEVELS IN 8-WORD LABEL)                      
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 11/83 - R.LAPRISE.                                                    
C     FEB 12/80 - J.D.HENDERSON 
C                                                                               C2
CDXDP    - VERTICAL PRESSURE DERIVATIVE OF A FILE                       1  1    C1
C                                                                               C3
CAUTHOR  - J.D.HENDERSON                                                        C3
C                                                                               C3
CPURPOSE - COMPUTES PRESSURE DERIVATIVE FOR ALL SETS IN A FILE                  C3
C          NOTE - THERE SHOULD BE AT LEAST 3 LEVELS IN XIN AND AT MOST $PL$.    C3
C                                                                               C3
CINPUT FILE...                                                                  C3
C                                                                               C3
C      XIN  = INPUT FILE OF PRESSURE LEVEL SETS (REAL OR COMPLEX)               C3
C             (MINIMUM NUMBER OF LEVELS IS 3)                                   C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      XOUT = PRESSURE DERIVATIVES AT THE SAME LEVELS AS THE DATA               C3
C             IN FILE XIN                                                       C3
C-----------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_PLEV

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/GG(SIZES_PLEV*SIZES_LONP1xLAT)
C 
      LOGICAL OK,SPEC 
      INTEGER LEV(SIZES_PLEV) 
      REAL PR(SIZES_PLEV),G(SIZES_PLEV),DP(SIZES_PLEV)
C 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/, MAXL/SIZES_PLEV/
C---------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,2,6)
      REWIND 1
      REWIND 2
      NSETS=0 
C 
C     * READ THE NEXT SET OF FIELDS.
C 
  110 CALL GETSET2(1,GG,LEV,NLEV,IBUF,MAXX,OK) 
      IF(.NOT.OK)THEN 
        IF(NSETS.EQ.0)THEN
          WRITE(6,6010) 
          CALL                                     XIT('DXDP',-1) 
        ELSE
          WRITE(6,6020) NSETS 
          CALL                                     XIT('DXDP',0)
        ENDIF 
      ENDIF 
      IF((NLEV.LT.3).OR.(NLEV.GT.MAXL)) CALL       XIT('DXDP',-2) 
C 
C     * DETERMINE KIND AND SIZE.
C 
      KIND=IBUF(1)
      NAME=IBUF(3)
      IF(NSETS.EQ.0)THEN
        WRITE(6,6003) KIND,NAME 
        WRITE(6,6005) NLEV,(LEV(L),L=1,NLEV)
      ENDIF 
      SPEC=(KIND.EQ.NC4TO8("SPEC") .OR. KIND.EQ.NC4TO8("FOUR"))
      NWDS=IBUF(5)*IBUF(6)
      IF(SPEC) NWDS=NWDS*2
C 
C     * SET PR TO THE PRESSURE IN N/M**2. 
C 
      CALL LVDCODE(PR,LEV,NLEV)
      DO 120 L=1,NLEV 
  120 PR(L)=PR(L)*100.E0
C 
C     * COMPUTE THE DERIVATIVE FROM THE TOP DOWN. 
C     * ENDS USE ONE-SIDED DIFFERENCE. INTERIOR USES CENTERED DIFF. 
C 
      DO 280 I=1,NWDS 
C 
      DO 220 L=1,NLEV 
      N=(L-1)*NWDS+I
  220 G(L)=GG(N)
C 
      NLEVM=NLEV-1
      DP(1)=(G(2)-G(1))/(PR(2)-PR(1)) 
      DO 240 L=2,NLEVM
  240 DP(L)=(G(L+1)-G(L-1))/(PR(L+1)-PR(L-1)) 
      DP(NLEV)=(G(NLEV)-G(NLEVM))/(PR(NLEV)-PR(NLEVM))
C 
      DO 260 L=1,NLEV 
      N=(L-1)*NWDS+I
  260 GG(N)=DP(L) 
C 
  280 CONTINUE
C 
C     * PUT THE RESULTS ONTO FILE GGOUT.
C 
      CALL PUTSET2(2,GG,LEV,NLEV,IBUF,MAXX)
      IF(NSETS.EQ.0) WRITE(6,6025) IBUF 
      NSETS=NSETS+1 
      GO TO 110 
C---------------------------------------------------------------------
 6003 FORMAT('0DXDP ON ',A4,2X,A4)
 6005 FORMAT('0',I5,' LEVELS =',20I5/(15X,20I5))
 6010 FORMAT('0..  DXDP INPUT FILE IS EMPTY')
 6020 FORMAT('0',I6,' SETS WERE PROCESSED')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
