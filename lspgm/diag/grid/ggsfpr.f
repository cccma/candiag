      PROGRAM GGSFPR
C     PROGRAM GGSFPR (PHI,      PHIS,      PMSL,      SFPR,      OUTPUT,)       D2
C    1          TAPE1=PHI,TAPE2=PHIS,TAPE3=PMSL,TAPE4=SFPR,TAPE6=OUTPUT)
C     ------------------------------------------------------------------        D2
C                                                                               D2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       D2
C     NOV 06/95 - F.MAJAESS (REVISE DEFAULT PACKING DENSITY VALUE)              
C     JAN 12/93 - E. CHAN  (DECODE LEVELS IN 8-WORD LABEL)                      
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 11/83 - R.LAPRISE.                                                    
C     NOV 24/80 - J.D.HENDERSON 
C                                                                               D2
CGGSFPR  - COMPUTES SURFACE PRESSURE FROM PHI,PHIS,MSLPR.               3  1    D1
C                                                                               D3
CAUTHOR  - J.D.HENDERSON                                                        D3
C                                                                               D3
CPURPOSE - COMPUTES A FILE OF SURFACE PRESSURE GRIDS FROM PRESSURE              D3
C          LEVEL GRIDS OF GEOPOTENTIAL, THE MOUNTAINS, AND                      D3
C          THE MEAN-SEA-LEVEL PRESSURE.                                         D3
C          NOTE - MAX GRID SIZE = $IJ$, MAX LEVELS = $PL$.                      D3
C                                                                               D3
CINPUT FILES...                                                                 D3
C                                                                               D3
C      PHI  = PRESSURE LEVEL GRID SETS OF GEOPOTENTIAL                          D3
C      PHIS = GEOPOTENTIAL AT THE SURFACE (MOUNTAIN FIELD).                     D3
C      PMSL = MEAN-SEA-LEVEL PRESSURE GRIDS (MB)                                D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      SFPR = GRIDS OF SURFACE PRESSURE                                         D3
C-----------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_PLEV

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/PHI(SIZES_PLEV*SIZES_LONP1xLAT) 
      COMMON/BLANCK/PMSL(SIZES_LONP1xLAT),PHIS(SIZES_LONP1xLAT),
     & SFPR(SIZES_LONP1xLAT)
C 
      LOGICAL OK
      INTEGER LEV(SIZES_PLEV) 
      REAL PR(SIZES_PLEV) 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/, MAXL/SIZES_PLEV/
C-----------------------------------------------------------------------
      NFF=5 
      CALL JCLPNT(NFF,1,2,3,4,6)
      REWIND 1
      REWIND 2
      REWIND 3
      REWIND 4
C 
C     * GET THE PRESSURE LEVELS FROM THE FIRST SET OF PHI.
C     * NLEV = NO. OF LEVELS, LEV(I) = PRESSURE LEVELS. 
C 
      CALL FILEV(LEV,NLEV,IBUF,1) 
      IF((NLEV.LT.1).OR.(NLEV.GT.MAXL)) CALL       XIT('GGSFPR',-1) 
      WRITE(6,6020) (LEV(L),L=1,NLEV) 
      CALL LVDCODE(PR,LEV,NLEV)
      NPTS=IBUF(5)*IBUF(6)
      REWIND 1
C 
C     * READ IN THE SURFACE GEOPOTENTIAL. 
C 
      CALL GETFLD2(2,PHIS,NC4TO8("GRID"),-1,NC4TO8("PHIS"),1,
     +                                          IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('GGSFPR',-2) 
      WRITE(6,6025) IBUF
C 
C     * READ THE NEXT SET OF PHI AND CORRESPONDING MSL PRES.
C 
      NSETS=0 
  100 DO 200 L=1,NLEV 
      N=(L-1)*NPTS+1
      CALL GETFLD2(1,PHI(N),NC4TO8("GRID"),-1,NC4TO8(" PHI"),LEV(L),
     +                                                 IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        WRITE(6,6010) NSETS 
        CALL                                       XIT('GGSFPR',0)
      ENDIF 
  200 CONTINUE
      IF(NSETS.EQ.0) THEN
        WRITE(6,6025) IBUF 
        NPACK=MIN(2,IBUF(8))
      ENDIF
      NT=IBUF(2)
      CALL GETFLD2(3,PMSL,NC4TO8("GRID"),NT,NC4TO8("PMSL"),1,
     +                                          IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('GGSFPR',-3) 
      IF(NSETS.EQ.0) THEN
        WRITE(6,6025) IBUF 
        NPACK=MIN(NPACK,IBUF(8))
      ENDIF
C 
C     * CALCULATE THE SURFACE PRESSURE. 
C 
      CALL GZASFP2(SFPR,PHI,PHIS,PMSL,NPTS,NLEV,PR) 
C 
      IBUF(3)=NC4TO8("SFPR")
      IBUF(4)=1 
      IBUF(8)=NPACK
      CALL PUTFLD2(4,SFPR,IBUF,MAXX) 
      IF(NSETS.EQ.0) WRITE(6,6025) IBUF 
      NSETS=NSETS+1 
      GO TO 100 
C-----------------------------------------------------------------------
 6010 FORMAT(' ',I6,' SETS READ')
 6020 FORMAT('0PRESSURE LEVELS',15I5/(16X,15I5))
 6025 FORMAT('0',A4,I10,2X,A4,I10,4I6)
      END
