      PROGRAM LLALL
C     PROGRAM LLALL (LLOLD,       LLNEW,       INPUT,       OUTPUT,     )       D2
C    1         TAPE1=LLOLD, TAPE2=LLNEW, TAPE5=INPUT, TAPE6=OUTPUT)
C     -------------------------------------------------------------             D2
C                                                                               D2
C     SEP 18/06 - F.MAJAESS  (PROTECT AGAINST EXCEEDING DECLARED DIMENSIONS)    D2
C     AUG 26/04 - F.MAJAESS  (REMOVE UNREACHABLE "CALL XIT(...,0)" STATEMENT)   
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       
C     JUL 14/00 - F.MAJAESS  (ENSURE "NLAT" IS RESET BACK, TO THE VALUE READ    
C                             FOR IT FROM THE INPUT CARD, IN THE "ICCRN.EQ.0"   
C                             CASE WITH DATA NOT SPANNING THE POLES)            
C     JUN 25/97 - F.MAJAESS  (REVISE TO SUPPORT OUTPUT ON SHIFTED GRID)         
C     FEB 03/97 - F.MAJAESS  (CORRECT "ICYC" DOCUMENTATION; RESET TO 1 NOT 0)
C     JAN 03/97 - F.MAJAESS  (REPLACE "FBUFFIN" USE BY "GETFLD2")
C     NOV 06/95 - F.MAJAESS  (REVISE TO ALLOW KHEM=3)
C     OCT 03/94 - N.SARGENT, E.CHAN (FIX AN I/O ERROR)
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII AND
C                           REPLACE BUFFERED I/O)
C     SEP 30/88 - R. MENARD, F.MAJAESS (ALLOW SEVERAL CCRN-STANDARD RECORDS
C                                       COMPUTATION ON NON-GLOBAL FILES)
C     FEB 09/88 - R. MENARD.
C     DEC 23/86 - M. LAZARE.
C                                                                               D2
CLLALL   - CONVERT ANY LAT-LONG FILE TO NON-SHIFTED OR SHIFTED                  D1
C                                                  LAT-LONG GRID        1  1 C GD1
C                                                                               D3
CAUTHOR  - MIKE LAZARE                                                          D3
C                                                                               D3
CPURPOSE - CONVERTS AN EQUALLY SPACED LAT-LON GRID FILE TO A NON-SHIFTED        D3
C          GLOBAL (REGULAR) OR SHIFTED GLOBAL LAT-LON GRID FILE BY LINEAR       D3
C          INTERPOLATIONS IN LONGITUDE AND LATITUDE WHICH ARE APPLIED           D3
C          SEPARATELY.                                                          D3
C          NOTE - THE RECORD(S) IN THE INPUT FILE MUST CONFORM TO THE STANDARD  D3
C                 CCRN RECORD FORMAT STRUCTURE (IE. 2 FORMATTED RECORDS, THE    D3
C                 FIRST HAS THE 8-WORDS LABEL AND THE SECOND CONTAINS THE       D3
C                 DATA). THE DATA ITSELF MAY OR MAY NOT BE GLOBAL AND IT MUST   D3
C                 BE ORDERED BY  LATITUDE, (FROM SOUTH TO NORTH OR VISE VERSA), D3
C                 AND        BY  LONGITUDE (FROM WEST TO EAST).                 D3
C                                                                               D3
C                 ALL VALID RECORDS IN THE INPUT FILE ARE PROCESSED.            D3
C                                                                               D3
C                 INPUT DATA ON NON-SHIFTED GLOBAL GRID MAY OR MAY NOT HAVE A   D3
C                 REPEATED LONGITUDE, (CYCLIC IN LONGITUDE). THE GEOGRAPHICAL   D3
C                 POSITION OF THE FIRST  GRID  POINT  MUST  BE  REFERENCED      D3
C                 RELATIVE  TO  THE  SOUTH  POLE  VIA  OFFLON  AND  OFFLAT      D3
C                 PARAMETERS (PLEASE CONSULT THE INPUT PARAMETERS  SECTION      D3
C                 BELOW).                                                       D3
C                 ALSO, FOR NON-SHIFTED NON-GLOBAL AND/OR SHIFTED  LAT-LONG     D3
C                 INPUT FILE, ADDITIONAL INFORMATIONS MUST BE PASSED VIA        D3
C                 ENDLON, ENDLAT AND USVALU.                                    D3
C                                                                               D3
C                 THE DATA IN THE OUTPUT FILE ON THE SHIFTED GLOBAL LAT-LONG    D3
C                 GRID IS DEFINED TO BE REPRESENTATING EQUAL SIZED GRID BOXES   D3
C                 CENTERED AT A HALF GRID BOX SPACING STARTING FROM GREENWICH   D3
C                 AND OFF THE POLES (IE. THE FIRST DATA POINT REPRESENT THE     D3
C                 GRID BOX EAST OF GREENWICH WHOSE WEST EDGE COINCIDE WITH      D3
C                 GREENWICH AND SOUTHERN EDGE COINCIDE WITH THE SOUTH POLE)     D3
C                 THE OUTPUT FILE SHIFTED GRID TYPE IS CONTROLLED BY ILON < 0   D3
C                 AND/OR ILAT < 0 (SEE THE INPUT PARAMETERS SECTION BELOW)      D3
C                                                                               D3
C                                                                               D3
CINPUT FILE...                                                                  D3
C                                                                               D3
C      LLOLD  = A NON-SHIFTED/SHIFTED GLOBAL/NON-GLOBAL LAT-LONG FILE           D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      LLNEW  = A NON-SHIFTED OR SHIFTED GLOBAL LAT-LONG FILE                   D3
C
CINPUT PARAMETERS...
C                                                                               D5
C      ICCRN  = SWITCH TO INDICATE WHETHER THE INPUT DATA CONFORM TO THE        D5
C               STANDARD CCRN RECORD FORMAT ON THE REGULAR NON-SHIFTED GLOBAL   D5
C               GRID (ICCRN.NE.0) OR NOT (ICCRN.EQ.0) IN WICH CASE A SECOND     D5
C               CARD IS READ.                                                   D5
C      NLON   = NUMBER OF EQUALLY-SPACED LONGIGTUDES OF THE INPUT FILE          D5
C               ( INCLUDES THE REPEATED LONGITUDE IF ICYC=1 AND RESET           D5
C               TO IBUF(5) IF ICCRN.NE.0 )                                      D5
C      NLAT   = NUMBER OF EQUALLY SPACED LATITUDES IN A "NON-REGULAR" GRID.     D5
C               (RESET TO IBUF(6) IF ICCRN.NE.0)                                D5
C      OFFLON = OFFSET OF THE FIRST LONGITUDE, MEASURED EASTWARD, IN DEGREES,   D5
C               FROM THE GREENWICH MERIDIAN ( 0. <= OFFLON < 360 ).             D5
C               OFFLON IS RESET TO ZERO IF ICCRN.NE.0 .                         D5
C      OFFLAT = OFFSET OF THE FIRST LATITUDE, MEASURED IN DEGREES FROM THE      D5
C               SOUTH POLE. ( 0. <= OFFLAT <= 180. ).                           D5
C               IF OFFLAT IS GREATER THAN 90.0, (THE EQUATOR), THIS MEANS       D5
C               THAT THE LATITUDES OF THE INPUT FILE ARE PROGRESSING FROM       D5
C               THE NORTH TO THE SOUTH.                                         D5
C               OFFLAT IS RESET TO ONE IF ICCRN.NE.0 .                          D5
C      ICYC   = INDICATES IF THE INPUT FILE HAS A REPEATED LONGITUDE, ICYC=1    D5
C               ( CYCLIC IN LONGITUDE ) OR NOT, ICYC=0. ICYC IS RESET TO ONE    D5
C               IF ICCRN.NE.0 AND IT IS IGNORED IF THE INPUT FILE IS NOT GLOBAL.D5
C      ILON   >0; NUMBER OF EQUALLY SPACED LONGITUDES ON THE REGULAR LAT-LON    D5
C                GRID (INCLUDING THE REPEATED LONGITUDE) IN THE OUTPUT FILE.    D5
C             <0 OR ILAT < 0 (SEE NEXT); ABS(ILON)=NUMBER OF EQUALLY SPACED     D5
C                                        LONGITUDES ON A SHIFTED LAT-LON GRID   D5
C                                        (NO REPEATED LONGITUDE) IN THE OUTPUT  D5
C                                        FILE.                                  D5
C      ILAT   >0; NUMBER OF EQUALLY SPACED LATITUDES ON THE REGULAR LAT-LON     D5
C                 GRID (INCLUDING BOTH POLES) IN THE OUTPUT FILE.               D5
C             <0 OR ILON < 0 (SEE ABOVE); ABS(ILAT)=NUMBER OF EQUALLY SPACED    D5
C                                         LATITUDES ON A SHIFTED LAT-LON GRID   D5
C                                         (DATA OFF THE POLES) IN THE OUTPUT    D5
C                                         FILE.                                 D5
C      NM     = 4 LETTER CHARACTER STRING TO BE USED AS IBUF(3).                D5
C               (RESET TO IBUF(3) IF ICCRN.NE.0 OR "NM=  -1")                   D5
C      LVL    = LEVEL TO BE USED AS IBUF(4).                                    D5
C               (RESET TO IBUF(4) IF ICCRN.NE.0 OR "LVL=-1")                    D5
C                                                                               D5
C      THE FOLLOWING SET OF PARAMETERS FOR A SECOND CARD ARE READ ONLY IF       D5
C      THE INPUT FILE "LLOLD" IS NOT A REGULAR NON-SHIFTED GLOBAL LAT-LONG      D5
C      FILE (I.E. ICCRN.EQ.0).                                                  D5
C                                                                               D5
C      NDATE  = TIME TO BE USED AS IBUF(2).                                     D5
C               (RESET TO IBUF(2) IF "NDATE=-1")                                D5
C      KHEM   = INTEGER INDICATING THE TYPE OF COVERAGE OF THE INPUT            D5
C               FILE. GLOBAL FILE KHEM = 0/3, NON-GLOBAL FILE KHEM = -1,        D5
C      ENDLON = LONGITUDE OF THE LAST GRID POINT OF THE INPUT FILE, USING       D5
C               THE SAME CONVENTION AS FOR OFFLON ( IS IGNORED IF KHEM = 0/3).  D5
C      ENDLAT = LATITUDE OF THE LAST GRID POINT OF THE INPUT FILE, MEASURED     D5
C               FROM THE SOUTH POLE ( IS IGNORED IF KHEM = 0/3 ).               D5
C      USVALU = A USER VALUE ASSIGNED TO GRID POINTS OF THE OUTPUT FILE         D5
C               LYING OUTSIDE THE DOMAIN OF THE INPUT FILE (I.E. OUTSIDE        D5
C               THE AREA DEFINED BY OFFLON, OFFLAT, ENDLON AND ENDLAT).         D5
C                                                                               D5
CEXAMPLE OF INPUT CARD(S)...                                                    D5
C                                                                               D5
C  FOR INPUT DATA ON A SHIFTED GRID AND OUTPUT ON THE REGULAR NON-SHIFTED       D5
C  GLOBAL LAT-LONG GRID:                                                        D5
C                                                                               D5
C* LLALL.     0   72   36       2.5       2.5    0   73   37   ST    1          D5
C*   DATE    -1    0     357.5      87.5 VALU     1.E38                         D5
C                                                                               D5
C  AND FOR INPUT DATA ON THE REGULAR NON-SHIFTED GLOBAL LAT-LONG GRID AND       D5
C  OUTPUT ON A SHIFTED GLOBAL LAT-LONG GRID:                                    D5
C                                                                               D5
C* LLALL.     1   73   37       0.0       0.0    1  -72  -36   ST    1          D5
C----------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLONP1,
     &                       SIZES_NWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

C     * SUPPORT IBI X IBJ 2-D GRID EXPANSION UP TO ADDING 2 EXTRA LATITUDES 
C     * AT THE POLES AND ADDING A CYCLICAL LONGITUDE.

      PARAMETER (IBI=SIZES_BLONP1,IBJ=SIZES_BLAT,NBI=IBI+1,NBJ=IBJ+2,
     1           NBIJ=NBI*NBJ,NBIJV=NBIJ*SIZES_NWORDIO)
      REAL AF(NBI,NBJ),BF(NBI,NBJ),G(NBIJ)
      REAL OLDLON(IBI),NEWLON(NBI),OLDLAT(IBJ),NEWLAT(NBJ)
      REAL RLON(NBI),RLAT(NBJ),RXLAT(NBJ),DIFF(NBI)
      REAL LONEXT,LATEXT
      LOGICAL OK,FLAG1,FLAG2,NEARPOL,INSIDE,POLES,OSHFTED
      CHARACTER*4 NM
      COMMON/ICOM/IBUF(8),IDAT(NBIJV)
C
      EQUIVALENCE (G(1),BF(1,1))
C
      DATA MAXX/NBIJV/
C------------------------------------------------------------------------------
      NFF=4
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
C
C     * READ IN INPUT DATA, AND CONVERT CHARACTER STRING NM TO "INTEGER"
C     * VARIABLE NAME FOR IBUF(4).
C
      READ(5,5010,END=900) ICCRN,NLON,NLAT,OFFLON,OFFLAT,ICYC,ILON,             D4
     1                     ILAT,NM,LVL                                          D4
      WRITE(6,6010) ICCRN,NLON,NLAT,OFFLON,OFFLAT,ICYC,ILON,ILAT,NM,LVL
C     READ(NM,'(A4)') NAME
      NAME=NC4TO8(NM)

C     CHECK IF THE OUTPUT IS TO BE ON A SHIFTED GRID ...

      IF ( ILON.LT.0 .OR. ILAT.LT.0) THEN
         OSHFTED=.TRUE.
         ILON=IABS(ILON)
         ILAT=IABS(ILAT)
         WRITE(6,6060)
      ELSE
         OSHFTED=.FALSE.
         WRITE(6,6055)
      ENDIF

      IF(ICCRN.EQ.0) THEN                                                       D4
          READ(5,5020,END=900) NDATE,KHEM,ENDLON,ENDLAT,USVALU                  D4
          IF(KHEM.EQ.0 .OR. KHEM.EQ.3 ) THEN
              WRITE(6,6030) NDATE
          ELSE
              WRITE(6,6035) NDATE,KHEM,ENDLON,ENDLAT,USVALU
          ENDIF
      ENDIF

C
C     * READ IN THE DATA FROM INPUT INTO ARRAY AF.
C
      NREC=0
   50 IF(ICCRN.NE.0)THEN
          CALL GETFLD2(1,G,NC4TO8("GRID"),0,0,0,IBUF,MAXX,OK)
          IF(.NOT.OK) THEN
              IF(NREC.EQ.0) CALL                   XIT('LLALL',-1)
              CALL                                 XIT('LLALL',0)
          ENDIF
          WRITE(6,6025) IBUF
          IF(NREC.EQ.0) THEN
              IMAX = IBUF(5)*IBUF(6)
              NLON=IBUF(5)
              NLAT=IBUF(6)
              KHEM=IBUF(7)
              IF(KHEM.NE.0 .AND. KHEM.NE.3 ) THEN
                WRITE(6,6050)
                CALL                               XIT('LLALL',-2)
              ENDIF
              POLES=.TRUE.
              OFFLON=0.0E0
              OFFLAT=0.0E0
              ICYC=1
          ELSE
              IF((IBUF(5).NE.NLON).OR.(IBUF(6).NE.NLAT).OR.
     1           (IBUF(7).NE.KHEM)) CALL           XIT('LLALL',-101)
          ENDIF
      ELSE
          IMAX=NLON*NLAT
          CALL GETFLD2(1,G,NC4TO8("GRID"),0,0,0,IBUF,MAXX,OK)
          IF(.NOT.OK) THEN
              IF(NREC.EQ.0) CALL                   XIT('LLALL',-3)
              CALL                                 XIT('LLALL',0)
          ENDIF
          IF(NREC.EQ.0) THEN
            IOLAT=NLAT
            IILON=IBUF(5)
            IILAT=IBUF(6)
            KKHEM=IBUF(7)
          ELSE
            IF((IBUF(5).NE.IILON).OR.(IBUF(6).NE.IILAT).OR.
     1         (IBUF(7).NE.KKHEM)) CALL            XIT('LLALL',-102)
            NLAT=IOLAT
          ENDIF
      ENDIF

      DO 100 K=1,IMAX
          J=((K-1)/NLON)+1
          I=K-(J-1)*NLON
          AF(I,J)=G(K)
  100 CONTINUE

      IF(NREC.EQ.0) THEN

C         * BUILT UP THE LONGITUDE AND LATITUDE ARRAYS ASSOCIATED WITH
C         * THE INPUT AND OUTPUT FIELDS.

          IF(ICCRN.NE.0) THEN
              DOLDLON=360.E0/(FLOAT(NLON-1))
              DOLDLON=1.E-10*ANINT(1.E10*DOLDLON)
              DOLDLAT=(180.E0-(2.E0*OFFLAT))/(FLOAT(NLAT-1))
              DOLDLAT=1.E-10*ANINT(1.E10*DOLDLAT)
C         PRINT *,'  DOLDLAT= ',DOLDLAT
          ELSE
              IF(KHEM.EQ.0 .OR. KHEM.EQ.3) THEN
                  IF(ICYC.NE.0) THEN
                      DOLDLON=360.E0/(FLOAT(NLON-1))
                  ELSE
                      DOLDLON=360.E0/(FLOAT(NLON))
                  ENDIF
                  DOLDLON=1.E-10*ANINT(1.E10*DOLDLON)
                  DOLDLAT=ABS(180.E0-(2.E0*OFFLAT))/(FLOAT(NLAT-1))
                  DOLDLAT=1.E-10*ANINT(1.E10*DOLDLAT)
              ELSE
                  LONEXT = ENDLON-OFFLON
C                 LONEXT = CVMGT(LONEXT+360.,LONEXT,LONEXT.LT.0.)
                  IF(LONEXT.LT.0.E0) LONEXT=LONEXT+360.E0
                  LATEXT = ABS(ENDLAT-OFFLAT)
                  DOLDLON= LONEXT/(FLOAT(NLON-1))
                  DOLDLON=1.E-10*ANINT(1.E10*DOLDLON)
                  DOLDLAT= LATEXT/(FLOAT(NLAT-1))
                  DOLDLAT=1.E-10*ANINT(1.E10*DOLDLAT)
              ENDIF
          ENDIF
C
          DO 105 I=1,NLON
             OLDLON(I) = OFFLON + (FLOAT(I-1))*DOLDLON
C            OLDLON(I) = CVMGT(OLDLON(I), OLDLON(I)-360.,
C    1                   OLDLON(I).LE.360.)
             IF(OLDLON(I).GT.360.E0) OLDLON(I)=OLDLON(I)-360.E0
  105     CONTINUE
          DO 110 J=1,NLAT
              IF(OFFLAT.GT.90.E0) THEN
                  OLDLAT(J) = -90.E0 + OFFLAT - (FLOAT(J-1))*DOLDLAT
              ELSE
                  OLDLAT(J) = -90.E0 + OFFLAT + (FLOAT(J-1))*DOLDLAT
              ENDIF
C         IF(ABS(OLDLAT(J)).LE.1.E-9) OLDLAT(J)=0.0
  110     CONTINUE
C
C         * TO EASE THE INTERPOLATION SCHEME, THE LATITUDE AND LONGITUDE ARRAYS,
C         * OLDLAT AND OLDLON, ASSOCIATED WITH NON-GLOBAL FILE WILL BE EXTENDED
C         * TO COVER THE WHOLE GLOBE ( THE LONGITUDE ARRAY IS NOT CYCLIC ).
C         * WE WILL VERIFY THE COMMENSURABILTY THE LONGITUDE GRID OVER A
C         * 360 DEGREE CYCLE BY A TEST WITH GRNLON, BEING THE NUMBER
C         * ( REAL NUMBER ) OF LONGITUDES IN THE GLOBAL ARRAY, THAT SHOULD BE
C         * VERY CLOSE TO AN INTEGER. A SIMILAR TEST FOR THE LATITUDE IS
C         * DONE TO VERIFY THE COMMENSURABILITY OF THE EXTENDED ARRAY WITH
C         * 180 DEGREE. UNDER SUCCESSFULL COMPLETION OF THOSE TESTS, NLONG AND
C         * NLATG WILL DESIGNATE THE NUMBER OF LONGITUDES AND LATITUDES IN THE
C         * GLOBAL ARRAY. NLATG MAY OR MAY NOT CONTAIN THE POLES AS LATITUDES
C         * DEPENDING IF THE EXTENDED ARRAY DOES CONTAIN IT OR NOT. ASLO THE
C         * VALUE OF OFFLAT WILL BE UPDATED TO MAKE IT CONSISTENT WITH THE
C         * GLOBAL ARRAY OF LATITUDES.
C
          IF((ICCRN.EQ.0).AND.(KHEM.NE.0).AND.(KHEM.NE.3)) THEN
              GRNLON = 360.000E0/DOLDLON
              IF( ABS(GRNLON-NINT(GRNLON)).GT. 0.0001E0 ) THEN
                  WRITE(6,6040)
                  CALL                             XIT('LLALL',-4)
              ELSE
                  NLONG = NINT(GRNLON)
              ENDIF
              GRNLAT = 180.000E0/DOLDLAT
              IF( ABS(GRNLAT-NINT(GRNLAT)).GT. 0.0001E0 ) THEN
                  WRITE(6,6045)
                  CALL                             XIT('LLALL',-5)
              ELSE
                  NLATG = NINT(GRNLAT)

C                 * TEST TO CHECK IF THE GLOBAL ARRAY OF LATITUDES
C                 * CONTAIN THE POLES OR NOT.

                  NLATGP1 = NLATG + 1
                  FLAG1 = .FALSE.
                  FLAG2 = .FALSE.
                  POLES = .FALSE.
                  DO 120 ITEST=1,NLATGP1
                      IF( ABS(OFFLAT+DOLDLAT*ITEST-180.E0).LT.
     &                    0.0001E0 ) FLAG1 = .TRUE.
                      IF( ABS(OFFLAT-DOLDLAT*ITEST).LT. 0.0001E0 )
     1                    FLAG2 = .TRUE.
                      POLES = FLAG1.AND.FLAG2
                      IF(POLES) NLATG = NLATG + 1
  120             CONTINUE
              ENDIF
C
C             * UPDATE OFFLAT, SO THAT NOW IT IS THE MEASURE OF
C             * THE FIRST LATITUDE OF THE GLOBAL ARRAY OLDLON.
C
              J = 0
              NEARPOL = .FALSE.
              IF( ENDLAT.LT.OFFLAT ) THEN
  130             J = J + 1
                  DISNPOL = 180.E0 - ( OFFLAT+(J-1)*DOLDLAT )
                  NEARPOL=(DISNPOL.LT.DOLDLAT).OR.
     1                    (ABS(DISNPOL).LT.0.0001E0)
                  IF(.NOT.NEARPOL) GOTO 130
                  IF(J.GT.NLATGP1) CALL            XIT('LLALL',-6)
                  JSCROLL = J - 1
                  OFFLAT = OFFLAT + JSCROLL*DOLDLAT
              ELSE
  140             J = J + 1
                  DISNPOL = OFFLAT-(J-1)*DOLDLAT
                  NEARPOL=(DISNPOL.LT.DOLDLAT).OR.
     1                    (ABS(DISNPOL).LT.0.0001E0)
                  IF(.NOT.NEARPOL) GOTO 140
                  IF(J.GT.NLATGP1) CALL            XIT('LLALL',-7)
                  JSCROLL = J - 1
                  OFFLAT = OFFLAT - JSCROLL*DOLDLAT
              ENDIF
C
              DO 150 I=1,NLONG
                  OLDLON(I) = OFFLON + (FLOAT(I-1))*DOLDLON
                  OLDLON(I) = MERGE(OLDLON(I), OLDLON(I)-360.E0,
     1                        OLDLON(I).LE.360.E0)
  150         CONTINUE
              DO 160 J=1,NLATG
                  IF(OFFLAT.GT.90.E0) THEN
                      OLDLAT(J) = -90.E0 + OFFLAT - (FLOAT(J-1))*DOLDLAT
                  ELSE
                      OLDLAT(J) = -90.E0 + OFFLAT + (FLOAT(J-1))*DOLDLAT
                  ENDIF
  160         CONTINUE
          ENDIF
C
          IF(OSHFTED) THEN
C
C           * SETUP FOR OUTPUT ON A SHIFTED GLOBAL LAT-LONG GRID ...
C
            DNEWLON = 360.E0/FLOAT(ILON)
            DNEWLON=1.E-10*ANINT(1.E10*DNEWLON)
            DNEWLAT = 180.E0/FLOAT(ILAT)
            DNEWLAT=1.E-10*ANINT(1.E10*DNEWLAT)
            DO 165 I=1,ILON
              NEWLON(I)=(0.5E0*DNEWLON)+(FLOAT(I-1))*DNEWLON
  165       CONTINUE
            DO 167 J=1,ILAT
              NEWLAT(J)=-90.E0+(0.5E0*DNEWLAT)+(FLOAT(J-1))*DNEWLAT
  167       CONTINUE
          ELSE
C
C           * SETUP FOR OUTPUT ON A THE REGULAR NON-SHIFTED GLOBAL
C           * LAT-LONG GRID ...
C
            DNEWLON = 360.E0/FLOAT(ILON-1)
            DNEWLON=1.E-10*ANINT(1.E10*DNEWLON)
            DNEWLAT = 180.E0/FLOAT(ILAT-1)
            DNEWLAT=1.E-10*ANINT(1.E10*DNEWLAT)
            DO 170 I=1,ILON
              NEWLON(I)=(FLOAT(I-1))*DNEWLON
  170       CONTINUE
            DO 180 J=1,ILAT
              NEWLAT(J)=-90.E0+(FLOAT(J-1))*DNEWLAT
  180       CONTINUE
          ENDIF
      ENDIF

      IF((ICCRN.EQ.0).AND.(KHEM.NE.0).AND.(KHEM.NE.3)) THEN
          IMAXG  = NLONG*NLATG
          KIN = 1
          ISTART = 1
          ISTOP  = NLON
          JSTART = JSCROLL + 1
          JSTOP  = JSTART + NLAT - 1
          DO 220 K = 1,IMAXG
              J=(K-1)/NLONG+1
              I=K-(J-1)*NLONG
              INSIDE = (I.GE.ISTART).AND.(I.LE.ISTOP).AND.
     1                 (J.GE.JSTART).AND.(J.LE.JSTOP)
              IF(INSIDE) THEN
                  AF(I,J) = G(KIN)
                  KIN = KIN + 1
              ENDIF
  220     CONTINUE
C
C         * THE REMAINDER OF ARRAY AF IS COMPLETED WITH VALUES IDENTICAL
C         * TO THE CLOSEST BOUNDARY VALUE OF THE ORIGINAL FILE.
C
          DO 225 I=1,NLONG
              DO 225 J=1,NLATG
                  BF(I,J) = AF(I,J)
  225     CONTINUE
          MIDLON = NLON + (NLONG+1-NLON)/2
          DO 260 J=1,NLATG
              DO 230 I=ISTART,ISTOP
                  IF(J.LT.JSTART) AF(I,J) = BF(I,JSTART)
                  IF(J.GT.JSTOP)  AF(I,J) = BF(I,JSTOP)
  230         CONTINUE
              DO 240 I=ISTOP+1,MIDLON
                  IF(J.LT.JSTART) AF(I,J) = BF(ISTOP,JSTART)
                  IF((J.GE.JSTART).AND.(J.LE.JSTOP)) THEN
                      AF(I,J) = BF(ISTOP,J)
                  ENDIF
                  IF(J.GT.JSTOP) AF(I,J) = BF(ISTOP,JSTOP)
  240         CONTINUE
              DO 250 I=MIDLON+1,NLONG
                  IF(J.LT.JSTART) AF(I,J) = BF(ISTART,JSTART)
                  IF((J.GE.JSTART).AND.(J.LE.JSTOP)) THEN
                      AF(I,J) = BF(ISTART,J)
                  ENDIF
                  IF(J.GT.JSTOP) AF(I,J) = BF(ISTART,JSTOP)
  250         CONTINUE
  260     CONTINUE
          NLON = NLONG
          NLAT = NLATG
          ICYC = 0
      ENDIF
C
C     * RE-ARRANGE THE VALUES IN ARRAY AF, ALONG WITH THE ARRAYS OLDLON AND
C     * OLDLAT, TO GET IN ORDER: SOUTH TO NORTH, AND TO HAVE THE FIRST
C     * LONGITUDE EAST OF GREENWICH. THE RE-ARRANGED GRID VALUES ARE STORED
C     * TEMPORARILY INTO ARRAY BF, WITH ASSOCIATED LONGITUDE AND LATITUDE
C     * VECTORS RLON AND RLAT, RESPECTIVELY. NOTE THAT IF THE ORIGINAL FIELD
C     * IS CYCLIC IN LONGITUDE, THEN THE REPEATED VALUE IS NOT COUNTED.
C     * BUT FIRST, WE SEARCH THE ARGUMENT OF OLDLON, CALLED LOCLON, SUCH THAT
C     * THE LOCATION OF THE ASSOCIATED POINT OF OLDLON IS THE CLOSEST TO
C     * (BUT NOT WEST OF) THE GREENWICH MERIDIAN. A SIMILAR SEARCH OF ARGUMENT
C     * OF OLDLAT IS DONE FOR THE CLOSEST LATITUDE TO THE SOUTH POLE.
C
      LOCLON=ISMIN(NLON,OLDLON,1)
      LOCLAT=ISMIN(NLAT,OLDLAT,1)
      IF((LOCLAT.NE.1).AND.(LOCLAT.NE.NLAT))THEN
        WRITE(6,6020)
        CALL                                       XIT('LLALL',-8)
      ENDIF
C
      IF((LOCLAT.EQ.1).AND.(LOCLON.EQ.1))THEN
        DO 305 J=1,NLAT
          RLAT(J)=OLDLAT(J)
          DO 300 I=1,NLON
            RLON(I)=OLDLON(I)
            BF(I,J)=AF(I,J)
  300   CONTINUE
  305   CONTINUE
      ELSE
        DO 320 J=1,NLAT
          IF(LOCLAT.EQ.1) THEN
              IY=J
          ELSE
              IY=NLAT-J+1
          ENDIF
          RLAT(J)=OLDLAT(IY)
          DO 315 I=1,NLON
            IX = LOCLON+I-1
            IF(IX.GT.NLON) IX = IX-NLON
            IF((ICYC.EQ.0).OR.((ICYC.NE.0).AND.(IX.NE.NLON))) THEN
              RLON(I)=OLDLON(IX)
              BF(I,J)=AF(IX,IY)
            ENDIF
  315     CONTINUE
  320   CONTINUE
      ENDIF
C
C     * ADD A REPEATED LONGITUDE IF NOT ALREADY THERE.
C
      IF( ICYC.EQ.0 ) THEN
          NNLON = NLON + 1

C         * ABORT IF EXPANSION INDEX EXCEEDS THE DECLARED ARRAY DIMENSIONS.

          IF (NNLON.GT.NBI) CALL                   XIT('LLALL',-9)

          RLON(NNLON) = RLON(NLON) + DOLDLON
          DO 400 J=1,NLAT
              BF(NNLON,J) = BF(1,J)
  400     CONTINUE
          ICYC = 1
      ELSE
          NNLON = NLON
      ENDIF

C
C     * IF BOTH POLES ARE NOT INCLUDED ( POLES = .FALSE. ) ADD TWO
C     * LATITUDES OF DATA BEFORE THE FIRST AND BEYOND THE LAST LATITUDE.
C
      IF(.NOT.POLES.AND..NOT.OSHFTED) THEN

          NLATP1 = NLAT + 1
          NLATP2 = NLAT + 2

C         * ABORT IF EXPANSION INDEX EXCEEDS THE DECLARED ARRAY DIMENSIONS.

          IF (NLATP2.GT.NBJ) CALL                  XIT('LLALL',-10)

          NOPP   = NLON/2

C         * EXTEND THE ARRAY RLAT.

          DO 510 J = 1,NLAT
              JP1 = J + 1
              RXLAT(JP1) = RLAT(J)
              DO 500 I = 1,NNLON
                  AF(I,JP1) = BF(I,J)
  500         CONTINUE
  510     CONTINUE
          DO 530 J = 2,NLATP1
              DO 520 I = 1,NNLON
                  BF(I,J) = AF(I,J)
  520         CONTINUE
  530     CONTINUE
          RXLAT(1)      = RXLAT(2) - DOLDLAT
          RXLAT(NLATP2) = RXLAT(NLATP1) + DOLDLAT
C
          DO 540 J = 1,NLATP2
              RLAT(J) = RXLAT(J)
  540     CONTINUE
C
C         * ASSIGN VALUES TO THE EXTRA TWO LATITUDES. IF NLON IS EVEN,
C         * THEN THE OPPOSITE POINT ON THE SAME LATITUDE IS GIVEN TO
C         * THE POINT OF THE EXTENDED LATITUDE.
C         * EG:            A(I,NLATP2) = A(I+NOPP,NLATP1).
C         * OTHERWISE IF NLON IS ODD, THEN AN AVERAGE VALUE TO THE TWO
C         * NEAREST POINTS OPPOSITE TO THE PRIMARY POINT IS ASSIGNED TO
C         * THE POINT OF THE EXTENDED LATITUDE.
C
          DO 550 I = 1,NNLON
              IOPP = MOD(I+NOPP,NLON)
              IF(IOPP.EQ.0) IOPP = NLON
              IF(MOD(NLON,2).EQ.0) THEN
                  BF(I,1)      = BF(IOPP,2)
                  BF(I,NLATP2) = BF(IOPP,NLATP1)
              ELSE
                  BF(I,1) = (BF(IOPP,2)+BF(IOPP+1,2))/2.E0
                  BF(I,NLATP2) = (BF(IOPP,NLATP1)+
     &              BF(IOPP+1,NLATP1))/2.E0
              ENDIF
  550     CONTINUE
          NLAT = NLATP2
      ENDIF

C     * PERFORM THE LINEAR INTERPOLATION IN LONGITUDE AND THEN IN
C     * LATITUDE. THE INTERPOLATION USES THE TWO NEAREST POINT
C     * ( ON THE SAME LONGITUDE/LATITUDE ) OF THE ORIGINAL FILE.
C
C     * START THE LONGITUDE INTERPOLATION.
C
      DO 620 J = 1,NLAT
          DO 610 I = 1,ILON

C     * CREATE A TEMPORARY ARRAY VALID FOR A GIVEN LONGITUDE.

              DO 600 II = 1,NNLON
                  DIFF(II) = RLON(II) - NEWLON(I)
  600         CONTINUE

C     * FIND THE CLOSEST OLD LONGITUDE TO NEWLON(J)

              IK = ISAMIN(NNLON,DIFF,1)

C     * THERE ARE THREE POSSIBLE CASES TO CONSIDER:
C     * A.  [ RLON(IK) - NEWLON(I) ].GT.0 , THEN THE CLOSEST LONGITUDE
C     *     IS EAST OF NEWLON(I).
C     * B.  [ RLON(IK) - NEWLON(I) ].LT.0 , THEN THE CLOSEST LONGITUDE
C     *     IS WEST OF NEWLON(I).
C     * C.  [ RLON(IK) - NEWLON(I) ].EQ.0 , THEN NO INTERPOLATION IS
C     *     NEEDED.

              IF((RLON(IK)-NEWLON(I)).GT.0) THEN
                IF(IK.EQ.1) THEN
                    IKM1 = NNLON - 1
                    RLON(IKM1) = RLON(IKM1) - 360.0E0
                ELSE
                    IKM1 = IK - 1
                ENDIF
                CALL LININT(RLON(IKM1),BF(IKM1,J),RLON(IK),
     1                      BF(IK,J),NEWLON(I),AF(I,J))
                IF(IK.EQ.1) RLON(IKM1) = RLON(IKM1) + 360.0E0
              ELSE
                IF((RLON(IK)-NEWLON(I)).LT.0) THEN
                  IF(IK.GE.NNLON) THEN
                    IKP1 = MOD(IK+1,NNLON)
                    RLON(IKP1) = RLON(IKP1) + 360.0E0
                  ELSE
                    IKP1 = IK + 1
                  ENDIF
                  CALL LININT(RLON(IK),BF(IK,J),RLON(IKP1),
     1                        BF(IKP1,J),NEWLON(I),AF(I,J))
                  IF(IK.GE.NNLON) RLON(IKP1) = RLON(IKP1) - 360.0E0
                ELSE
                  AF(I,J) = BF(IK,J)
                ENDIF
              ENDIF
  610     CONTINUE
  620 CONTINUE
C
C     * START THE LATITUDE INTERPOLATION.
C
      DO 720 I = 1,ILON
          DO 710 J = 1,ILAT

C     * CREATE A TEMPORARY ARRAY VALID FOR A GIVEN LATITUDE.

              DO 700 JJ = 1,NLAT
                  DIFF(JJ) = RLAT(JJ) - NEWLAT(J)
  700         CONTINUE

C     * FIND THE CLOSEST OLD LATITUDE TO NEWLAT(J)

              JK = ISAMIN(NLAT,DIFF,1)

C     * THERE ARE THREE POSSIBLE CASES TO CONSIDER:
C     * A.  [ RLAT(JK) - NEWLAT(J) ].GT.0 , THEN THE CLOSEST LATITUDE
C     *     IS NORTH OF NEWLAT(J).
C     * B.  [ RLAT(JK) - NEWLAT(J) ].LT.0 , THEN THE CLOSEST LATITUDE
C     *     IS SOUTH OF NEWLAT(J).
C     * C.  [ RLAT(JK) - NEWLAT(J) ].EQ.0 , THEN NO INTERPOLATION IS
C     *     NEEDED.

              IF((RLAT(JK)-NEWLAT(J)).GT.0) THEN
                IF(JK.EQ.1) THEN
                    JKM1 = NLAT - 1
                ELSE
                    JKM1 = JK - 1
                ENDIF
                CALL LININT(RLAT(JKM1),AF(I,JKM1),RLAT(JK),
     1                      AF(I,JK),NEWLAT(J),BF(I,J))
              ELSE
                IF((RLAT(JK)-NEWLAT(J)).LT.0) THEN
                  IF(JK.GE.NLAT) THEN
                    JKP1 = MOD(JK + 1,NLAT)
                  ELSE
                    JKP1 = JK + 1
                  ENDIF
                  CALL LININT(RLAT(JK),AF(I,JK),RLAT(JKP1),
     1                        AF(I,JKP1),NEWLAT(J),BF(I,J))
                ELSE
                  BF(I,J) = AF(I,JK)
                ENDIF
              ENDIF
  710     CONTINUE
  720 CONTINUE
C
C     * FIX THE VALUE AT THE POLES. THE POLES ARE DEGENERATE POINTS.
C
      IF(.NOT.POLES .AND. .NOT.OSHFTED)THEN
        POLNORD = 0.E0
        POLSUD = 0.E0
        DO 800 I=1,ILON
            POLNORD = POLNORD + BF(I,ILAT)
            POLSUD  = POLSUD  + BF(I,1)
  800   CONTINUE
        POLNORD = POLNORD/FLOAT(ILON)
        POLSUD  = POLSUD/FLOAT(ILON)
        DO 810 I=1,ILON
            BF(I,ILAT) = POLNORD
            BF(I,1)    = POLSUD
  810   CONTINUE
      ENDIF
C
C     * FOR NON-GLOBAL ARRAYS, SUBSTITUTE THE USER VALUE OUTSIDE THE
C     * INTERPOLATED AREA.
C
      IF((ICCRN.EQ.0).AND.(KHEM.NE.0).AND.(KHEM.NE.3)) THEN

C         * FIND THE INDEXES OF AF(I,J) CORRESPONDING TO
C         * OFFLON,OFFLAT,ENDLON,ENDLAT.

          ILONM1 = ILON - 1
          K = 0
          DO 820 I=1,ILONM1
              DIFF(I) = NEWLON(I) - OFFLON
              IF(DIFF(I).GE.0) THEN
                 K = K + 1
                 IF(K.EQ.1) ISTART = I
              ENDIF
  820     CONTINUE
          IF(ISTART.EQ.ILON) ISTART = 1
          K = 0
          DO 830 I=1,ILONM1
              IP1     = I + 1
              DIFF(I) = NEWLON(IP1) - ENDLON
              IF(DIFF(I).GE.0) THEN
                  K = K + 1
                  IF(K.EQ.1) ISTOP = I
              ENDIF
  830     CONTINUE
          IF(ISTOP.EQ.1) ISTOP = ILON

C         * CHANGE THE SIGNICANCE OF OFFLAT AND ENDLAT IF THE
C         * ORIGINAL FILE WAS ORDERED FROM NORTH TO SOUTH.

          IF(OFFLAT.GT.ENDLAT) THEN
             TLAT   = OFFLAT
             OFFLAT = ENDLAT
             ENDLAT = TLAT
          ENDIF

          K = 0
          DO 840 J=1,ILAT
              DIFF(J) = NEWLAT(J) - OFFLAT + 90.0E0
              IF(DIFF(J).GE.0) THEN
                  K = K + 1
                  IF(K.EQ.1) JSTART = J
              ENDIF
  840     CONTINUE
          IF(DIFF(JSTART).LT.0) JSTART = JSTART + 1
          K = 0
          DO 850 J=1,ILAT
              JM1 = J - 1
              DIFF(J) = NEWLAT(J) - ENDLAT + 90.0E0
              IF(DIFF(J).GE.0) THEN
                  K = K + 1
                  IF(K.EQ.1) JSTOP = JM1
              ENDIF
  850     CONTINUE

C         * ASSIGN THE USER VALUE OUTSIDE THE INTERPOLATED AREA.

          IF(ENDLON.GT.OFFLON) THEN
              DO 860 I=1,ILON
                  DO 860 J=1,ILAT
                      INSIDE = (I.GE.ISTART).AND.(I.LE.ISTOP).AND.
     1                         (J.GE.JSTART).AND.(J.LE.JSTOP)
                      IF(INSIDE) THEN
                          AF(I,J) = BF(I,J)
                      ELSE
                          AF(I,J) = USVALU
                      ENDIF
  860         CONTINUE
          ELSE
              DO 870 I=1,ILON
                  DO 870 J=1,ILAT
                      INSIDE = ((I.GE.ISTART).AND.(J.GE.JSTART).AND.
     1                         (J.LE.JSTOP)).OR.((I.LE.ISTOP).AND.
     2                         (J.GE.JSTART).AND.(J.LE.JSTOP))
                      IF(INSIDE) THEN
                          AF(I,J) = BF(I,J)
                      ELSE
                          AF(I,J) = USVALU
                      ENDIF
  870         CONTINUE
          ENDIF
      ELSE
          DO 880 J=1,ILAT
            DO 875 I=1,ILON
              AF(I,J)=BF(I,J)
  875       CONTINUE
  880     CONTINUE
      ENDIF
C
C     * OUTPUT THE RESULT.
C
C     K=0
      DO 895 J=1,ILAT
        K=(J-1)*ILON
        DO 885 I=1,ILON
C         K=K+1
C         G(K)    = AF(I,J)
          G(K+I)  = AF(I,J)
  885   CONTINUE
  895 CONTINUE
C
      IF(ICCRN.NE.0)THEN
        IBUF(5)=ILON
        IBUF(6)=ILAT
        IBUF(8)=1
      ELSE
C       CALL SETLAB(IBUF,4HGRID,NDATE,NAME,LVL,ILON,ILAT,0,1)
        IBUF(1)=NC4TO8("GRID")
        IF(NDATE.NE.-1)THEN
          IBUF(2)=NDATE
        ENDIF
        IF(NAME.NE.NC4TO8("  -1"))THEN
          IBUF(3)=NAME
        ENDIF
        IF(LVL.NE.-1) THEN
          IBUF(4)=LVL
        ENDIF
        IBUF(5)=ILON
        IBUF(6)=ILAT
        IBUF(7)=0
        IBUF(8)=1
      ENDIF
      CALL PUTFLD2(2,G,IBUF,MAXX)
      WRITE(6,6025) IBUF
      NREC=NREC+1
C     IF(ICCRN.NE.0)GO TO 50
      GO TO 50
C
C     * E.O.F. ON INPUT.
C
  900 CALL                                         XIT('LLALL',-11)
C------------------------------------------------------------------------------
 5010 FORMAT(10X,3I5,2E10.0,3I5,1X,A4,I5)                                       D4
 5020 FORMAT(10X,2I5,2F10.2,5X,E10.0)                                           D4
 6010 FORMAT('0ICCRN,NLON,NLAT,OFFLON,OFFLAT,ICYC,ILON,ILAT,NAME,LEVEL',
     1 ' =',3I5,2(5X,F6.2),5X,3I5,1X,A4,I5)
 6020 FORMAT('0LATITUDE ROW ORDER IS NOT SOUTH-NORTH OR NORTH-SOUTH')
 6025 FORMAT('0',A4,I10,2X,A4,I10,4I6)
 6030 FORMAT('0NON-REGULAR LAT-LON GRID FILE',/,'  NDATE =',I5)
 6035 FORMAT('0NON-REGULAR GRID FILE',/,'  NDATE,KHEM,',
     1 'ENDLON,ENDLAT,USVALU =',2I5,2F10.2,5X,E10.3)
 6040 FORMAT('0TEST OF COMMENSURABILITY OF THE LONGITUDE WITH A 360',
     1  ' DEGREE CYCLE HAS FAILED')
 6045 FORMAT('0TEST OF COMMENSURABILITY OF THE LATITUDE WITH A 180',
     1  ' DEGREE HAS FAILED')
 6050 FORMAT('0 LLALL: ERROR, INPUT DATA IS NOT GLOBAL')
 6055 FORMAT('0 LLALL: OUTPUT FIELD(S) WILL BE ON THE REGULAR ',
     1                 'NON-SHIFTED GLOBAL LAT-LONG GRID')
 6060 FORMAT('0 LLALL: OUTPUT FIELD(S) WILL BE ON THE SHIFTED ',
     1                 'GLOBAL LAT-LONG GRID')

      END
