      PROGRAM NHALL 
C     PROGRAM NHALL (GIN,       GOUT,       INPUT,       OUTPUT,        )       D2
C    1         TAPE1=GIN, TAPE2=GOUT, TAPE5=INPUT, TAPE6=OUTPUT)
C     ----------------------------------------------------------                D2
C                                                                               D2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       D2
C     AUG 25/93 - R. HOURSTON (ALLOW FRACTIONAL LATITUDE GRID SPACING)          
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     NOV   /83 - S. LAMBERT                                                    
C                                                                               D2
CNHALL   - CONVERT HEMISPHERIC LAT.LONG. TO GLOBAL LAT. LONG. GRID      1  1 C  D1
C                                                                               D3
CAUTHOR  - S. LAMBERT                                                           D3
C                                                                               D3
CPURPOSE - PROGAM CONVERTS A HEMISPHERIC OR HEMISPERIC CAP                      D3
C          LAT LONG GRID TO A GLOBAL LAT LONG GRID. THE TOP                     D3
C          ROW MUST BE AT THE NORTH POLE AND THE SPACING IN                     D3
C          LATITUDE MUST BE SUCH THAT WHEN THE GRID IS EXTENDED                 D3
C          SOUTHWARDS, THE EQUATOR WILL BECOME ONE OF THE ROWS.                 D3
C          NOTE - INPUT GRID CANNOT EXTEND SOUTH OF THE EQUATOR.                D3
C                                                                               D3
CINPUT FILE...                                                                  D3
C                                                                               D3
C      GIN = HEMISPHERIC INPUT FILE                                             D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      GOUT = GLOBAL LAT. LONG. OUTPUT FILE                                     D3
C 
CINPUT PARAMETERS...
C                                                                               D5
C      LATM = LATITIDE OF THE BOTTOM ROW OF THE INPUT GRID.                     D5
C      KSYM =  1 , OUTPUT GRID IS      SYMMETRIC ABOUT THE EQUATOR.             D5
C           = -1 , OUTPUT GRID IS ANTI-SYMMETRIC ABOUT THE EQUATOR.             D5
CEXAMPLE OF INPUT CARD...                                                       D5
C                                                                               D5
C*  NHALL.    0    1                                                            D5
C---------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_MAXLONP1LAT

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)


      DIMENSION F(SIZES_LONP1xLAT),G(SIZES_LONP1xLAT),
     & D(SIZES_MAXLONP1LAT)
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
      COMMON/JCOM/JBUF(8),JDAT(SIZES_LONP1xLATxNWORDIO)
      LOGICAL OK
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
C 
C---------------------------------------------------------------------- 
C 
      NFF=4 
      CALL JCLPNT(NFF,1,2,5,6)
      NRECS=0 
      REWIND 1
      REWIND 2
C 
C     * LATM IS THE LATITUDE OF THE BOTTOM ROW OF THE INPUT GRID. 
C     * KSYM GREATER THAN ZERO OUTPUT GRID IS SYMMETRIC ABOUT 
C     *      THE EQUATOR
C     * KSYM LESS THAN ZERO OUTPUT GRID IS ANTI-SYMMETRIC ABOUT 
C     *      THE EQUATOR
C 
      READ(5,5000) LATM,KSYM                                                    D4
      IF(LATM.LT.0)THEN 
        WRITE(6,6010) LATM
        CALL                                       XIT('NHALL' ,-101) 
      ENDIF 
  100 CALL GETFLD2(1,F,NC4TO8("GRID"),0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NRECS.EQ.0) CALL                        XIT('NHALL' ,-1) 
        WRITE(6,6020) NRECS 
        CALL                                       XIT('NHALL' ,0)
      ENDIF 
      IF(NRECS.EQ.0) WRITE(6,6030) IBUF 
      NX=IBUF(5)
      NX1=NX-1
      NYF=IBUF(6) 
C 
C     * COMPUTE NUMBER OF ROWS IN THE OUTPUT GRID 
C 
      RLINC=(90.E0-FLOAT(LATM))/(FLOAT(NYF)-1.E0)
      NYG=INT(180.E0/RLINC) + 1
      NWDSF=NX*NYF
      NWDSG=NX*NYG
      IOFF=NWDSG-NWDSF
C 
C     * COPY INPUT GRID TO THE APPROPRIATE PART OF THE OUTPUT 
C     * GRID
C 
      DO 150 I=1,NWDSF
  150 G(I+IOFF)=F(I)
      JEQ=(NYG+1)/2 
      JEQ1=JEQ-1
      JBTM=NYG-NYF+1
      IF(LATM.EQ.0) GO TO 325 
C 
C     * FILL IN THE TOPICS IF REQUIRED.  FOR THE SYMMETRIC CASE,
C     * THE ZONAL AVERAGE AT THE BOTTOM ROW OF THE INPUT GRID IS
C     * PLACED AT THE EQUATOR OF THE OUTPUT GRID.  THE ROWS 
C     * BETWEEN THE BOTTOM ROW OF THE INPUT GRID AND THE EQUATOR
C     * ARE FILLED IN BY LINEARILY INTERPOLATING THE VALUES 
C     * BETWEEN THE EQUATOR AND THE BOTTOM ROW OF THE INPUT GRID. 
C     * THE PROCEDURE IS THE SAME FOR THE ANTI-SYMMETRIC CASE,
C     * EXCEPT THE EQUATOR IS ASSIGNED A VALUE OF ZERO. 
C 
      AVG=0.0E0 
      IF(KSYM.LT.0) GO TO 275 
      DO 200 I=1,NX1
  200 AVG=AVG+F(I)
  275 AVG=AVG/FLOAT(NX1)
      DO 250 I=1,NX 
      G(I+JEQ1*NX)=AVG
  250 D(I)=F(I)-AVG 
      LYT=JBTM-JEQ
      LYT1=LYT-1
      DO 300 J=1,LYT1 
      DO 300 I=1,NX 
      K=NX*JEQ
  300 G(K+(J-1)*NX+I)=AVG+FLOAT(J)*D(I)/FLOAT(LYT)
C 
C     * FILL IN THE SOUTHERN HEMISPHERE 
C 
  325 SYM=1.0E0 
      IF(KSYM.LT.0) SYM=-1.0E0
      DO 350 J=1,JEQ1 
      DO 350 I=1,NX 
      K=NYG-J 
  350 G(I+(J-1)*NX)=SYM*G(I+K*NX) 
      CALL SETLAB(JBUF,NC4TO8("GRID"),IBUF(2),IBUF(3),IBUF(4),
     +                                             NX,NYG,0,1)
      CALL PUTFLD2(2,G,JBUF,MAXX)
      IF(NRECS.EQ.0) WRITE(6,6030) JBUF 
      NRECS=NRECS+1 
      GO TO 100 
C 
C---------------------------------------------------------------------- 
C 
 5000 FORMAT(10X,2I5)                                                           D4
 6010 FORMAT('0INPUT GRID MUST BE COMPLETELY IN THE NORTHERN ',
     1       'HEMISPHERE. BOTTOM ROW =',I5)
 6020 FORMAT('0NHALL COMPUTED ',I5,' RECORDS')
 6030 FORMAT(2X,A4,I10,1X,A4,I10,4I7)
      END
