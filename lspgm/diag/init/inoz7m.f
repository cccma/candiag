      PROGRAM INOZ7M
C     PROGRAM INOZ7M  (ICTL,       OZLX,       OZONE,       OUTPUT,     )       I2
C    1          TAPE99=ICTL, TAPE1=OZLX, TAPE2=OZONE, TAPE6=OUTPUT)
C     -------------------------------------------------------------             I2
C                                                                               I2
C     APR 05/08 - M.LAZARE (INCREASED DIMENSIONS TO HANDLE T127 )               I2
C     JUL 27/07 - M.LAZARE (NEW: PRODUCES GRID FIELDS AND ON MDAY ONLY)
C     JUL 03/03 - A.IRWIN,F.MAJAESS (PREVIOUS VERSION INITOZ7)
C     OCT 29/96 - M.LAZARE (REVISE FOR NEW WANG INPUT OZONE DATASET)
C     SEP 12/96 - J.DE GRANDPRE (PREVIOUS VERSION INITOZ6)
C                                                                               I2
CINOZ7M  - PRODUCES MONTHLY GRIDS OF MEAN OZONE VALID AT MID-MONTH      2  1    I1
C                                                                               I3
CAUTHORS - M.LAZARE                                                             I3
C                                                                               I3
CPURPOSE - INTERPOLATES INITIAL MONTHLY ZONAL CROSS-SECTIONS OF PPMV            I3
C          OZONE FROM T42 GAUSSIAN-GRID TO THE GAUSSIAN GRID, AND THEN          I3
C          FILLS 3-D GRID AND MULTIPLIES BY 1.E-6 TO CONVERT TO PPV.            I3
C          (WANG DATA)                                                          I3
C          THE MODEL ROUTINE OZON12 USES THIS RESULT, ALONG WITH THE            I3
C          SURFACE PRESSURE, TO CALCULATE THE OZONE AMOUNT BETWEEN              I3
C          FULL MODEL SIGMA SURFACES.                                           I3
C                                                                               I3
CINPUT FILES...                                                                 I3
C                                                                               I3
C      ICTL  = INITIALIZATION CONTROL DATASET (SEE ICNTRL5).                    I3
C      OZLX  = MONTHLY LATITUDINAL GLOBAL CROSS-SECTIONS OF OZONE IN            I3
C              PPMV (ON A T42 GAUSSIAN GRID AND A RELATIVELY DENSE              I3
C              PRESSURE GRID) OBTAINED FROM THE PCMDI OFFICE (WANG DATA).       I3
C                                                                               I3
COUTPUT FILE...                                                                 I3
C                                                                               I3
C      OZONE = MONTHLY GAUSSIAN GRIDS OF OZONE AMOUNT                           I3
C              (PPV) FOR EACH INPUT PRESSURE LEVEL, (ON LEVOZ LEVELS,           I3
C              WHERE LEVOZ IS THE NUMBER OF PRESSURE LEVELS IN THE              I3
C              ORIGINAL OZONE DATASET IN THE LLPHYS).                           I3
C              THERE ARE LEVOZ (=59) PRESSURE LEVEL (RECORDS) FOR EACH MONTH.   I3
C---------------------------------------------------------------------------
C
C
C     *    ILAT = SOUTH - NORTH NUMBER OF GRID POINTS.
C     *    ALAT = VECTOR OF INPUT T42 GAUSSIAN LATITUDES (S.P. TO N.P.) IN
C     *           ORIGINAL OZONE DATA (THE VALUES AT -90. AND 90. WERE ADDED
C     *           FOR COMPLETENESS BY COPYING THE ADJACENT VALUES AT BOTH
C     *           POLES).
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      PARAMETER(ILGM=256, ILGM1=ILGM+1, ILATM=128, LEVOZ=59,
     1          NLATG=64,NLAT=NLATG+2,
     2          MOGRID=ILGM1*ILATM, MOMAX=MOGRID,
     3          IGRID=NLATG*LEVOZ)

      LOGICAL OK

      REAL GH(MOGRID)
      REAL*8 SL(ILATM),CL(ILATM),WL(ILATM),WOSSL(ILATM),RADL(ILATM)
      REAL   DLAT(ILATM)
C
      REAL*8 SLI(NLATG),CLI(NLATG),WLI(NLATG),WOSSLI(NLATG),RADLI(NLATG)
      REAL   DLATI(NLATG)
      REAL ALAT(NLAT),OZZXG(NLATG,LEVOZ),OZZX(NLAT,LEVOZ)
      REAL O3(ILATM,LEVOZ)
C
      INTEGER NFDM(12),MMD(12),LBL(ILATM)
C
      COMMON/AOLD/G(IGRID)
      COMMON/ANEW/H(MOGRID)
      COMMON/ICOM/IBUF(8),IDAT(IGRID)
      COMMON/JCOM/JBUF(8),JDAT(MOMAX)
C
      EQUIVALENCE (G(1),OZZXG(1,1))
C
      DATA NFDM/1,32,60,91,121,152,182,213,244,274,305,335/
      DATA  MMD/ 16, 46, 75,106,136,167,197,228,259,289,320,350/

C     ***************************************************************
C     *    VECTOR OF PRESSURE LEVELS IN ORIGINAL OZONE DATA.        *
C     *    THE SIZE OF THIS VECTOR IS 59. ITS VALUES ARE GIVEN IN   *
C     *    THE DATASET BELOW FOR REFERENCE, ALTHOUGH THEY ARE NOT   *
C     *    USED EXPLICITLY IN THE PROGRAM (THE CORRESPONDING        *
C     *    HEIGHTS IN KILOMETRES ARE ALSO GIVEN).                   *
C     *                                                             *
C          ------------ LAYER PRESSURE (MB) -------------->         *
C     * 2.84191E-01 3.25350E-01 3.71936E-01 4.24615E-01 4.84153E-01 *
C     * 5.51432E-01 6.27490E-01 7.13542E-01 8.11039E-01 9.21699E-01 *
C     * 1.04757E+00 1.19108E+00 1.35511E+00 1.54305E+00 1.75891E+00 *
C     * 2.00739E+00 2.29403E+00 2.62529E+00 3.00884E+00 3.45369E+00 *
C     * 3.97050E+00 4.57190E+00 5.27278E+00 6.09073E+00 7.04636E+00 *
C     * 8.16371E+00 9.47075E+00 1.10000E+01 1.27894E+01 1.48832E+01 *
C     * 1.73342E+01 2.02039E+01 2.35659E+01 2.75066E+01 3.21283E+01 *
C     * 3.75509E+01 4.39150E+01 5.13840E+01 6.01483E+01 7.04289E+01 *
C     * 8.24830E+01 9.66096E+01 1.13155E+02 1.32514E+02 1.55122E+02 *
C     * 1.81445E+02 2.11947E+02 2.47079E+02 2.87273E+02 3.32956E+02 *
C     * 3.84573E+02 4.42610E+02 5.07601E+02 5.80132E+02 6.60837E+02 *
C     * 7.50393E+02 8.49521E+02 9.58981E+02 1.00369E+03             *
C     *                                                             *
C          ------------ LAYER ALTITUDE (KM) -------------->         *
C     * 5.75000E+01 5.65000E+01 5.55000E+01 5.45000E+01 5.35000E+01 *
C     * 5.25000E+01 5.15000E+01 5.05000E+01 4.95000E+01 4.85000E+01 *
C     * 4.75000E+01 4.65000E+01 4.55000E+01 4.45000E+01 4.35000E+01 *
C     * 4.25000E+01 4.15000E+01 4.05000E+01 3.95000E+01 3.85000E+01 *
C     * 3.75000E+01 3.65000E+01 3.55000E+01 3.45000E+01 3.35000E+01 *
C     * 3.25000E+01 3.15000E+01 3.05000E+01 2.95000E+01 2.85000E+01 *
C     * 2.75000E+01 2.65000E+01 2.55000E+01 2.45000E+01 2.35000E+01 *
C     * 2.25000E+01 2.15000E+01 2.05000E+01 1.95000E+01 1.85000E+01 *
C     * 1.75000E+01 1.65000E+01 1.55000E+01 1.45000E+01 1.35000E+01 *
C     * 1.25000E+01 1.15000E+01 1.05000E+01 9.50000E+00 8.50000E+00 *
C     * 7.50000E+00 6.50000E+00 5.50000E+00 4.50000E+00 3.50000E+00 *
C     * 2.50000E+00 1.50000E+00 5.00000E-01 1.18000E-01             *
C     ***************************************************************

      DATA MAXX/MOGRID/, MAXLAT/ILATM/
C-----------------------------------------------------------------------
      NFF=4
      CALL JCLPNT(NFF,99,1,2,6)
C
C     * GET PARAMETERS FROM CONTROL FILE.
C
      REWIND 99
      READ(99,END=902) LABL,ILEV,(XXX,L=1,ILEV)
      READ(99,END=903) LABL,IXX,IXX,ILG,ILAT,IXX,IDAY,GMT
      ILG1=ILG+1
      WRITE(6,6010)  ILEV,ILG,ILAT,IDAY,GMT
      IF(ILAT*ILG1.GT.MOGRID) CALL                 XIT('INOZ7M',-1)
C
C     * GET LAT-HGT CROSS-SECTION FOR EACH MONTH (ARRAY NLATG X LEVOZ),
C     * WHERE NLATG IS THE NUMBER OF T42 GAUSSIAN LATITUDES AND LEVOZ
C     * IS THE NUMBER OF PRESSURE LEVELS.
C
      DO 290 MONTH=1,12
      NDAY=NFDM(MONTH)
      CALL GETFLD2(-1,G,NC4TO8("ZONL"),NDAY,NC4TO8("OZWA"),0,
     +                                          IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('INOZ7M',-2)
      WRITE(6,6020) IBUF
      IF(NLATG.NE.IBUF(5)) CALL                    XIT('INOZ7M',-3)
      IF(LEVOZ.NE.IBUF(6)) CALL                    XIT('INOZ7M',-4)
C
      IF(ILAT.GT.MAXLAT.OR.NLAT.GT.MAXLAT) CALL    XIT('INOZ7M',-5)
C
C     * SKIP FURTHER PROCESSING IF MODEL GRID HAS NLATG GAUSSIAN LATITUDES.
C
      IF(ILAT.EQ.NLATG)       THEN
        DO 50 L=1,LEVOZ
        DO 50 J=1,ILAT
          O3(J,L)=OZZXG(J,L)
   50   CONTINUE
        GO TO 170
      ENDIF
C
C     * SET DLATI TO INPUT T42 GAUSSIAN LATITUDES (DEG).
C
      NLATH=NLATG/2
      CALL GAUSSG(NLATH,SLI,WLI,CLI,RADLI,WOSSLI)
      CALL  TRIGL(NLATH,SLI,WLI,CLI,RADLI,WOSSLI)
      DO 100 J=1,NLATG
  100 DLATI(J)=RADLI(J)*180.E0/3.14159E0
C
C     * ADD EXTRA LATITUDES AT S.P. AND N.P. FOR INTERPOLATION PURPOSES.
C
      ALAT(1)=-90.E0
      ALAT(NLAT)=90.E0
      DO 103 L=1,LEVOZ
        OZZX(1,L)=OZZXG(1,L)
        OZZX(NLAT,L)=OZZXG(NLATG,L)
  103 CONTINUE
C
      DO 107 J=1,NLATG
        ALAT(J+1)=DLATI(J)
        DO 105 L=1,LEVOZ
          OZZX(J+1,L)=OZZXG(J,L)
  105   CONTINUE
  107 CONTINUE
C
C     * SET DLAT TO GAUSSIAN LATITUDES (DEG).
C
      ILATH=ILAT/2
      CALL GAUSSG(ILATH,SL,WL,CL,RADL,WOSSL)
      CALL  TRIGL(ILATH,SL,WL,CL,RADL,WOSSL)
      DO 110 J=1,ILAT
  110 DLAT(J)=RADL(J)*180.E0/3.14159E0
C
C     * PERFORM INTERPOLATION ON PRESSURE SURFACES TO MODEL GAUSSIAN LATITUDES.
C     * THE ARRAY LBL HOLDS THE POSITION IN THE ARRAY ALAT WHOSE CORRESPONDING
C     * LATITUDE IS CLOSEST TO, BUT SOUTH OF, (OR POSSIBLY EQUAL TO) THE
C     * GAUSSIAN LATITUDE DLAT(J); HENCE ONE VALUE FOR EACH GAUSSIAN LATITUDE.
C     * THE RESULTING ARRAY IS O3(ILAT,LEVOZ).
C
      DO 145 J=1,ILAT
        IB=0
        DO 140 I=1,NLAT
          IF(DLAT(J).LT.ALAT(I).AND.IB.EQ.0) IB=I
  140   CONTINUE
        IF(IB.EQ.0) THEN
          WRITE(6,6030) DLAT(J)
          CALL                                     XIT('INOZ7M',-6)
        ENDIF
        LBL(J)=IB-1
  145 CONTINUE
C
      DO 160 L=1,LEVOZ
      DO 150 J=1,ILAT
        K=LBL(J)
        CALL LININT(ALAT(K),OZZX(K,L),ALAT(K+1),OZZX(K+1,L),DLAT(J),
     1              O3(J,L))
  150 CONTINUE
  160 CONTINUE
C
C     * EVALUATE THE AMOUNT OF OZONE (IN VOLUME MIXING RATIO).
C
  170 CONTINUE
      MDAY=MMD(MONTH)
      CALL SETLAB(JBUF,NC4TO8("GRID"),MDAY,NC4TO8("  OZ"),
     1            0,ILG1,ILAT,0,1)
      DO 200 L=1,LEVOZ
        NIJ=0
        DO 180 J=1,ILAT
          H(J)=O3(J,L)*1.0E-6
          DO I=1,ILG1
            NIJ=NIJ+1
            GH(NIJ)=H(J)
          END DO
  180   CONTINUE
        JBUF(4)=L
        CALL PUTFLD2(2,GH,JBUF,MAXX)
        WRITE(6,6020) JBUF
  200 CONTINUE
  290 CONTINUE
C
      CALL                                         XIT('INOZ7M',0)
C
C     * E.O.F. ON FILE ICTL.
C
  902 CALL                                         XIT('INOZ7M',-7)
  903 CALL                                         XIT('INOZ7M',-8)
C--------------------------------------------------------------------
 6010 FORMAT('0 ILEV,ILG,ILAT,IDAY,GMT =',4I5,2X,F5.2)
 6020 FORMAT(' ',A4,I10,2X,A4,I10,4I10)
 6030 FORMAT(////,5X,'**** INTERPOLATION ATTEMPTED OUTSIDE THE RANGE ',
     1'OF THE LATITUDE GRID (-90 TO 90 DEG OF LAT) FOR LAT= ',F6.2,////)
      END
