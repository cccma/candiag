      PROGRAM PARMSUB 
C     PROGRAM PARMSUB (IN,       OUT,       IN2,       OUTPUT,          )       G2
C    1           TAPE1=IN, TAPE2=OUT, TAPE3=IN2, TAPE6=OUTPUT)
C     --------------------------------------------------------                  G2
C                                                                               G2
C     NOV 25/03 - L. SOLHEIM (DO NOT PROCESS COMMENT LINES,                     G2
C                             SIMPLY COPY THEM TO THE OUTPUT UNITS)             G2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       
C     AUG 11/94 - F. MAJAESS (USE INTEGER*4 FOR C-ROUTINE ARG)                  
C     MAR 22/93 - E.CHAN   (INCREASE MAX NUMBER OF PARAMETERS TO 400)           
C     APR 03/92 - E.CHAN   (HARD CODE THE PARAMETER 'S' TO A '$')     
C     FEB 10/92 - E.CHAN   (REPLACE CALL TO JCLPNT WITH CALLS TO GETARG         
C                           AND OPEN STATEMENTS, REMOVE XIT(___,0) AND         
C                           CONVERT HOLLERITH TO ASCII)                       
C     DEC 20/91 - M.LAZARE (MODIFIED TO HANDLE 132 CHAR/LINE)
C     FEB 08/91 - F.MAJAESS (MODIFIED TO HANDLE 82 CHAR/LINE)                   
C     MAY 15/83 - R.LAPRISE,B.DUGAS,J.D.HENDERSON.
C                                                                               G2
CPARMSUB - PARAMETER SUBSTITUTION PROGRAM                               2  1    G1
C                                                                               G3
CAUTHOR  - R.LAPRISE.                                                           G3
C                                                                               G3
CPURPOSE - PARAMETER SUBSTITUTION PROGRAM.                                      G3
C          ANY CHARACTER STRING ENCLOSED IN A PAIR OF DOLLAR SIGNS IN           G3
C          FILE "IN" IS CONSIDERED TO BE THE NAME OF A PARAMETER TO BE          G3
C          REPLACED IN FILE "OUT" BY A SUBSTITUTION VALUE FROM FILE "IN2".      G3
C          THE EXCEPTION IS THE PARAMETER 'S' WHICH IS AUTOMATICALLY            G3
C          REPLACED BY A '$' WITHIN THE PROGRAM. A SUBSTITUTION VALUE           G3
C          FROM FILE "IN2" IS NOT REQUIRED IN THIS CASE.                        G3
C          NOTE - ONLY FILE "IN" IS REWOUND DURING EXECUTION AND OUTPUT         G3
C                 IS ON FILE "OUT" FOR ALL PARAMETER SETS.                      G3
C                 IF A FATAL ERROR OCCURS IN ANY SET,THE PROGRAM ABORTS.        G3
C                                                                               G3
CINPUT FILES...                                                                 G3
C                                                                               G3
C      IN    = INPUT FILE WITH 132 CHARACTER LINES CONTAINING CHARACTER         G3
C              STRING(S) ENCLOSED IN A PAIR OF DOLLAR SIGNS.                    G3
C              TWO CONSECUTIVE DOLLAR SIGNS ARE REPLACED BY ONE OF 36           G3
C              CHARACTER SPECIFIED IN SUBROUTINE PARINS,  BASED ON              G3
C              SUBSTITUTION SET NUMBER.                                         G3
C      IN2   = INPUT FILE CONTAINING VALUES OF THE FORM  NAME=VALUE,            G3
C              (THE COMMA IS PART OF THE SYNTAX).  THERE CAN BE MANY            G3
C              SUBSTITUTION VALUES ON A CARD. DO NOT USE THE CHARACTER          G3
C              (<) WHICH IS (?) ON A 029 PUNCH. A SUBSTITUTION CARD WHICH       G3
C              STARTS WITH   ,=X   IN COLUMNS 1 TO 4 CHANGES THE SEPARATOR      G3
C              FROM COMMA TO THE CHARACTER X FOR THAT CARD ONLY. THIS           G3
C              PERMITS THE INSERTION OF VALUES THAT CONTAIN COMMAS.             G3
C              IF A NAME IS REDEFINED,  THE ORIGINAL VALUE IS USED. MORE        G3
C              THAN ONE SUBSTITUTION SET CAN BE READ.  THEY ARE THEN            G3
C              SEPARATED BY A SLASH CARD (/ IN COLUMN 1).                       G3
C                                                                               G3
COUTPUT FILE...                                                                 G3
C                                                                               G3
C      OUT   = CONTAINS THE SUBSTITUTED FILE UPON TERMINATION.                  G3
C              OUTPUT IS ON FILE OUT FOR ALL PARAMETER SETS.                    G3
C                                                                               G3
CEXAMPLE OF INPUT/OUTPUT FILES...                                               G3
C                                                                               G3
C      INPUT FILE IN...                                                         G3
C                           A$D$B$D$CD$D$E$D$F                                  G3
C      INPUT FILE IN2...                                                        G3
C                           B=XY,  E=Z,                                         G3
C      OUTPUT FILE OUT...                                                       G3
C                           AXYCDZF                                             G3
C------------------------------------------------------------------------ 
C     * NPS   = NUMBER OF SUBSTITUTION SETS ON IN2. 
C     * NPCDS = NUMBER OF REPLACEMENT PARAMETER CARDS IN A SET ON IN2.  
C     * NPARM = NUMBER OF REPLACEMENT PARAMETERS IN A SET ON IN2. 
C     * NCARDS= NUMBER OF CARDS TO PROCESS IN FILE IN.  
C     * NUS   = NUMBER OF TIMES A REPLACEMENT PARAMETER HAS BEEN USED 
C     * OK    = .FALSE.  IF NO MORE CARDS TO BE READ IN FROM IN2. 
C 
      IMPLICIT INTEGER (A-Z)
C 
      LOGICAL      ALLOK,OK
      CHARACTER*512 FILNAM
      INTEGER*4 II
      CHARACTER*1 SLASH,IMAGE,OUTCR,INPUT,NAME,VALUE
C 
      DIMENSION INPUT(80),OUTCR(132),IMAGE(132) 
C 
      COMMON/NAMVAL/NAME(80,401),VALUE(80,401),NUS(401) 
C 
      DATA MAXPM/400/, SLASH/'/'/, IMAGE(132)/' '/ 
C-------------------------------------------------------------- 
      DO 100 I=1,3
        II=I
        CALL GETARG(II,FILNAM)
        OPEN(I,FILE=FILNAM)
  100 CONTINUE
      WRITE(6,6000) 
      NPS=0 
      ALLOK=.TRUE.
      OK=.TRUE. 
C 
C     * LOOP ON ALL SUBSTITUTION SETS.
C     * (FILE IN IS REWOUND AND READ IN FOR EACH SET) 
C 
  110 CONTINUE
      IPARM=1 
      NPCDS=0 
      NPS=MOD(NPS,36) + 1 
      IF(.NOT.OK) GO TO 125 
C 
C     * READ AND DECODE THE NEXT SUBSTITUTION SET FROM IN2. 
C     * A SET MUST END WITH A / CARD OR EOF.
C 
  120 READ( 3,5020,END=121) INPUT 
      OK=.TRUE. 
      GO TO 122 
  121 OK=.FALSE.
  122 IF(.NOT.OK .OR. INPUT(1).EQ.SLASH) GO TO 125
      WRITE(6,6030) INPUT 
      CALL PARDEC(INPUT,NAME,VALUE,MAXPM,IPARM) 
      NPCDS=NPCDS+1 
      GO TO 120 
C 
C     * STOP HERE IF A SUBSTITUTION SET IS EMPTY. 
C 
  125 IF(NPCDS.GT.0) GO TO 130
      IF(NPS.EQ.1)THEN
        WRITE(6,6025) 
        CALL                                       XIT('PARMSUB',-1)
      ENDIF
 
C     * NORMAL PROGRAM TERMINATION.
 
      IF(ALLOK)                                                 STOP 
C
      WRITE(6,6050) 
      CALL                                         XIT('PARMSUB',-2)
C
C     * HARD CODE THE PARAMETER 'S' TO A '$'.
C
  130 NAME(1,IPARM)='S'
      VALUE(1,IPARM)='$'
C 
C     *  READ FILE IN AND DO THE SUBSTITUTION CARD BY CARD. 
C 
      NPARM=IPARM-1 
      WRITE(6,6032) NPARM 
      DO 190 J=1,IPARM
  190 NUS(J)=0
      REWIND 1
      NCARDS=0
  200 CONTINUE
C 
      READ( 1,5020,END=250)(IMAGE(I),I=1,132)
      NCARDS=NCARDS+1
      IF (IMAGE(1).EQ.'C'.OR.IMAGE(1).EQ.'c'.OR.IMAGE(1).EQ.'!') THEN
C       * DO NOT PROCESS COMMENT LINES
        WRITE(2,5020) IMAGE
        WRITE(6,6030) IMAGE
        ALLOK=.TRUE.
      ELSE
        CALL PARINS(OUTCR,IMAGE,NCARDS,NAME,VALUE,NUS,IPARM,NPS,ALLOK)
        WRITE(2,5020) OUTCR 
        WRITE(6,6030) OUTCR 
      ENDIF
      GO TO 200 
C 
C     * STOP IF FILE IN IS EMPTY. 
C 
  250 WRITE(6,6020) NCARDS
      WRITE(6,6035)NPS
      IF(NCARDS.EQ.0) CALL                         XIT('PARMSUB',-3)
C 
C     * LIST ANY PARAMETER NAMES THAT DID NOT GET USED. 
C 
      DO 330 J=1,NPARM
      IF(NUS(J).NE.0) GO TO 330 
      DO 325 I=1,80 
  325 IF(NAME(I,J).EQ.'<') NAME(I,J)=' '
      WRITE(6,6060) (NAME(I,J),I=1,80)
  330 CONTINUE
      WRITE(6,6065) 
      GO TO 110 
C-------------------------------------------------------------------- 
 5020 FORMAT(132A1)
 6000 FORMAT(/,'   CCRN PRODUCT -91/02/12-',/)
 6020 FORMAT('0NUMBER OF INPUT CARD IMAGES =',I6//)
 6025 FORMAT('0...NO SUBSTITUTION SET FOUND')
 6030 FORMAT(' ',132A1)
 6032 FORMAT('0   NUMBER OF PARAMETER NAMES IN THIS SET =',I5//)
 6035 FORMAT('0PROCESSING COMPLETED FOR PARAMETER SET',I6/)
 6050 FORMAT('0...ABORT DUE TO ERROR(S) IN PARAMETER INSERTION')
 6060 FORMAT(' ...NAME NOT USED = ',80A1)
 6065 FORMAT('0==',9('==========')/)
      END 
