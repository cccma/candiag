      PROGRAM GSPAPL
C     PROGRAM  GSPAPL(GSFLD,    GSLNSP,  GSPRES,   GPFLD,     INPUT,            J2
C    1                                                        OUTPUT,   )       J2
C    2          TAPE1=GSFLD, TAPE2=GSLNSP, TAPE3=GPFLD, TAPE5=INPUT,
C    3                                                  TAPE6=OUTPUT)
C     ---------------------------------------------------------------           J2
C                                                                               J2
C     NOV 01/17 - ORIGINAL GSAPL MODIFIED TO READ IN ATMOSPHERIC 
C                   PRESSURE OF INPUT FIELD
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       J2
C     JAN 12/93 - E. CHAN  (DECODE LEVELS IN 8-WORD LABEL)                      
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     FEB 29/88 - R.LAPRISE.                                                    
C                                                                               J2
CGSAPL   - INTERPOLATES FROM SIGMA/HYBRID LEVELS TO NPL PRES. LEVELS    2  1 C  J1
C                                                                               J3
CAUTHOR  - R. LAPRISE                                                           J3
C                                                                               J3
CPURPOSE - INTERPOLATES FROM ETA (SIGMA/HYBRID) LEVELS TO NPL                   J3
C          PRESSURE LEVELS. THE INTERPOLATION IS LINEAR IN LN(SIGMA).           J3
C          EXTRAPOLATION UP AND DOWN IS BY LAPSE RATES SPECIFIED BY             J3
C          THE USER.                                                            J3
C                                                                               J3
CINPUT FILES...                                                                 J3
C                                                                               J3
C      GSFLD  = SETS OF ETA (SIGMA/HYBRID) LEVEL GRID DATA.                     J3
C      GSLNSP = SERIES OF GRIDS OF LN(SF PRES).                                 J3
C                                                                               J3
COUTPUT FILE...                                                                 J3
C                                                                               J3
C      GPFLD  = SETS OF PRESSURE LEVEL GRID DATA.                               J3
C 
CINPUT PARAMETERS...
C                                                                               J5
C      NPL    = NUMBER OF REQUESTED PRESSURE LEVELS, (MAX $L$).                 J5
C      RLUP   = LAPSE RATE USED TO EXTRAPOLATE UPWARDS.                         J5
C      RLDN   = LAPSE RATE USED TO EXTRAPOLATE DOWNWARDS.                       J5
C      ICOORD = 4H SIG/ 4H ETA FOR SIGMA/ETA VERTICAL COORDINATES.              J5
C      PTOIT  = PRESSURE (PA) AT THE LID OF MODEL.                              J5
C      PR     = PRESSURE LEVELS (MB)                                            J5
C                                                                               J5
CEXAMPLE OF INPUT CARDS...                                                      J5
C                                                                               J5
C*GSPAPL.     5        0.        0.  SIG        0.                              J5
C*100  300  500  850 1000                                                       J5
C---------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_MAXLEV,
     &                       SIZES_MAXLEVP1xLONP1xLAT,
     &                       SIZES_PTMIN

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK
  
      INTEGER LEV(SIZES_MAXLEV),LEVP(SIZES_MAXLEV),KBUF(8)
  
      REAL SIG (SIZES_MAXLEV),
     &     FSIG(SIZES_MAXLEV),
     &  DFLNSIG(SIZES_MAXLEV+1),
     &   DLNSIG(SIZES_MAXLEV)
      REAL PR(SIZES_MAXLEV),
     &     PRLOG(SIZES_MAXLEV)
  
      REAL, ALLOCATABLE, DIMENSION(:) :: FA, FP, FPS

      COMMON/ICOM  /IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
  
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/, 
     & MAXL/SIZES_MAXLEVP1xLONP1xLAT/, 
     & MAXLEV/SIZES_MAXLEV/
C---------------------------------------------------------------------
      NFIL=6
      CALL JCLPNT(NFIL,1,2,3,4,5,6)
      DO 110 N=1,4
  110 REWIND N
  
C     * READ THE CONTROL CARDS. 
  
      READ(5,5010,END=908) NPL,RLUP,RLDN,ICOORD,PTOIT                           J4
      IF(ICOORD.EQ.NC4TO8("    ")) ICOORD=NC4TO8(" SIG")
      IF(ICOORD.EQ.NC4TO8(" SIG")) THEN
        PTOIT=MAX(PTOIT,0.00E0) 
      ELSE
        PTOIT=MAX(PTOIT,SIZES_PTMIN)
      ENDIF 
      IF(NPL.GT.MAXLEV) CALL                       XIT('GSPAPL',-1)
      READ(5,5020,END=909) (LEVP(I),I=1,NPL)                                    J4
 
C     * DECODE LEVELS.
  
      CALL LVDCODE(PR,LEVP,NPL) 

      WRITE(6,6010) RLUP,RLDN,ICOORD,PTOIT
      CALL WRITLEV(PR,NPL,' PR ')
  
      DO 114 L=2,NPL
  114 IF(PR(L).LE.PR(L-1)) CALL                    XIT('GSPAPL',-2)
  
      DO 130 L=1,NPL
  130 PRLOG(L)=LOG(PR(L))
  
C     * GET ETA VALUES FROM THE GSFLD FILE. 
  
      CALL FILEV (LEV,NSL,IBUF,1) 
      IF(NSL.LT.1 .OR. NSL.GT.MAXLEV) CALL         XIT('GSPAPL',-3)
      NWDS = IBUF(5)*IBUF(6)
      IF((MAX(NSL,NPL)+1)*NWDS.GT.MAXL) CALL      XIT('GSPAPL',-4)
      DO 116 I=1,8
  116 KBUF(I)=IBUF(I) 
  
      ALLOCATE(FA(SIZES_MAXLEVP1xLONP1xLAT),
     +         FP(SIZES_MAXLEVP1xLONP1xLAT),
     +         FPS(SIZES_MAXLEVP1xLONP1xLAT))

C---------------------------------------------------------------------
C     * GET NEXT SET FROM FILE GSFLD. 
  
      NSETS=0
  150 CALL GETSET2 (1,FA,LEV,NSL,IBUF,MAXX,OK)
      IF(NSETS.EQ.0) WRITE(6,6035) IBUF
      IF(.NOT.OK)THEN
        WRITE(6,6030) NSETS
        IF(NSETS.EQ.0)THEN
          CALL                                     XIT('GSPAPL',-5)
        ELSE
          CALL                                     XIT('GSPAPL',0)
        ENDIF
      ENDIF
      CALL CMPLBL (0,IBUF,0,KBUF,OK)
      IF(.NOT.OK) CALL                             XIT('GSPAPL',-6)
      NAME=IBUF(3)
      NPACK=IBUF(8)

C     * GET LN(SF PRES) FOR THIS STEP

      NST= IBUF(2)
      CALL GETFLD2 (2, FPS ,NC4TO8("GRID"),NST,NC4TO8("LNSP"),1,
     +                                           IBUF,MAXX,OK)
      IF(NSETS.EQ.0) WRITE(6,6035) IBUF
      IF(.NOT.OK) CALL                             XIT('GSPAPL',-7)
      CALL CMPLBL (0,IBUF,0,KBUF,OK)
      IF(.NOT.OK) CALL                             XIT('GSPAPL',-8)

C     * GET SET OF PRESSURE FIELDS (PASCALS) FOR THIS STEP

      DO L=1,NSL
         N=(L-1)*NWDS+1
         CALL GETFLD2(3,FP(N),NC4TO8("GRID"),NST,-1,LEV(L),
     1                IBUF,MAXX,OK)
         IF(NSETS.EQ.0) WRITE(6,6035) IBUF
         IF(.NOT.OK) CALL                          XIT('GSPAPL',-9)
         CALL CMPLBL (0,IBUF,0,KBUF,OK)
         IF(.NOT.OK) CALL                          XIT('GSPAPL',-10)
      ENDDO

C     * INTERPOLATE IN-PLACE FROM ETA TO PRESSURE.

      CALL EPAPL (FA, NWDS,PRLOG,NPL, FA,SIG,NSL ,FPS,FP,
     1            RLUP,RLDN,NSL+1,FSIG,DFLNSIG,DLNSIG)

C     * WRITE THE PRESSURE LEVEL GRIDS ONTO FILE 3.

      IBUF(3)=NAME
      IBUF(8)=NPACK
      CALL PUTSET2 (4,FA,LEVP,NPL,IBUF,MAXX)
      IF(NSETS.EQ.0) WRITE(6,6035) IBUF
      NSETS=NSETS+1
      GO TO 150

C     * E.O.F. ON INPUT.

  908 CALL                                         XIT('GSPAPL',-11)
  909 CALL                                         XIT('GSPAPL',-12)
C---------------------------------------------------------------------
 5010 FORMAT(10X,I5,2E10.0,1X,A4,E10.0)                                         J4
 5020 FORMAT(16I5)                                                              J4
 6010 FORMAT(' RLUP,RLDN = ',2F6.2,', ICOORD=',1X,A4,
     1       ', P.LID (PA)=',E10.3)
 6030 FORMAT('0 GSPAPL INTERPOLATED',I5,' SETS OF ',A4)
 6035 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
      SUBROUTINE EPAPL (FP, LA,PRLOG,NPL, FS,SIG,NSL, PSLOG,PRES,
     1                  RLUP,RLDN,NSL1,FSC,DFDLNS,DLNS)

C     * NOV 01/17 - D.PLUMMER
C     * EAPL ROUTINE MODIFIED SO THAT PRESSURE OF INPUT DATA IS 
C     * PROVIDED INSTEAD OF BEING CALCULATED FROM THE HYBRID COEFFICIENTS
C     * FEB 11/88 - R.LAPRISE.
C     * INTERPOLATES MULTI-LEVEL SET OF GRIDS FROM ETA LEVELS
C     * TO PRESSURE LEVELS FOR HYBRID MODEL.
C     * THE INTERPOLATION IS LINEAR IN LN(PRES).
C
C     * ALL GRIDS HAVE THE SAME HORIZONTAL SIZE (LA POINTS).
C     * FS    =  INPUT GRIDS ON ETA      LEVELS.
C     * PRES  =  PRESSURE (HPA) OF INPUT FIELD
C     * FP    = OUTPUT GRIDS ON PRESSURE LEVELS.
C     *     (NOTE THAT FP AND FS MAY BE EQUIVALENCED IN CALLING PROGRAM)
C     * PSLOG = INPUT  GRID OF LN(SURFACE PRESSURE IN MB).
C
C     * PRLOG(NPL) = VALUES OF INPUT PRESSURE LEVEL (MB).
C     * SIG  (NSL) = VALUES OF ETA LEVELS OF INPUT FIELD.
C     *             (BOTH MUST BE MONOTONIC AND INCREASING).
C     * NSL1  = NSL+1.
C
C     * RLUP  = LAPSE RATE USED TO EXTRAPOLATE ABOVE TOP ETA.
C     * RLDN  = LAPSE RATE USED TO EXTRAPOLATE BELOW BOTTOM ETA.
C     *         UNITS OF RLUP,RLDN ARE  DF/D(LN PRES).
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL FP   (LA,NPL)
      REAL FS (LA,NSL), PRES(LA,NSL)
      REAL PSLOG (LA)
      REAL PRLOG(NPL)

C     * WORK SPACE.

      REAL FSC(NSL),DFDLNS(NSL1),DLNS(NSL),SIG(NSL)
C---------------------------------------------------------------------
C     * LOOP OVER ALL HORIZONTAL POINTS.

      DO 500 I=1,LA

      DO 100 L=1,NSL
         SIG(L)=LOG(PRES(I,L)) - PSLOG(I)
  100 CONTINUE

      DO 180 L=1,NSL-1
         DLNS(L) =1.E0 / (SIG(L+1)-SIG(L))
  180 CONTINUE

C     * GET A COLUMN OF FP (ON PRESSURE LEVELS).

      DO 210 L=1,NSL
         FSC(L)=FS(I,L)
  210 CONTINUE

C     * COMPUTE VERTICAL DERIVATIVE OVER ALL PRESSURE INTERVALS.

      DO 260 L=1,NSL-1
  260 DFDLNS(L+1  ) = (FSC(L+1)-FSC(L)) *DLNS(L)
      DFDLNS(1    ) = RLUP
      DFDLNS(NSL+1) = RLDN

C     * LOOP OVER PRESSURE LEVELS TO BE INTERPOLATED.
C     * X IS THE LN(SIGMA)=LN(PRES)-LN(PS) VALUE OF REQUIRED PRESSURE LEVEL.

      K=1
      DO 350 N=1,NPL
      X=PRLOG(N)-PSLOG(I)

C     * FIND WHICH SIGMA INTERVAL WE ARE IN.

      DO 310 L=K,NSL
      INTVL=L
  310 IF(X.LT.SIG(L)) GO TO 320
      INTVL=NSL+1
  320 K=INTVL-1
      IF(K.EQ.0) K=1

C     * NOW INTERPOLATE AT THIS POINT.

  350 FP(I,N)  = FSC(K)+DFDLNS(INTVL)*(X-SIG(K))

  500 CONTINUE

      RETURN
      END
