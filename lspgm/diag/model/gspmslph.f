      PROGRAM GSPMSLPH
C     PROGRAM GSPMSLPH (GSTEMP,   GLNSP,   GPHIS,    GSPRES,   GPMSL,           J2
C    1                                           INPUT,       OUTPUT,   )       J2
C    2          TAPE11=GSTEMP, TAPE12=GLNSP, TAPE13=GPHIS, TAPE14=GSPRES,
C    3          TAPE15=GPMSL,                 TAPE5=INPUT, TAPE6=OUTPUT)
C     ---------------------------------------------------------------           J2
C                                                                               J2
C     NOV 07/17 - D.PLUMMER - MODIFIED GSMSLPH TO READ PRESSURE FROM FILE       J2
C     MAR 04/10 - S.KHARIN,F.MAJAESS (CHANGE "RGAS" FROM 287. TO 287.04,        J2
C                                     AND    "GRAV" FROM 9.8  TO 9.80616)       J2
C     NOV 17/08 - S.KHARIN (USE PACKING DENSITY OF GSTEMP FOR GPMSL)
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     JAN 12/93 - E. CHAN  (DECODE LEVELS IN 8-WORD LABEL)
C     JAN 29/92 - E. CHAN     (CONVERT HOLLERITH LITERALS TO ASCII)
C     JAN 19/89 - M.LAZARE. - BASED ON GSMSLP EXCEPT FOR HYBRID MODEL.
C     MAY 06/81 - R.LAPRISE.- ORIGINAL VERSION GSMSLP.
C                                                                               J2
CGSPMSLPH - COMPUTES MEAN-SEA-LEVEL PRESSURE FROM SIGMA T, LNSP, PHIS    3  1 C J1
C                                                                               J3
CAUTHOR  - M.LAZARE                                                             J3
C                                                                               J3
CPURPOSE - FROM A SERIES OF SIGMA LEVEL TEMPERATURES (=GSTEMP) AND              J3
C          LN(PS) (=GLNSP), COMPUTE  A  SERIES  OF  MEAN-SEA-LEVEL              J3
C          PRESSURES (=GPMSL), USING THE MOUNTAIN FIELD PHIS (=GPHIS).          J3
C                                                                               J3
CINPUT FILES...                                                                 J3
C                                                                               J3
C      GSTEMP = SIGMA LEVEL GRID SETS OF TEMPERATURE (DEG K).                   J3
C      GSLNSP = CORRESPONDING GRIDS OF LN(SF.PRES.)                             J3
C      GPHIS  = GRID OF SURFACE GEOPOTENTIAL.                                   J3
C                                                                               J3
COUTPUT FILE...                                                                 J3
C                                                                               J3
C      GPMSL  = MEAN-SEA-LEVEL PRESSURE GRIDS.                                  J3
C
CINPUT PARAMETERS...
C                                                                               J5
C      GAM    = LOW LEVEL LAPSE RATE                                            J5
C      NLVLUP = NUMBER OF LEVELS BETWEEN REFERENCE AND LOWEST LEVEL TEMP        J5
C      LAY    = LAYERING SCHEME USED IN MODEL.                                  J5
C      ICOORD = VERTICAL COORDINATE (ETA OR SIG).                               J5
C      PLID   = MODEL "LID" IN PASCALS.                                         J5
C                                                                               J5
CEXAMPLE OF INPUT CARD...                                                       J5
C                                                                               J5
C* GSPMSLPH     .0065    1    3  ETA      500.                                  J5
C-------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_MAXLEV,
     &                       SIZES_PTMIN

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK
      INTEGER LEV(SIZES_MAXLEV)
C
      COMMON/ICOM/ IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
C
      REAL, ALLOCATABLE, DIMENSION(:) :: TEMP, RLNSP, PHIS, PMSL, PRES
C
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/, MAXL/SIZES_MAXLEV/
      DATA RGAS/287.04/, GRAV/9.80616/
C-----------------------------------------------------------------------
      NFF=7
      CALL JCLPNT(NFF,11,12,13,14,15,5,6)
      REWIND 11
      REWIND 12
      REWIND 13
      REWIND 14
C
      RGOCP=2.E0/7.E0
      READ(5,5010,END=901)GAM,NLVLUP,LAY,ICOORD,PLID                            J4
      IF(ICOORD.EQ.NC4TO8(" SIG")) THEN
        PLID=MAX(PLID,0.00E0)
      ELSE
        PLID=MAX(PLID,SIZES_PTMIN)
      ENDIF
      WRITE(6,6000)GAM,NLVLUP
      WRITE(6,6010)LAY,ICOORD,PLID
C
C     * USE THE TEMP AT THE (NLVLUP+1) LEVEL ABOVE SURFACE TO EXTRAPOL.
C
      CALL FILEV(LEV,NLEV,IBUF,11)
      IF((NLEV.LT.1).OR.(NLEV.GT.MAXL)) CALL       XIT('GSPMSLPH',-1)
      NLEV=NLEV-NLVLUP
      IF(NLEV.LE.0) CALL                           XIT('GSPMSLPH',-99)
      LOWEST=LEV(NLEV)
      NPACK=IBUF(8)
      REWIND 11
C
      NPTS=IBUF(5)*IBUF(6)
C
      ALLOCATE(TEMP(SIZES_LONP1xLAT), RLNSP(SIZES_LONP1xLAT),
     +         PHIS(SIZES_LONP1xLAT), PMSL(SIZES_LONP1xLAT),
     +         PRES(SIZES_LONP1xLAT))
C
C     * GET THE MOUNTAIN FIELD.
C
      CALL GETFLD2(13,PHIS,NC4TO8("GRID"),-1,NC4TO8("PHIS"),-1,
     +                                           IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('GSPMSLPH',-2)
      IF(IBUF(5)*IBUF(6).NE.NPTS) CALL             XIT('GSPMSLPH',-3)
      WRITE(6,6025)IBUF
C
C     * GET REFERENCE LEVEL OF TEMPERATURE.
C
      NRECS=0
  100 CALL GETFLD2(11,TEMP,NC4TO8("GRID"),-1,NC4TO8("TEMP"),LOWEST,
     +                                            IBUF,MAXX,OK)
      IF(.NOT.OK.AND.NRECS.EQ.0) CALL              XIT('GSPMSLPH',-4)
      IF(IBUF(5)*IBUF(6).NE.NPTS) CALL             XIT('GSPMSLPH',-5)
      IF(.NOT.OK)THEN
        WRITE(6,6030)NRECS
        CALL                                       XIT('GSPMSLPH',0)
      ENDIF
      IF(NRECS.EQ.0)WRITE(6,6025)IBUF
      NT=IBUF(2)
C
C     * GET LN(PS) FOR CORRESPONDING TIMESTEP.
C
      CALL GETFLD2(12,RLNSP,NC4TO8("GRID"),NT,NC4TO8("LNSP"),-1,
     +                                            IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('GSPMSLPH',-6)
      IF(IBUF(5)*IBUF(6).NE.NPTS) CALL             XIT('GSPMSLPH',-7)
      IF(NRECS.EQ.0) WRITE(6,6025)IBUF
C
C     * GET ATMOSPHERIC PRESSURE (HPA) IN LOWEST MODEL LAYER
C
      CALL GETFLD2(14,PRES,NC4TO8("GRID"),NT,-1,LOWEST,
     +                                            IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('GSPMSLPH',-8)
      IF(IBUF(5)*IBUF(6).NE.NPTS) CALL             XIT('GSPMSLPH',-9)
      IF(NRECS.EQ.0) WRITE(6,6025)IBUF
C
C     * COMPUTE M.S.L. PRESSURE.
C     * ASSUME A UNIFORM LAPSE RATE OF (DT/DZ)=-GAM, FROM REFERENCE
C     * LEVEL TO THE SURFACE, AND THEN FROM THE SURFACE TO SEA LEVEL.
C     * USE HYDROSTATIC EQUATION AND CONSTANT-LAPSE RATE EQUATION FIRST
C     * TO DETERMINE "SURFACE TEMPERATURE".
C
      DO 200 I=1,NPTS
        GAMRGOG = GAM*RGAS/GRAV
        PS      = EXP(RLNSP(I))
        PL      = PRES(I)
        TS      = TEMP(I) * (PS/PL)**GAMRGOG
        TBAR    = TS + 0.5E0*GAM*PHIS(I)/GRAV
        PMSL(I) = EXP(RLNSP(I)+PHIS(I)/(RGAS*TBAR))
  200 CONTINUE
C
C     * SAVE M.S.L. PRESSURE.
C
      IBUF(3)=NC4TO8("PMSL")
      IBUF(4)=1
      IBUF(8)=NPACK
      CALL PUTFLD2(15,PMSL,IBUF,MAXX)
      IF(NRECS.EQ.0) WRITE(6,6025)IBUF
      NRECS=NRECS+1
      GO TO 100
C
C     * E.O.F. ON INPUT.
C
  901 CALL                                       XIT('GSPMSLPH',-10)
C-----------------------------------------------------------------------
 5010 FORMAT(10X,F10.0,2I5,1X,A4,E10.0)                                         J4
 6000 FORMAT('0GAM=',E10.3,' TEMP. LEVEL USED IS ILEV -',I2)
 6010 FORMAT('0LAY,ICOORD,PLID  = ',I5,1X,A4,E10.3)
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
 6030 FORMAT('0',I5,' FIELDS SAVED')
      END
