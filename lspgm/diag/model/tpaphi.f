      PROGRAM TPAPHI
C     PROGRAM TPAPHI(GSTEMP,       GSPHIS,       GSLNSP,       PRES_T,
C    1               PRES_M,      GSRGASM,        GSPHI,
C    2               INPUT,       OUTPUT, )                                     J2
C    3        TAPE11=GSTEMP,TAPE12=GSPHIS,TAPE13=GSLNSP,TAPE14=PRES_T
C    4        TAPE15=PRES_M TAPE16=GSRGASM,TAPE17=GSPHI,
C    5        TAPE5= INPUT,TAPE6 = OUTPUT)
C     -----------------------------------------------------------------         J2
C                                                                               J2
C     NOV 02/17 - D. PLUMMER - ORIGINAL TAPHI MODIFED TO READ PRESSURE
C                               FROM AN INPUT FILE
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       J2
C     JUL 27/94 - J. KOSHYK
C                                                                               J2
CTAPHI   - CALCULATES PHI ON ETA (SIGMA/HYBRID) LEVELS FROM TEMPERATURE         J1
C          AND MOIST GAS CONSTANT ON ETA LEVELS.                        4  1 C  J1
C                                                                               J3
CAUTHOR  - J. KOSHYK                                                            J3
C                                                                               J3
CPURPOSE - CALCULATES PHI ON ETA (SIGMA/HYBRID) LEVELS FROM TEMPERATURE         J3
C          AND MOIST GAS CONSTANT ON ETA LEVELS.                                J3
CINPUT FILES...                                                                 J3
C                                                                               J3
C      GSTEMP  = SETS OF TEMPERATURE ON ETA LEVELS.                             J3
C      GSPHIS  = MOUNTAINS (SURFACE GEOPOTENTIAL).                              J3
C      GSLNSP  = SETS OF LN(SURFACE PRESSURE).                                  J3
C      GSRGASM = SETS OF MOIST GAS CONSTANT ON ETA LEVELS.                      J3
C                                                                               J3
COUTPUT FILES...                                                                J3
C                                                                               J3
C      GSPHI   = ETA LEVEL GEOPOTENTIALS (ON SAME LEVELS AS GSTEMP).            J3
C
CINPUT PARAMETERS...
C                                                                               J5
C      ICOORD = 4H SIG/4H ETA/4HET10/4HET15  FOR VERTICAL COORDINATE.           J5
C      LAY    = LAYERING SCHEME FOR CALCULATING LAYER INTERFACES.               J5
C      PTOIT  = PRESSURE (PA) OF LID AT TOP OF MODEL.                           J5
C                                                                               J5
CEXAMPLE OF INPUT CARDS...                                                      J5
C                                                                               J5
C*TAPHI.    ET15    3        0.                                                 J5
C-------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_MAXLEV,
     &                       SIZES_MAXLEVP1xLONP1xLAT,
     &                       SIZES_PTMIN

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK

      INTEGER LH(SIZES_MAXLEV+1), LM(SIZES_MAXLEV+1)

      REAL, ALLOCATABLE, DIMENSION(:) :: SFM, SHM, SFT, SHT,
     &                                   DLNSIGFT
      REAL, ALLOCATABLE, DIMENSION(:) :: RGASM, PHI, TEMP, PRT, PRM

      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
      COMMON/JCOM/JBUF(8),JDAT(SIZES_LONP1xLATxNWORDIO)

      DATA MAXX /SIZES_LONP1xLATxNWORDIO/,
     & MAXL /SIZES_MAXLEVP1xLONP1xLAT/, MAXLEV /SIZES_MAXLEV/
C---------------------------------------------------------------------
      NFIL=9
      CALL JCLPNT(NFIL,11,12,13,14,15,16,17,5,6)
      DO 110 N=11,17
  110 REWIND N

C     * READ-IN DIRECTIVE CARDS.

      READ(5,5010,END=912) ICOORD,LAY,PTOIT                                     J4
      IF(ICOORD.EQ.NC4TO8("    ")) ICOORD=NC4TO8(" SIG")
      IF(ICOORD.EQ.NC4TO8(" SIG")) THEN
        PTOIT=MAX(PTOIT,0.00E0)
      ELSE
        PTOIT=MAX(PTOIT, SIZES_PTMIN)
      ENDIF
      WRITE(6,6010) ICOORD,LAY,PTOIT

C     * GET LAYER LABELS FOR MOMENTUM LAYERS  - ASSUMED HERE TO BE
C     * THE UPPER FACE OF EACH THERMODYNAMIC LAYER

      CALL FILEV(LM,NSM,IBUF,15)
      IF(NSM.LT.1 .OR. NSM.GT.MAXLEV) CALL         XIT('TAPHIP',-1)

      ALLOCATE(SFM(SIZES_MAXLEV+1), SHM(SIZES_MAXLEV),
     &         SFT(SIZES_MAXLEV+1), SHT(SIZES_MAXLEV),
     &         DLNSIGFT(SIZES_MAXLEV))

      CALL LVDCODE(SFT(2),LM,NSM)
      DO 130 L=2,NSM+1
         SFT(L) = SFT(L)*0.001E0
  130 CONTINUE
      SFT(1)=PTOIT/101320.E0
      CALL WRITLEV(SFT,NSM+1,'ETAF')

C     * GET LAYERS FOR THERMODYNAMIC LEVELS FROM TEMPERATURE FILE.

      CALL FILEV (LH,NSL,IBUF,11)
      IF(NSL.LT.1 .OR. NSL.GT.MAXLEV) CALL         XIT('TAPHIP',-2)
      CALL LVDCODE(SHT,LH,NSL)
      DO 140 L=1,NSL
         SHT(L) = SHT(L)*0.001E0
         SHM(L) = SHT(L)
  140 CONTINUE
      CALL WRITLEV(SHT,NSL,' ETA')

      IF(NSL .NE. NSM) CALL                        XIT('TAPHIP',-3)

C     * READ MOUNTAINS AND KEEP IN JCOM.

      CALL RECGET (12,NC4TO8("GRID"),-1,NC4TO8("PHIS"),1,JBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('TAPHIP',-4)
      WRITE(6,6025) JBUF
      CALL CMPLBL (0,IBUF, 0,JBUF, OK)
      IF(.NOT.OK) THEN
         WRITE(6,6025) IBUF,JBUF
         CALL                                      XIT('TAPHIP',-5)
      ENDIF

C     * STOP IF THERE IS NOT ENOUGH SPACE.

      NWDS = JBUF(5)*JBUF(6)
      IF((NSL+1)*NWDS.GT.MAXL) CALL                XIT('TAPHIP',-6)

      ALLOCATE(PHI(SIZES_MAXLEVP1xLONP1xLAT),
     &         TEMP(SIZES_MAXLEVP1xLONP1xLAT),
     &         RGASM(SIZES_MAXLEVP1xLONP1xLAT),
     &         PRT(SIZES_MAXLEVP1xLONP1xLAT),
     &         PRM(SIZES_MAXLEVP1xLONP1xLAT))
C---------------------------------------------------------------------
C     * GET NEXT SET OF TEMPERATURES INTO ARRAY T.

      NSETS=0
  150 CALL GETSET2 (11,TEMP,LH,ISL,IBUF,MAXX,OK)
      IF(NSETS.EQ.0) WRITE(6,6025) IBUF
      IF(.NOT.OK)THEN
        WRITE(6,6030) NSETS
        CALL                                       XIT('TAPHIP ',0)
      ENDIF
      IF(IBUF(3).NE.NC4TO8("TEMP")) CALL           XIT('TAPHIP',-7)
      IF(ISL.NE.NSL) CALL                          XIT('TAPHIP',-8)
      CALL CMPLBL(0,IBUF,0,JBUF,OK)
      IF(.NOT.OK)THEN
        WRITE(6,6025) IBUF,JBUF
        CALL                                       XIT('TAPHIP',-9)
      ENDIF
      NPACK=IBUF(8)

C     * TRANSFER MOUNTAINS FROM JBUF TO LEVEL NSL+1 OF PHI.

      IPHIS=NSL*NWDS+1
      CALL RECUP2 (PHI(IPHIS),JBUF)

C     * READ LN(SF PRES) FOR THIS STEP AND PUT IT AS THE LAST LEVEL
C     * OF THE THERMODYNAMIC FULL LEVELS (LAYER INTERFACES).

      ILNSP=IPHIS
      NST = IBUF(2)
      CALL GETFLD2 (13,PRM(ILNSP),NC4TO8("GRID"),NST,NC4TO8("LNSP"),
     +                                             1,IBUF,MAXX,OK)
      IF(NSETS.EQ.0) WRITE(6,6025) IBUF
      IF(.NOT.OK) CALL                             XIT('TAPHIP',-10)
      CALL CMPLBL(0,IBUF,0,JBUF,OK)
      IF(.NOT.OK) CALL                             XIT('TAPHIP',-11)

C     * CONVERT LN(SF PRES) TO PRESSURE IN HECTOPASCALS

      DO I=1,NWDS
        PRM(ILNSP+I-1) = EXP(PRM(ILNSP+I-1))
      ENDDO

C     * GET NEXT SET THERMODYNAMIC MID-LAYER PRESSURE.

      CALL GETSET2 (14,PRT,LH,ISL,IBUF,MAXX,OK)
      IF(NSETS.EQ.0) WRITE(6,6025) IBUF
      IF(.NOT.OK) CALL                             XIT('TAPHIP',-12)
      IF(ISL.NE.NSL) CALL                          XIT('TAPHIP',-13)
      IF(IBUF(2).NE.NST) CALL                      XIT('TAPHIP',-14)
      CALL CMPLBL(0,IBUF,0,JBUF,OK)
      IF(.NOT.OK)THEN
        WRITE(6,6025) IBUF,JBUF
        CALL                                       XIT('TAPHIP',-15)
      ENDIF


C     * GET NEXT SET OF UPPER FACE (MOMENTUM) PRESSURES

      CALL GETSET2 (15,PRM,LM,ISL,IBUF,MAXX,OK)
      IF(NSETS.EQ.0) WRITE(6,6025) IBUF
      IF(.NOT.OK) CALL                             XIT('TAPHIP',-16)
      IF(ISL.NE.NSL) CALL                          XIT('TAPHIP',-17)
      IF(IBUF(2).NE.NST) CALL                      XIT('TAPHIP',-18)
      CALL CMPLBL(0,IBUF,0,JBUF,OK)
      IF(.NOT.OK)THEN
        WRITE(6,6025) IBUF,JBUF
        CALL                                       XIT('TAPHIP',-19)
      ENDIF

C     * GET NEXT SET OF MOIST GAS CONSTANT INTO ARRAY RGASM.

      CALL GETSET2 (16,RGASM,LH,ISL,IBUF,MAXX,OK)
      IF(NSETS.EQ.0) WRITE(6,6025) IBUF
      IF(.NOT.OK) CALL                             XIT('TAPHIP',-20)
      IF(IBUF(3).NE.NC4TO8("RGAS")) CALL           XIT('TAPHIP',-21)
      IF(ISL.NE.NSL) CALL                          XIT('TAPHI ',-22)
      CALL CMPLBL(0,IBUF,0,JBUF,OK)
      IF(.NOT.OK)THEN
        WRITE(6,6025) IBUF,JBUF
        CALL                                       XIT('TAPHI ',-23)
      ENDIF

C     * COMPUTE PHI ON MODEL HALF LEVELS USING HYDROSTATIC EQUATION.

      CALL PHIETAP (PHI, TEMP, PRT, PRM, RGASM,
     1              NWDS,NSL,NSL+1, NSETS, DLNSIGFT)

C     * WRITE-OUT PHI ON ETA LEVELS.

      IBUF(3)=NC4TO8(" PHI")
      IBUF(8)=NPACK
      CALL PUTSET2 (17,PHI,LH,NSL,IBUF,MAXX)
      IF(NSETS.EQ.0) WRITE(6,6025) IBUF
      NSETS=NSETS+1
      GO TO 150

C     * E.O.F. ON INPUT.

  912 CALL                                         XIT('TAPHI ',-24)
  913 CALL                                         XIT('TAPHI ',-25)
C---------------------------------------------------------------------------------
 5010 FORMAT(10X,1X,A4,I5,E10.0)                                                J4
 6010 FORMAT('0 ICOORD=',1X,A4,', LAY=',I5,', P.LID (PA)=',E10.3)
 6025 FORMAT(' ',A4,I10,2X,A4,1X,I10,4I6)
 6030 FORMAT('0',I5,' SETS INTERPOLATED')
      END
      SUBROUTINE PHIETAP(PHI, TEMP, PRT, PRM, RGASM,
     1                   LEN,NSL,NSL1, NSETS, DLNSIGFT)

C     * JUL 27/94 - J. KOSHYK

C     * COMPUTE PHI FROM T ON MODEL THERMOD. HALF LEVELS.
C     * PHI AND T MAY BE EQUIVALENCED IN THE CALLING PROGRAM.
C     * NOV 06/17 - D. PLUMMER
C     * IN PLACE OF CALCULATING THE PRESSURE FROM THE HYBRID
C     * COEFFICIENTS, THE PRESSURE AT THE MID-POINT AND UPPER FACE FOR
C     * THE THERMODYNAMICS LEVELS IS PASSED IN
C     * THE FULL THERMODYNAMIC LEVEL (INTERFACE) PRESSURE HAS AN EXTRA
C     * LAYER THAT CARRIES THE SURFACE PRESSURE
C     * PRESSURE AT MODEL THERMOD. FULL LEVELS.
C     * PRESSURE AT MODEL THERMOD. HALF LEVELS.
C     * PHIS, THE SURFACE GEOPOTENTIAL, MUST BE IN PHI( ,NSL+1)
C     * PSMBLN IS LN(PS), FOR PS IN MB.
C     * NSLP = NSL + 1.

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL PHI(LEN,NSL1)
      REAL TEMP(LEN,NSL), PRT(LEN,NSL), RGASM(LEN,NSL)
      REAL PRM(LEN,NSL1)

      REAL DLNSIGFT(NSL)

C---------------------------------------------------------------------------------

      DO 500 N=1,LEN

         DO 200 L=1,NSL
            DLNSIGFT(L)=LOG(PRM(N,L+1)/PRM(N,L))
  200    CONTINUE

C        * COMPUTE PHI ON MODEL THERMOD. FULL LEVELS (LAYER INTERFACES).

         DO 300 L=NSL,1,-1
            PHI(N,L)=PHI(N,L+1) + RGASM(N,L)*TEMP(N,L)*DLNSIGFT(L)
  300    CONTINUE

C        * COMPUTE PHI ON MODEL THERMOD. HALF LEVELS.

         DO 400 L=1,NSL
            PHI(N,L)=PHI(N,L+1) +
     1                RGASM(N,L)*TEMP(N,L)*LOG(PRM(N,L+1)/PRT(N,L))
  400    CONTINUE

  500 CONTINUE

      RETURN
      END
