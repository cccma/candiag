      PROGRAM SP2SID
C     PROGRAM SP2SID (SPIN,       SPOUT,       OUTPUT,                  )       E2
C    1          TAPE1=SPIN, TAPE2=SPOUT, TAPE6=OUTPUT)
C     ------------------------------------------------                          E2
C                                                                               E2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       E2
C     AUG 05/94 - F.MAJAESS (REVISE FOR TRUNCATIONS > 99)                       
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY   /86 - G.J.BOER                                                      
C                                                                               E2
CSP2SID  - COMPUTES THE TWO-SIDED SPECTRAL REPRESENTATION               1  1    E1
C                                                                               E3
CAUTHOR  - G.J.BOER                                                             E3
C                                                                               E3
CPURPOSE - COMPUTES THE TWO-SIDED SPECTRAL REPRESENTATION GIVEN THE ONE-SIDED   E3
C          SPECTRAL REPRESENTATION. THUS GIVEN A SPECTRAL ARRAY OF COEFFICIENTS E3
C          REPRESENTING SOME ONE-SIDED (REAL) SPECTRAL ARRAY SUCH AS SPECTRAL   E3
C          DENSITY OR THE SPECTRAL INTERACTION TERM, THE PROGRAM DIVIDES THE    E3
C          NON-ZONAL, IE M.NE.0, COMPONENTS BY 2.                               E3
C                                                                               E3
CINPUT FILE...                                                                  E3
C                                                                               E3
C      SPIN  = ARRAYS WITH ONE-SIDED SPECTRAL REPRESENTATION                    E3
C                                                                               E3
COUTPUT FILE...                                                                 E3
C                                                                               E3
C      SPOUT = TWO-SIDED SPECTRAL REPRESENTATION OF SPIN                        E3
C---------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_LA,
     &                       SIZES_LMTP1,
     &                       SIZES_NWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      integer, parameter :: 
     & MAXX = 2*(SIZES_LA+SIZES_LMTP1)*SIZES_NWORDIO 

      COMMON/BLANCK/F(2*(SIZES_LA+SIZES_LMTP1)) 
C 
      LOGICAL OK
      COMMON/ICOM/IBUF(8),IDAT(MAXX)
C-----------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,2,6)
      REWIND 1
      REWIND 2
      NRECS=0 
C 
C     * READ THE NEXT FIELD 
C 
  100 CALL GETFLD2(1,F,NC4TO8("SPEC"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NRECS.EQ.0) CALL                        XIT('SP2SID',-1) 
        WRITE(6,6020) NRECS 
        CALL                                       XIT('SP2SID',0)
      ENDIF 
      IF(NRECS.EQ.0) WRITE(6,6030) IBUF 
C 
C     * MULT THE M.EQ.0 VALUES BY 2 THEN DIVIDE EVERYTHING BY 2 
C 
      NWDS=2*IBUF(5)*IBUF(6)
      IF(IBUF(7).LE.99999) THEN
       LR=2*(IBUF(7)/1000) 
      ELSE
       LR=2*(IBUF(7)/10000) 
      ENDIF
C 
      DO 200 I=1,LR 
  200 F(I)=F(I)*2.0E0 
C 
      DO 210 I=1,NWDS 
  210 F(I)=F(I)*0.5E0 
C 
      CALL PUTFLD2(2,F,IBUF,MAXX)
      IF(NRECS.EQ.0) WRITE(6,6030) IBUF 
      NRECS=NRECS+1 
      GO TO 100 
C-----------------------------------------------------------------------
6020  FORMAT('0SP2SID READ',I6,' RECORDS')
6030  FORMAT(1X,A4,I10,2X,A4,I10,4I6)
      END
