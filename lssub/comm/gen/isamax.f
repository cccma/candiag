      INTEGER FUNCTION ISAMAX(N,SX,INCX)
C
C     * FINDS THE INDEX OF ELEMENT HAVING MAXIMUM ABSOLUTE VALUE.
C
C     * JACK DONGARRA, LINPACK, 3/11/78. - ISAMAX.
C     * CLEANUP AND VECTORIZED BY: M.LAZARE, SEPT. 24/91.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL SX(N)
C
      ISAMAX = 0
      IF(N .LT. 1) RETURN
      ISAMAX = 1
      IF(N .EQ. 1) RETURN
C
      IF(INCX.NE.1)                             THEN
C
C        * CODE FOR INCREMENT NOT EQUAL TO 1.
C
         IX = 1
         SMAX = ABS(SX(1))
         DO 10 I = 2,N
            IX = IX + INCX
            IF(IX.LE.N .AND. ABS(SX(IX)).GT.SMAX)    THEN
               ISAMAX = IX
               SMAX = ABS(SX(IX))
            ENDIF
   10    CONTINUE
      ELSE
C
C        * CODE FOR INCREMENT EQUAL TO 1.
C
         SMAX = ABS(SX(1))
         DO 30 I = 2,N
            IF(ABS(SX(I)).GT.SMAX)                   THEN
               ISAMAX = I
               SMAX = ABS(SX(I))
            ENDIF
   30    CONTINUE
      ENDIF
C
      RETURN
      END
