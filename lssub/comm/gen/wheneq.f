      SUBROUTINE WHENEQ(N,ARRAY,INC,TARGET,INDEX,NVAL)
C
C     * M LAZARE. SEPT 25/91.
C     * FORTRAN EQUIVILENT OF INTRINSIC CRAY SUBROUTINE.
C     * SEARCHES ARRAY "ARRAY" FOR VALUE "TARGET" AND
C     * STORES ALL INDICES OF "ARRAY" CONTAINING THIS
C     * TARGET VALUE IN THE ARRAY "INDEX". 
C
C     * DESCRIPTION OF PARAMETERS:
C
C     * N       NUMBER OF ELEMENTS TO BE SEARCHED.
C     * ARRAY   FIRST ELEMENT OF THE REAL ARRAY TO BE SEARCHED.
C     * INC     SKIP DISTANCE BETWEEN ELEMENTS OF THE SEARCHED ARRAY.
C     * TARGET  REAL VALUE SEARCHED FOR IN THE ARRAY.
C     * INDEX   INTEGER ARRAY CONTAINING THE INDEX OF THE FOUND TARGET
C     *         IN THE ARRAY.
C     * NVAL    NUMBER OF VALUES PUT IN THE INDEX ARRAY.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL ARRAY(N)
      INTEGER INDEX(N)
C ----------------------------------------------------------------------
      IF(INC.LE.0)                                CALL ABORT
      NVAL=0
      DO 100 I=1,N,INC
        IF(ARRAY(I).EQ.TARGET)                    THEN
           NVAL=NVAL+1
           INDEX(NVAL)=I
        ENDIF
 100  CONTINUE
      RETURN
      END
