      SUBROUTINE PARDEC(INPUT,NAME,VALUE,MAXPM,IPARM) 
C
C     * DEC 20/91 - M.LAZARE (RE-MODIFIED BACK TO 80 CHAR/LINE). 
C     * FEB 19/91 - F.MAJAESS (MODIFIED TO HANDLE 82 CHAR/LINE). 
C     * JAN 20/83 - R.LAPRISE,B.DUGAS,J.D.HENDERSON.
C 
C     * PARAMETER DECODER SUBROUTINE FOR PROGRAM PARMSUB. 
C 
C     * INPUT = 80 CHARACTER CARD IMAGE FROM THE SUBSTITUTION SET 
C     *         HAVING THE FORM  NAM1=VAL1, NAM2=VAL2, ETC. 
C     * NAME = OUTPUT LIST OF PARAMETER NAMES (MAX 80 CHARACTERS).
C     * VALUE = OUTPUT LIST OF SUBSTITUTION VALUES FOR THE
C     *         PARAMETERS IN ARRAY NAME (MAX 80 CHARACTERS). 
C     * MAXPM = MAXIMUM NUMBER OF PARAMETERS ALLOWED. 
C     * IPARM = THE NUMBER OF THE NEXT PARAMETER. 
C 
      IMPLICIT INTEGER (A-Z)
      LOGICAL INANAM
C 
C     * LEVEL 2,NAME,VALUE
      CHARACTER*1 INPUT,NAME,VALUE,SEP,ICH
      DIMENSION INPUT(80),NAME(80,MAXPM),VALUE(80,MAXPM)
C-------------------------------------------------------------------- 
C     * FILL NEXT NAME AND VALUE WITH A DUMMY CHARACTER.
C     * THE CHARACTER USED HERE ACTS AS AN END DELIMITER. 
C 
      DO 110 K=1,80 
      NAME (K,IPARM)='<'
  110 VALUE(K,IPARM)='<'
C 
C     * IF COLUMNS 1-3 CONTAIN (,=X) THEN THE SEPARATOR IS CHANGED
C     * FROM A COMMA TO THE CHARACTER (X) FOR THIS CARD ONLY. 
C     * IN THIS CASE SCANNING STARTS IN COLUMN 5. 
C 
      SEP=',' 
      LCOL=1
      IF(INPUT(1).NE.SEP) GO TO 210 
      IF(INPUT(2).NE.'=') GO TO 210 
      IF(INPUT(3).NE.' ') SEP=INPUT(3)
      LCOL=5
C 
C     * SCAN THE INPUT CARD ONE COLUMN AT A TIME. 
C     * BLANKS ARE SIGNIFICANT INSIDE A SUBSTITUTION VALUE ONLY.
C     * INANAM=.TRUE. INSIDE A NAME, .FALSE. INSIDE A VALUE.
C 
  210 INANAM=.TRUE. 
      CHAR=1
      DO 290 I=LCOL,80
      ICH=INPUT(I)
      IF((INANAM).AND.(ICH.EQ.' ')) GO TO 290 
C 
C     * A SEPARATION CHARACTER IS ALLOWED INSIDE A NAME 
C     *  BUT IT TERMINATES A VALUE. 
C     * IN THIS CASE, FILL THE NEXT NAME AND VALUE. 
C     * STOP IF MORE THAN MAXPM PARAMETER NAMES ARE ENCOUNTERED.
C 
      IF(ICH.NE.SEP) GO TO 220
      IF(INANAM) GO TO 220
      INANAM=.TRUE. 
      CHAR=1
      IPARM=IPARM+1 
      DO 215 K=1,80 
      NAME (K,IPARM)='<'
  215 VALUE(K,IPARM)='<'
      IF(IPARM.LE.MAXPM) GO TO 290
      WRITE(6,6010) MAXPM 
      CALL XIT('PARDEC',-1) 
C 
C     * AN EQUAL SIGN (=) IS ALLOWED INSIDE A VALUE 
C     *  BUT IT TERMINATES A NAME.
C 
  220 IF(ICH.NE.'=')  GO TO 230 
      IF(.NOT.INANAM) GO TO 230 
      INANAM=.FALSE.
      CHAR=1
      GO TO 290 
C 
C     * INSERT THE CURRENT CHARACTER INTO NAME OR VALUE.
C 
  230 IF(     INANAM)  NAME(CHAR,IPARM)=ICH 
      IF(.NOT.INANAM) VALUE(CHAR,IPARM)=ICH 
      CHAR=CHAR+1 
  290 CONTINUE
C 
C     * WRITE A WARNING IF THE LAST VALUE ON THE CARD IS MISSING. 
C 
      IF(.NOT.INANAM) WRITE(6,6020) 
      RETURN
C-------------------------------------------------------------------- 
 6010 FORMAT('0...NUMBER OF PARAMETERS EXCEEDS',I6)
 6020 FORMAT('0...WARNING - LAST VALUE ON THIS CARD IS MISSING'//) 
      END 
