      SUBROUTINE BETAWS(NLONO,NLATO,BETAO,BETAUV)
C
C     * APR 22/02 - B. MIVILLE
C      
C     * CALCULATE THE OCEAN SURFACE BETA FOR WIND STRESS
C
C     * INPUT PARAMETERS...
C
C     * NLON  = NUMBER OF LONGITUDES
C     * NLAT  = NUMBER OF LATITUDES
C     * BETAO = SURFACE OCEAN BETA AT TRACER LOCATION
C
C     * OUTPUT PARAMETERS...
C
C     * BETAUV = OCEAN SURFACE BETA FOR WIND STRESS (AT U,V LOCATION)
C
C----------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER NLONO, NLATO
      REAL    BETAO(NLONO,NLATO), BETAUV(NLONO,NLATO)
C-----------------------------------------------------------------------
C
C     * THE WIND STRESS LOCATION IS AT THE CORNER OF THE U,V POINTS
C     * WIND WANT TO MAKE BETA EQUAL TO ONE WHENEVER THE WIND STRESS IS IN
C     * CONTACT WITH LAND.
C
      DO 112 J=1,NLATO
         DO 110 I=1,NLONO
            IF(I.LT.NLONO)THEN
               IF(((BETAO(I,J).EQ.1.E0).AND.(BETAO(I+1,J).EQ.0.E0)).OR.
     1              ((BETAO(I,J).EQ.0.E0).AND.
     2              (BETAO(I+1,J).EQ.1.E0)))THEN
                  BETAUV(I,J)=1.E0
               ELSEIF(J.LT.NLATO)THEN
                  IF(((BETAO(I,J).EQ.1.E0).AND.(BETAO(I,J+1).EQ.0.E0))
     1                 .OR.
     2                 ((BETAO(I,J).EQ.0.E0).AND.
     3                 (BETAO(I,J+1).EQ.1.E0)))THEN
                     BETAUV(I,J)=1.E0
                  ELSE
                     BETAUV(I,J)=BETAO(I,J)
                  ENDIF
               ELSE
                  BETAUV(I,J)=BETAO(I,J)
               ENDIF
            ELSE
               BETAUV(NLONO,J)=BETAUV(1,J)
            ENDIF
 110     CONTINUE
 112  CONTINUE
C  
C---------------------------------------------------------------------
C     
      RETURN
      END
