      SUBROUTINE INDEXOA(ELONA,ELATA,NLONA,NLATA,LONO,LATO,NLONO,NLATO,
     1                   INDI,INDJ)
C
C     * APR 04/02 - B. MIVILLE - REVISED FOR NEW DATA DESCRIPTION FORMAT
C     * JUN 08/01 - B. MIVILLE
C
C     * SEARCH FOR OCEAN POINTS WHICH SURROUNDS THE ATMOSPHERE 
C     * POINT AND THE OCEAN POINTS WITHIN THE 4 ATMOSPHERE POINTS
C     * NEEDED TO DO THE INTERPOLATION
C
C     * INPUT PARAMETERS...
C
C     * ELONA = ATMOSPHERE LONGITUDES (PLUS ONE EXTRA TO THE WEST OF 
C     *         THE FIRST LONGITUDE
C     * ELATA = ATMOSPHERE LATITUDES (PLUS ONE EXTRA TO THE NORTH AND 
C     *         SOUTH)
C     * NLONA = NUMBER OF ATMOSPHERE LONGITUDES INCLUDING CYCLIC
C     * NLATA = NUMBER OF ATMOSPHERE LATITUDES 
C     * LONO  = OCEAN LONGITUDES
C     * LATO  = OCEAN LATITUDES
C     * NLONO = NUMBER OF OCEAN LONGITUDES INCLUDING CYCLIC
C     * NLATO = NUMBER OF OCEAN LATITUDES
C
C     * OUTPUT PARAMETERS...
C
C     * INDI = INDEX THE ATMOSPHERE II POINT TO OCEAN I POINT FOR 
C     *        INTERPOLATION
C     * INDJ = INDEX THE ATMOSPHERE JJ POINT TO OCEAN J POINT FOR 
C     *        INTERPOLATION
C
C-----------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER NLONA,NLATA,NLONO,NLATO
      INTEGER INDI(NLONO,NLATO),INDJ(NLONO,NLATO)
      REAL LONO(NLONO,NLATO),LATO(NLONO,NLATO)
      REAL ELONA(0:NLONA,0:NLATA+1),ELATA(0:NLONA,0:NLATA+1)
      REAL DLATM,DLATP,DLONM,DLONP
C-----------------------------------------------------------------------
C
      DO 300 J=1,NLATO
         JJ=0
         DO 220 I=1,NLONO
            II=0
 200        IF((LATO(I,J).GE.ELATA(II,JJ)).AND.
     1           (LATO(I,J).LT.ELATA(II,JJ+1)))THEN
               DLATM=LATO(I,J)-ELATA(II,JJ)
               DLATP=ELATA(II,JJ+1)-LATO(I,J)
               IF(DLATM.GT.DLATP)THEN
                  INDJ(I,J)=JJ+1
               ELSE             
                  INDJ(I,J)=JJ
               ENDIF
 210           IF((LONO(I,J).GE.ELONA(II,JJ)).AND.
     1              (LONO(I,J).LT.ELONA(II+1,JJ))) THEN
                  DLONM=LONO(I,J)-ELONA(II,JJ)
                  DLONP=ELONA(II+1,JJ)-LONO(I,J)
                  IF(DLONM.GT.DLONP)THEN
                     INDI(I,J)=II+1
                  ELSE
                     INDI(I,J)=II
                  ENDIF
               ELSE
                  II=II+1
                  IF(II.GT.(NLONA-1)) GOTO 220
                  GOTO 210
               ENDIF
            ELSE
               JJ=JJ+1
               IF(JJ.GT.(NLATA)) GOTO 300
               GOTO 200
            ENDIF
 220     CONTINUE
C     
 300  CONTINUE
C
      RETURN
      END
