      REAL FUNCTION GGUBFS (DSEED)                                      
C   IMSL ROUTINE NAME   - GGUBFS                                                
C                                                                               
C-----------------------------------------------------------------------        
C                                                                               
C   COMPUTER            - CRAY/SINGLE                                           
C                                                                               
C   LATEST REVISION     - JUNE 1, 1980                                          
C                                                                               
C   PURPOSE             - BASIC UNIFORM (0,1) RANDOM NUMBER GENERATOR -         
C                           FUNCTION FORM OF GGUBS                              
C                                                                               
C   USAGE               - FUNCTION GGUBFS (DSEED)                               
C                                                                               
C   ARGUMENTS    GGUBFS - RESULTANT DEVIATE.                                    
C                DSEED  - INPUT/OUTPUT DOUBLE PRECISION VARIABLE                
C                           ASSIGNED AN INTEGER VALUE IN THE                    
C                           EXCLUSIVE RANGE (1.D0, 2147483647.D0).              
C                           DSEED IS REPLACED BY A NEW VALUE TO BE              
C                           USED IN A SUBSEQUENT CALL.                          
C                                                                               
C   PRECISION/HARDWARE  - SINGLE/ALL                                            
C                                                                               
C   REQD. IMSL ROUTINES - NONE REQUIRED                                         
C                                                                               
C   NOTATION            - INFORMATION ON SPECIAL NOTATION AND                   
C                           CONVENTIONS IS AVAILABLE IN THE MANUAL              
C                           INTRODUCTION OR THROUGH IMSL ROUTINE UHELP          
C                                                                               
C   COPYRIGHT           - 1978 BY IMSL, INC. ALL RIGHTS RESERVED.               
C                                                                               
C   WARRANTY            - IMSL WARRANTS ONLY THAT IMSL TESTING HAS BEEN         
C                           APPLIED TO THIS CODE. NO OTHER WARRANTY,            
C                           EXPRESSED OR IMPLIED, IS APPLICABLE.                
C                                                                               
C-----------------------------------------------------------------------        
C                                                                               
C                                  SPECIFICATIONS FOR ARGUMENTS                 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DOUBLE PRECISION   DSEED                                          
C                                  SPECIFICATIONS FOR LOCAL VARIABLES           
      REAL               S2P31,S2P31M,SEED                              
C                                  S2P31M = (2**31) - 1                         
C                                  S2P31 = (2**31)                              
      DATA               S2P31M/2147483647.E0/,S2P31/2147483648.E0/     
C                                  FIRST EXECUTABLE STATEMENT                   
      SEED = DSEED                                                      
      SEED = MOD(16807.E0*SEED,S2P31M)                                  
      GGUBFS = SEED / S2P31                                             
      DSEED = SEED                                                      
      RETURN                                                            
      END
