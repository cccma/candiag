      FUNCTION ERF1(X)
C
C     * THIS FUNCTION IS TAKEN FROM NUMERICAL RECIPES.
C     * CALL TO "GAMMP" IS REPLACED WITH "GAMMP1".
C     * THE ORIGINAL NAME "ERF" IS CHANGED TO "ERF1".
C
C     JUL 25/96 - SLAVA KHARIN
C
C  (C) COPR. 1986-92 NUMERICAL RECIPES SOFTWARE #=!E=#,)]UBCJ.
C--------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL ERF1,X
CU    USES GAMMP1
      REAL GAMMP1
C--------------------------------------------------------------------
      IF(X.LT.0.E0)THEN
        ERF1=-GAMMP1(0.5E0,X*X)
      ELSE
        ERF1=GAMMP1(0.5E0,X*X)
      ENDIF
      RETURN
      END
