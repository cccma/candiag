      SUBROUTINE PRECON3(FLO,HI,CINT,SCALE,A,NI,NJ,NC,SPVAL)
  
C **********************************************************************
C *   SEP 18/2006 - F.MAJAESS (MODIFIED FOR TOLERANCE IN SPVAL)        *
C *   DEC 06/90 - F.MAJAESS (BASED ON PRECON2 WITH SPVAL ADDED)        *
C *   OBTAINS CONTOUR INTERVAL AND SCALING FACTOR FOR AN SCM ARRAY     *
C *   APPROPRIATE FOR INPUT TO CONREC WITH VALUES EQUAL TO "SPVAL"     *
C *   SKIPPED.                                                         *
C *   INPUT: A=ARRAY                                                   *
C *          NI,NJ=DIMENSIONS OF A                                     *
C *          NC=APPROXIMATE NUMBER OF CONTOUR INTERVALS                *
C *   OUTPUT:FLO  =MINIMUM CONTOUR CONSIDERED                          *
C *          HI   =MAXIMUM CONTOUR CONSIDERED                          *
C *          CINT =CONTOUR INTERVAL                                    *
C *          SCALE=SCALING FACTOR                                      *
C *          SPVAL=SKIPPING REAL VALUE                                 *
C **********************************************************************
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DIMENSION A(NI,NJ)
C     ------------------------------------------------------------------

C     * SETUP IN "SPVALT" THE TOLERANCE TO CHECK "SPVAL" AGAINST.

      SPVALT=1.E-6*SPVAL

      AMN  =+1.E+38
      AMX  =-1.E+38
      DO 200 J=1,NJ 
        DO 100 I=1,NI 
          IF(ABS(A(I,J)-SPVAL).GT.SPVALT) THEN
           IF(A(I,J).LT.AMN) AMN=A(I,J)
           IF(A(I,J).GT.AMX) AMX=A(I,J)
          ENDIF
 100    CONTINUE
 200  CONTINUE
      RANGE=AMX-AMN 
      SCALE=FSCAL(AMX,AMN)
      CINT =CONT(RANGE,NC,SCALE)
      FLO  =FLOAT((INT(AMN*SCALE)/100+1)*100)
      HI   =FLOAT((INT(AMX*SCALE)/100)*100)
C     ------------------------------------------------------------------
      RETURN
      END
