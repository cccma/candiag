      BLOCK DATA AGCHCU_BD
C
C     * FEB 02/2010 - F.MAJAESS (ADD "NCURRCOLRESET" TO "CCCAGCHCU"
C     *                          COMMON BLOCK)
C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      LOGICAL COLOURCURVES
      INTEGER NCURVCOL,MAXNCURVCOL,ICURVCOL(26)
      INTEGER IPATNE,NCURRCOLRESET
      CHARACTER PAT(26)*16

      COMMON /CCCAGCHCU/ COLOURCURVES,NCURVCOL,MAXNCURVCOL,ICURVCOL,
     1     PAT,IPATNE,NCURRCOLRESET

      DATA COLOURCURVES/.FALSE./
      DATA MAXNCURVCOL/26/
      DATA IPATNE/26/
      DATA NCURRCOLRESET/0/
      DATA PAT( 1)/'$$$$$$$$$$$$$$$$'/,
     B     PAT( 2)/'$$$''$$$''$$$''$$$'''/,
     C     PAT( 3)/'$''$''$''$''$''$''$''$'''/,
     D     PAT( 4)/'$$$$$''$''$$$$$''$'''/,
     E     PAT( 5)/'$''$$$$$$$$$''$''$'''/,
     F     PAT( 6)/'$$''$''$$''$''$$''$''$'/,
     G     PAT( 7)/'$$$$''$$$$''$$$$''$'/,
     H     PAT( 8)/'$$$$$$''$''$$$$$$$'/,
     I     PAT( 9)/'$$$$$$$''$$$$$$$'''/,
     J     PAT(10)/'$$$$$$$''$$$$$$$$'/
      DATA PAT(11)/'$A$$$$$$$$$$$$$$'/,
     B     PAT(12)/'$$$$B$$$$$$$$$$$'/,
     C     PAT(13)/'$$$$$$$C$$$$$$$$'/,
     D     PAT(14)/'$$$$$$$$$$D$$$$$'/,
     E     PAT(15)/'$$$$$$$$$$$$$E$$'/,
     F     PAT(16)/'F$$$$$$$$$$$$$$$'/,
     G     PAT(17)/'$$$G$$$$$$$$$$$$'/,
     H     PAT(18)/'$$$$$$H$$$$$$$$$'/,
     I     PAT(19)/'$$$$$$$$$I$$$$$$'/,
     J     PAT(20)/'$$$$$$$$$$$$J$$$'/
      DATA PAT(21)/'$$$$$$$$$$$$$$$K'/,
     B     PAT(22)/'$$L$$$$$$$$$$$$$'/,
     C     PAT(23)/'$$$$$M$$$$$$$$$$'/,
     D     PAT(24)/'$$$$$$$$N$$$$$$$'/,
     E     PAT(25)/'$$$$$$$$$$$O$$$$'/,
     F     PAT(26)/'$$$$$$$$$$$$$$P$'/

      END
