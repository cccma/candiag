#define stringize(x) str(x)
#define str(x) #x
! The above macros, IN COMBINATION, support stringizing macro
! expansions, i.e. _UDUNITS_DATA_PATH. See here for details
!   https://gcc.gnu.org/onlinedocs/gcc-4.3.6/cpp/Stringification.html

      module misc_param
      implicit none

c.....Root directory for CCCma source code etc
      character(256) :: ccrnsrc="dummy"

c.....Location of udunits.dat
      ! _UDUNITS_DATA_PATH is replaced via a cpp key def at compile
      !     time, and stringize puts quotes around it.
      character(256) :: udunits_data_path=stringize(_UDUNITS_DATA_PATH)
      namelist /nc2ccc_par/ ccrnsrc, udunits_data_path

      end module misc_param
