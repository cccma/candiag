      subroutine nc_get_file_info(fid,nvars,vars,ngatts,gattribs)
c************************************************************************
c 
c************************************************************************
      use cdftype

      implicit none

      !--- input netcdf file id
      integer(kind=4) :: fid
      integer :: nvars, ngatts

      !--- These are returned to the calling program
      type(cdf_var_t), pointer :: vars(:)
      type(cdf_att_t), pointer :: gattribs(:)

      include "netcdf.inc"

      integer ret, did, dsz, vid, aid, asz, n
      integer ndims, unlimdimid
      character(NF_MAX_NAME) :: dname, vname, attname
      integer :: xtype, dimids(NF_MAX_VAR_DIMS), natts
      character(10) :: var_type
      integer :: verbose=0

      character*40 errstr     ! error string

      errstr="nc_get_file_info :: "

c.....Find out the number of dimensions (ndims), variables (nvars)
c     and global attributes (ngatts) as well as the id of the unlimited
c     dimension (or -1 if no unlimited dimension)
      ret = NF_INQ(fid, ndims, nvars, ngatts, unlimdimid)
      if (ret .ne. NF_NOERR) then
        write(6,*)"NF_INQ(fid=",fid," ret=",ret
      endif
      call nc_handle_err (errstr, ret, fid)

c.....read variable meta data
      allocate(vars(nvars))
      do vid=1,nvars
        !--- get variable info for each variable id
        ret =  NF_INQ_VAR(fid, vid, vname, xtype, ndims, dimids, natts)
        if (ret .ne. NF_NOERR) then
          write(6,*)"NF_INQ_VAR(fid,..."
        endif
        call nc_handle_err (errstr, ret, fid)

        if (xtype.eq.NF_BYTE) then
          var_type='byte'
        else if (xtype.eq.NF_CHAR) then
          var_type='text'
        else if (xtype.eq.NF_SHORT) then
          var_type='short'
        else if (xtype.eq.NF_INT) then
          var_type='int'
        else if (xtype.eq.NF_FLOAT) then
          var_type='float'
        else if (xtype.eq.NF_DOUBLE) then
          var_type='double'
        else
          var_type='unknown'
        endif

        vars(vid)%name = trim(vname)
        vars(vid)%xtype = xtype
        vars(vid)%ndims = ndims
        vars(vid)%natts = natts
        vars(vid)%iscoord=.false.
        vars(vid)%vid = vid

        !--- define variable shape
        if (ndims.gt.0) then
          allocate(vars(vid)%dimids(ndims))
          vars(vid)%dimids = dimids(1:ndims)
          allocate(vars(vid)%shape(ndims))
        endif
        do n=1,ndims
          ret = NF_INQ_DIM(fid, dimids(n), dname, dsz)
          if (ret .ne. NF_NOERR) then
            write(6,*)"NF_INQ_DIM(fid,..."
          endif
          call nc_handle_err (errstr, ret, fid)
          if (ndims.eq.1 .and. trim(dname).eq.trim(vname)) then
            !--- by convention this is a coordinate variable
            vars(vid)%iscoord=.true.
            !--- allocate and assign coordinate values
            if (xtype.eq.NF_BYTE) then
              allocate(vars(vid)%byte_vals(dsz))
              ret = NF_GET_VAR_INT1(fid, vid, vars(vid)%byte_vals)
              call nc_handle_err (errstr, ret, fid)
            else if (xtype.eq.NF_SHORT) then
              allocate(vars(vid)%short_vals(dsz))
              ret = NF_GET_VAR_INT2(fid, vid, vars(vid)%short_vals)
              call nc_handle_err (errstr, ret, fid)
            else if (xtype.eq.NF_INT) then
              allocate(vars(vid)%int_vals(dsz))
              ret = NF_GET_VAR_INT(fid, vid, vars(vid)%int_vals)
              call nc_handle_err (errstr, ret, fid)
            else if (xtype.eq.NF_FLOAT) then
              allocate(vars(vid)%float_vals(dsz))
              ret = NF_GET_VAR_REAL(fid, vid, vars(vid)%float_vals)
              call nc_handle_err (errstr, ret, fid)
            else if (xtype.eq.NF_DOUBLE) then
              allocate(vars(vid)%double_vals(dsz))
              ret = NF_GET_VAR_DOUBLE(fid, vid, vars(vid)%double_vals)
              call nc_handle_err (errstr, ret, fid)
            endif
          endif
          vars(vid)%shape(n)=dsz
        enddo
        if (verbose.gt.0) then
          write(6,'(a,i4,5a,i4,a,i4,a,7i5)')
     &      ' vid=',vid,' ',trim(var_type),' ',trim(vname),
     &      ' :: ndims=',ndims,' natts=', vars(vid)%natts,
     &      ' shape: ',vars(vid)%shape(:)
        endif

        !--- define variable attributes
        if (natts.gt.0) then
          allocate(vars(vid)%attribs(natts))
        endif
        do n=1,natts
          !--- get attribute info for each attribute id
          ret = NF_INQ_ATTNAME(fid, vid, n, attname)
          call nc_handle_err (errstr, ret, fid)
          ret = NF_INQ_ATT(fid, vid, attname, xtype, asz)
          call nc_handle_err (errstr, ret, fid)

          if (len_trim(attname).gt.len(vars(vid)%attribs(n)%name)) then
            write(6,*)'Global attribute name is too long.'
            write(6,*)trim(attname)
            cycle
          endif

          !--- process the attribute according to its external type
          vars(vid)%attribs(n)%vid = vid
          vars(vid)%attribs(n)%xtype = xtype
          vars(vid)%attribs(n)%name = trim(attname)
          vars(vid)%attribs(n)%length = asz

          if (asz.le.0) then
            !--- This attribute has zero length (e.g. empty string)
            !--- do not allocate space for it
            cycle
          endif

          if (xtype.eq.NF_BYTE) then
            allocate(vars(vid)%attribs(n)%byte_vals(asz))
            ret = NF_GET_ATT_INT1(fid, vid, attname,
     &                            vars(vid)%attribs(n)%byte_vals)
            call nc_handle_err (errstr, ret, fid)
          else if (xtype.eq.NF_CHAR) then
            if (asz.le.len(vars(vid)%attribs(n)%char_vals)) then
              ret = NF_GET_ATT_TEXT(fid, vid, attname,
     &                              vars(vid)%attribs(n)%char_vals)
              call nc_handle_err (errstr, ret, fid)
            else
              write(6,*)'global character attribute ',trim(attname),
     &                 ' is too large. att_length = ',asz
              cycle
            endif
          else if (xtype.eq.NF_SHORT) then
            allocate(vars(vid)%attribs(n)%short_vals(asz))
            ret = NF_GET_ATT_INT2(fid, vid, attname,
     &                            vars(vid)%attribs(n)%short_vals)
            call nc_handle_err (errstr, ret, fid)
          else if (xtype.eq.NF_INT) then
            allocate(vars(vid)%attribs(n)%int_vals(asz))
            ret = NF_GET_ATT_INT(fid, vid, attname,
     &                            vars(vid)%attribs(n)%int_vals)
            call nc_handle_err (errstr, ret, fid)
          else if (xtype.eq.NF_FLOAT) then
            allocate(vars(vid)%attribs(n)%float_vals(asz))
            ret = NF_GET_ATT_REAL(fid, vid, attname,
     &                            vars(vid)%attribs(n)%float_vals)
            call nc_handle_err (errstr, ret, fid)
          else if (xtype.eq.NF_DOUBLE) then
            allocate(vars(vid)%attribs(n)%double_vals(asz))
            ret = NF_GET_ATT_DOUBLE(fid, vid, attname,
     &                           vars(vid)%attribs(n)%double_vals)
            call nc_handle_err (errstr, ret, fid)
          else
            write(6,*)'Unknown external type for attribute ',
     &        trim(attname),' for variable ',trim(vname)
            stop
          endif
        enddo
        if (verbose.gt.0) then
          call print_cdf_var_t(vars(vid))
        endif
      enddo

c.....read global attributes
      allocate(gattribs(ngatts))
      do aid=1,ngatts
        !--- get attribute info for each global attribute id
        ret = NF_INQ_ATTNAME(fid, NF_GLOBAL, aid, attname)
        call nc_handle_err (errstr, ret, fid)
        ret = NF_INQ_ATT(fid, NF_GLOBAL, attname, xtype, asz)
        call nc_handle_err (errstr, ret, fid)

        if (len_trim(attname).gt.len(gattribs(aid)%name)) then
          write(6,*)'Global attribute name is too long.'
          write(6,*)trim(attname)
          cycle
        endif

        !--- process the attribute according to its external type
        gattribs(aid)%vid = NF_GLOBAL
        gattribs(aid)%xtype = xtype
        gattribs(aid)%name = trim(attname)
        gattribs(aid)%length = asz

        if (asz.le.0) then
          !--- This attribute has zero length (e.g. empty string)
          !--- do not allocate space for it
          cycle
        endif

        if (xtype.eq.NF_BYTE) then
          allocate(gattribs(aid)%byte_vals(asz))
          ret = NF_GET_ATT_INT1(fid, NF_GLOBAL, attname,
     &                          gattribs(aid)%byte_vals)
          call nc_handle_err (errstr, ret, fid)
        else if (xtype.eq.NF_CHAR) then
          if (asz.le.len(gattribs(aid)%char_vals)) then
            ret = NF_GET_ATT_TEXT(fid, NF_GLOBAL, attname,
     &                            gattribs(aid)%char_vals)
            call nc_handle_err (errstr, ret, fid)
          else
            write(6,*)'global character attribute ',trim(attname),
     &               ' is too large. att_length = ',asz
            cycle
          endif
        else if (xtype.eq.NF_SHORT) then
          allocate(gattribs(aid)%short_vals(asz))
          ret = NF_GET_ATT_INT2(fid, NF_GLOBAL, attname,
     &                          gattribs(aid)%short_vals)
          call nc_handle_err (errstr, ret, fid)
        else if (xtype.eq.NF_INT) then
          allocate(gattribs(aid)%int_vals(asz))
          ret = NF_GET_ATT_INT(fid, NF_GLOBAL, attname,
     &                          gattribs(aid)%int_vals)
          call nc_handle_err (errstr, ret, fid)
        else if (xtype.eq.NF_FLOAT) then
          allocate(gattribs(aid)%float_vals(asz))
          ret = NF_GET_ATT_REAL(fid, NF_GLOBAL, attname,
     &                          gattribs(aid)%float_vals)
          call nc_handle_err (errstr, ret, fid)
        else if (xtype.eq.NF_DOUBLE) then
          allocate(gattribs(aid)%double_vals(asz))
          ret = NF_GET_ATT_DOUBLE(fid, NF_GLOBAL, attname,
     &                          gattribs(aid)%double_vals)
          call nc_handle_err (errstr, ret, fid)
        else
          write(6,*)'Unknown external type for attribute ',trim(attname)
          stop
        endif
        if (verbose.gt.0) then
          call print_cdf_att_t(gattribs(aid))
        endif
      enddo

      end
