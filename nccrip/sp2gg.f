      subroutine sp2gg(grid,spec,ibuf,kuv)
c***********************************************************************
c Map 2d spectral fields to a gaussian grid
c
c Larry Solheim   Jun,2009
c $Id: sp2gg.f 648 2011-04-22 03:42:40Z acrnrls $
c***********************************************************************
      implicit none

      !--- Input
      real :: grid(*), spec(*)
      integer :: kuv, ibuf(8)

      !--- Local
      integer, parameter :: nlat_max=512
      integer, parameter :: maxw=2304400, maxl=100
      integer, parameter :: lda=2275

      integer :: ilg,ilat,maxlg,ilath,ilg1,lgg,lrlmt
      integer :: nwds,maxlev,nsp,ilev,ngg,nc4to8
      integer :: verbose, j
      integer :: lr, lm, ktr

      integer, save :: lr_save=-1, lm_save=-1, ktr_save=-1
      integer, save :: lsr(2,500), la, ifax(10)

      real*8,save :: alp(lda,nlat_max),epsi(lda)
      real*8,save :: sl(nlat_max),cl(nlat_max),wl(nlat_max)
      real*8,save :: wossl(nlat_max),rad(nlat_max)
      real,  save :: trigs(nlat_max)

      !--- Work space used by staggx
      real :: wrk(maxw), wrks(12416), wrkl(19400)
      !--- Ensure wrk is contiguous in memory
      common/w/wrk
c-----------------------------------------------------------------------

      ilev=1
      verbose=1

      if (ibuf(1) .ne. nc4to8("SPEC")) then
        write(6,*)"SP2GG: Input record is not SPEC"
        call xit("SP2GG",-1)
      endif

      !--- Determine grid size from input ibuf(7) value
      lrlmt=ibuf(7)
      if ( lrlmt .le. 99999 ) then
        lr=lrlmt/1000
        lm=lrlmt/10-100*lr
      else
        lr=lrlmt/10000
        lm=lrlmt/10-1000*lr
      endif
      if (lr.lt.lm) then
        write(6,*)"SP2GG: Invalid lrlmt value ",lrlmt
        call xit("SP2GG",-2)
      endif
      ktr=mod(lrlmt,10)

c  if [ $kuv -eq 1 ]; then
c    # When converting model winds to real winds assume that the field to be
c    # converted has been through cwinds which will have modified the ibuf7
c    # value such that the value of LR calculated above will be 1 greater than
c    # it should actually be. (e.g. an input value of ibuf7=48482 gets changed
c    # to ibuf7=49482 on return from cwinds)
c    LR=`expr $LR - 1`
c  fi
      ilat=lr
      ilg=2*ilat

      maxlg=ilg+2
      ilath=ilat/2
      ilg1=ilg+1
      lgg=ilg1*ilat

      if (lr.ne.lr_save .or. lm.ne.lm_save .or. ktr.ne.ktr_save) then
        !--- These quantities are only calculated on the first call
        !--- or when the grid sizes changes (compared to ibuf(7)=lrlmt)

        !--- dimgt will calculate lsr,la,lr,lm,ktr given lrlmt
        call dimgt(lsr,la,lr,lm,ktr,lrlmt)
        if (verbose.gt.1) then
          write(6,'(a,5i8)')' SP2GG: la,lr,lm,ktr,lrlmt: ',
     &                              la,lr,lm,ktr,lrlmt
        endif

        if (ibuf(5).ne.la) then
          write(6,*)"SP2GG: Invalid ibuf5 value ",ibuf(5)
          write(6,*)"       Expecting ",la
          call xit("SP2GG",-3)
        endif

        call epscal(epsi,lsr,lm)
        call gaussg(ilath,sl,wl,cl,rad,wossl)
        call trigl(ilath,sl,wl,cl,rad,wossl)
        call ftsetup(trigs,ifax,ilg)

        !--- Calculate Legendre polynomials in alp for all latitudes
        do j=1,ilat
          call alpst2(alp(1,j),lsr,lm,sl(j),epsi)
        enddo

        lr_save=lr
        lm_save=lm
        ktr_save=ktr
      endif

      nwds=2*la+lgg
      maxlev=maxw/nwds
      if (maxlev.gt.maxl) maxlev=maxl
      ngg=2*la*maxlev+1

      if (verbose.gt.1) then
        write(6,6015)ibuf(3),ibuf(2),ibuf(4),lr,lm,ilg1,ilat
 6015   format('SP2GG: ',a4,1x,i10,'  level=',i5,'  spec=',2i5,
     &         '  grid=',2i5)
      endif

c.....Assign the input spectral field to a work array for use in STAGGX
      wrk(1:2*la) = spec(1:2*la)

c.....Convert spectral fields to gaussian grids.
c     Normal spec fields are (lr,lm) but winds are (lr+1,lm)
      call staggx (wrk(ngg),ilg1,ilat,ilev,sl,wrk(1),lsr,lm,la,
     1            alp,lda,epsi,wrks,wrkl,trigs,ifax,maxlg)

      if (kuv.eq.1) then
        !--- Convert model winds to real winds
        !--- ie multiply by (radius of earth)/cos(lat)
        call lwbw2(wrk(ngg),ilg1,ilat,cl,-1)
      endif

c.....Reset ibuf values for the output GRID record
      ibuf(1) = nc4to8("GRID")
      ibuf(5) = ilg1
      ibuf(6) = ilat
      ibuf(7) = 0

c.....Assign the output GRID field from the wrk array
      grid(1:ilg1*ilat) = wrk(ngg:ngg+ilg1*ilat-1)

      end

      SUBROUTINE STAGGX (GG,ILG1,ILAT,ILEV,SL, SC,LSR,LM,LA,
     1               ALP,lda,EPSI,WRKS,TRANCHE,TRIGS,IFAX,MAXL)
C
C     * Jun 24,2009 - L.Solheim - Calculate Legendre coefficients
C                     in calling routine
C     * JUL 22/94 - F.MAJAESS(BECAUSE OF FOURIER TRANSFORM ROUTINE
C     *                       CODE STRUCTURE, ENFORCE DOING
C     *                       CALCULATIONS 1 LEVEL AT A TIME IF
C     *                       MAXL<2*LM)
C     * JUL 14/92 - E. CHAN    (ADD REAL*8 DECLARATIONS)
C     * AUG 13/90 - M.LAZARE. - REMOVE HARD-COAT ON IFAX, SINCE PASSED.
C     * DEC 20/83 - B.DUGAS.
C     * JUL 31/79 - J.D.HENDERSON
C
C     * PRODUCES GLOBAL GAUSSIAN GRID SET GG(ILG1,ILAT,ILEV)
C     * FROM GLOBAL SPECTRAL COEFF SET IN SC(LA,ILEV).
C     * LONGITUDE IS ZERO AT THE LEFT AND POSITIVE TO THE RIGHT.
C     * LSR CONTAINS ROW LENGTH INFO FOR ALP,EPSI,SC.
C     * TRIGS AND IFAX CONTAIN THE INFO FOR CRAY FFT'S.
C     * ALP = WORK FIELD FOR LEGENDRE POLYNOMIALS.
C     * EPSI = PRECOMPUTED CONSTANTS.
C     * SL = SIN(LATITUDE) OF GAUSSIAN ROWS FROM S.POLE TO N.POLE.
C
C     * LEVEL 2,GG,SC,ALP,EPSI
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMPLEX SC(LA,ILEV)
      REAL GG(ILG1,ILAT,ILEV)
      REAL*8 SL(1),ALP(lda,*),EPSI(1)
      REAL   TRIGS(1),WRKS(1),TRANCHE(MAXL,1)
      INTEGER LSR(2,1),IFAX(1)
C--------------------------------------------------------------------
      ILG=ILG1-1
      ILH=MAXL/2
      ILH2=MAXL
      MAXF=LM-1
C
      IF(ILEV.GT.1.AND.MAXL.LT.2*LM) THEN
clps
clps Note that MAXL will always be 100 when this routine is called
clps from cofagg. Therefore, for LM>50 this branch will always be taken.
clps This means that for T63 or greater grid size, the conversion from
clps SPEC to GRID records will always happen 1 level at a time in cofagg.
clps
       WRITE(6,6000)MAXL,2*LM
       DO 110 J=1,ILAT
c         CALL ALPST2(ALP,LSR ,LM,SL(J),EPSI)
         DO 110 L=1,ILEV
          CALL STAF (TRANCHE,SC(1,L),LSR,LM,LA,ILH,1,ALP(1,J))
          CALL FFGFW(TRANCHE,ILH2,TRANCHE,ILH,MAXF,ILG,WRKS,
     1               1,IFAX,TRIGS)
          DO 110 I=1,ILG
            GG(I,J,L)=TRANCHE(I,1)
  110  CONTINUE
      ELSE
       DO 210 J=1,ILAT
c         CALL ALPST2(ALP,LSR ,LM,SL(J),EPSI)
         CALL STAF (TRANCHE,SC,LSR,LM,LA,ILH,ILEV,ALP(1,J))
         CALL FFGFW(TRANCHE,ILH2,TRANCHE,ILH,MAXF,ILG,WRKS,
     1               ILEV,IFAX,TRIGS)
         DO 210 L=1,ILEV
            DO 210 I=1,ILG
               GG(I,J,L)=TRANCHE(I,L)
  210  CONTINUE
      ENDIF
      DO 220 L=1,ILEV
         DO 220 J=1,ILAT
            GG(ILG1,J,L)=GG(1,J,L)
  220 CONTINUE
C
      RETURN
C--------------------------------------------------------------------
 6000 FORMAT(/,' *** STAGGX-WARNING,COMPUTATION BY SINGLE LEVEL IS',
     1       ' USED SINCE MAXL=',I3,' < 2*LM=',I3,' ***',/)
C--------------------------------------------------------------------
      END
